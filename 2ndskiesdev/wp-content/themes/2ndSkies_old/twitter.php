<?php
/*
Template Name: twitter
*/
?>

<?php
/*
	Widget Theme for HL Twitter
	   To change this theme, copy hl_twitter_widget.php
	   to your current theme folder, do not edit this
	   file directly.

	Available Properties:
		$before_widget
		$after_widget
		$before_title
		$after_title
		$widget_title
		$show_avatars
		$show_powered_by
		$num_tweets: how many tweets to show
		$tweets: array of $tweet
		$tweet: object representing a tweet
			$tweet->twitter_tweet_id
			$tweet->tweet
			$tweet->lat
			$tweet->lon
			$tweet->created
			$tweet->reply_tweet_id
			$tweet->reply_screen_name
			$tweet->source
			$tweet->screen_name
			$tweet->name
			$tweet->avatar
		$user: represents the Twitter user (ONLY SET IF SHOWING A SINGLE USERS TWEETS!)
			$user->twitter_user_id
			$user->screen_name
			$user->name
			$user->num_friends
			$user->num_followers
			$user->num_tweets
			$user->registered
			$user->url
			$user->description
			$user->location
			$user->avatar
*/
?>

<?php get_header(); die('1');?>
<!-- main content -->
<div id="content">
    <!-- breadcrumbs container -->
    <div class="breadcrumbs-container clearfix">
        <div class="breadcrumbs">
            <ul>
                <li><a href="<?php echo home_url(); ?>">Home</a></li>
                <li><?php the_title(); ?></li>
            </ul>
        </div>
    </div>

    <h1>Tweets</h1>
    <div   allign="center">
    <a class="twitter-timeline" href="https://twitter.com/2ndSkiesTeam"  data-widget-id="411077797436616704"  data-chrome="noheader">
        Tweets from @2ndSkiesTeam</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
    </div>




<?php get_footer(); ?>