    <div id="footer">
    
        <div class="center-container">
        
            <div class="cols-holder">
            
                <div class="col">
                    <h2>Navigate</h2>
                    <!-- navigation -->
                    <div class="nav">
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>' ) ); ?>
                    </div><!-- .nav-->
                </div><!-- .col -->
                
                <div class="col">
                    <h2>Forex Strategies</h2>
                    <!-- blog -->
                    <div class="blog slideshow">
                        <div class="pager">
                            <span class="title">Featured</span>
                            <div class="buttons">
                                <a href="#" class="btn-prev">Previous</a>
                                <a href="#" class="btn-next">Next</a>
                            </div>
                        </div>
                        <div class="mask">
                            <div class="blog-list slideset">
                                <?php dynamic_sidebar('footer-sidebar'); ?>
                            </div>
                        </div>
                        <a href="/forex-trading-strategies/forex-strategies/" class="more">Read more</a>
                    </div><!--blog slideshow-->
                </div><!-- .col -->
                
                <div class="col follow">
                    <h2>Daily News</h2>
                    <!-- social networks -->
                    <?php
						global $wpdb;
						$sql_where = "";
						$objects = $wpdb->get_results('
							SELECT u.screen_name, u.avatar, u.name, t.*
							FROM '.HL_TWITTER_DB_PREFIX.'tweets AS t
							LEFT JOIN '.HL_TWITTER_DB_PREFIX.'users AS u ON t.twitter_user_id = u.twitter_user_id
							'.$sql_where.'
							ORDER BY t.created DESC
							LIMIT 2
						');
						$num_objects = $wpdb->num_rows;
                    ?>
                    <ul class="twitter-list">
                        <?php if($num_objects>0): ?>
                            <?php foreach($objects as $object): ?>
                                <li>
                                    <div class="visual">
                                        <a href="/daily-news/"><img alt="" src="<?php echo hl_twitter_get_avatar($object->avatar); ?>" /></a>
                                    </div>
                                    <div class="text-holder">
                                        <p><?php echo hl_twitter_show_tweet($object->tweet); ?></p>
                                        <em class="date"><?php echo hl_e($object->created); ?> <?php echo hl_time_ago($object->created); ?> ago</em>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <li class="no-tweets">No tweets were found.</li>
                        <?php endif; ?>
                    </ul><!--twitter-list-->
                    <a href="/daily-news">View All Daily News</a>
                </div><!-- .col.follow -->
                
            </div><!-- .cols-holder -->
            
		</div><!-- .center-container -->
        
        <div class="add-info">
            <div class="center-container">
   
            <div class="logo-small"><img src="<?php echo THEME_IMAGES ?>/logo-small.png" width="59" height="53" alt="2ndSkies logo"  /></div>
                <!-- copyright and address -->
                <?php dynamic_sidebar('social-networks-sidebar'); ?>
                <div class="clearfix"></div>
                <?php dynamic_sidebar('copyright-sidebar'); ?> 
            </div>
        </div><!-- .add-info -->
        
	</div><!-- #footer -->
        
</div><!-- END #wrapper -->

<?php wp_footer(); ?>
        <script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//dnn506yrbagrg.cloudfront.net/pages/scripts/0014/2188.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>

</body>
</html>