<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * @package WordPress
 * @subpackage 2ndSkies
 *
 * Functions - Framework gatekeeper
 *
 * This file loads up the framework file,
 * and finally initialises the main Theme Class.
 *
 */

/* Initialise THEME */

/* Get the template directory and make sure it has a trailing slash. */
$theme_dir = trailingslashit( get_template_directory() );

/* Load the Themelia framework and launch it. */
require_once( $theme_dir . 'library/framework.php' );
new ThemeSSFRX();

/* Set up the theme early. */
add_action( 'after_setup_theme', 'ssfrx_theme_setup', 5 );


/**
 * The theme setup function.  This function sets up support for various WordPress and framework functionality.
 *
 * @since  1.0.0
 * @access public
 * @return void
 */
function ssfrx_theme_setup() {
	/* Load files. */
	require_once( trailingslashit( get_template_directory() ) . 'library/functions/core.php' );
	
	/**
	 * Disable automatic general feed link outputting.
	 */
	remove_action('wp_head', 'feed_links_extra', 3);
	
	/**
	 * Add Theme Support
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'single-post-thumbnail', 400, 9999, true );

}
