<?php
/*
Template Name: Advanced Price Action Course
*/
?>
<?php get_header(); ?>
	<!-- main content -->
	<div id="content">
		<!-- breadcrumbs container -->
		<div class="breadcrumbs-container clearfix">
			<div class="share-block alignright">
				<!-- AddThis Button BEGIN -->
                <a href="http://www.facebook.com/sharer.php?u=http://2ndskies.com/advanced-price-action-course/" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/facebook.png" height="20" width="20" border="0" alt="Facebook" />
                </a>
                <a href="http://twitter.com/share?url=http://2ndskies.com/advanced-price-action-course/&text=Advanced+Price+Action+Course+" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/twitter.png" height="20" width="20" border="0" alt="Twitter" />
                </a>
                <a href="http://www.addthis.com/bookmark.php?source=tbx32nj-1.0&amp;=300&amp;pubid=ra-52f4b153538fc7fd&amp;url=http%3A%2F%2F " target="_blank"  >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/more.png" height="20" width="20" border="0" alt="More..." /></a>
                <!-- AddThis Button END -->
			</div>
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><?php the_title() ?></li>
				</ul>
			</div>
		</div>
		<!-- promo video -->
		<div class="promo-video-block">
			<?php the_post(); ?>
			<?php the_content(); ?>
			<div class="video-holder">
				<?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
			</div>
		</div>
		<!-- promo text -->
		<div class="promo-text clearfix">
			<h2>What You Get When You Become A Member</h2>
			<div class="clearfix">
				<div class="alignright">
					<a href="#"><img src="<?php echo THEME_IMAGES ?>/img12.png" width="382" height="182" alt="image description" /></a>
				</div>
				<div class="list">
					<ul>
						<li>High Probability Price Action and Pivot Point Strategies I use everyday</li>
						<li>Quantitative Rule-Based Strategies and Data</li>
						<li>Several Free Webinar trainings per year</li>
						<li>Ongoing Lessons</li>
						<li>Full Email Access to Chris Capre</li>
						<li>Free Private Follow Up Session with Chris Capre</li>
					</ul>
				</div>
			</div>
			<p>In this course, you will learn high-probability, advanced price action and pivot point strategies that are backed up with more than 10 years of quantitative data. You will learn the same strategies I trade everyday, which are entirely rule-based, making them easy to learn and identify while reducing emotions and decision making for your trading.</p>
		</div>
		<h2>What We Cover</h2>
		<!-- table list -->
		<div class="table-list">
			<ul>
				<li>Identifying trends with volatility and momentum</li>
				<li>How to Trade Weekend Gaps</li>
				<li>BR Trading and Tactics</li>
				<li>Intraday 6NT Pullbacks</li>
				<li>How to Classify Trends and get into them</li>
				<li>How to Gauge Your Growth</li>
				<li>Intraday 3P HLR Reversal Setup</li>
				<li>3 Dimensional Money Management</li>
				<li>How to Trade Tops and Bottoms</li>
				<li>How to Prepare For Your Trading Day</li>
				<li>Advanced Pbar Reversal Strategy</li>
				<li>Building a Successful Trading Psychology</li>
				<li>Quantitative Data on Pivot Points over the last 10 yrs</li>
				<li>Know When to Trade and When Not To</li>
				<li>The quantitative LBar System with Enhanced Targeting</li>
				<li>Intraday Breakouts with our Mid-Stream System</li>
				<li>The quantitative SBar System with Enhanced Targeting</li>
				<li>Reversion to the Mean Trading with Pivot Points</li>
				<li>How to Identify the Most Important Moves in the Market</li>
				<li>How to Advance Your Pattern Recognition Skills</li>
				<li>Trading OBar Reversal Formations with Quantitative Data</li>
				<li>Develop a Trading Plan (in Follow Up Session)</li>
			</ul>
		</div>
		<!-- add promo text -->
		<div class="add-promo-text clearfix">
			<div class="alignright"><a href="#"><img src="<?php echo THEME_IMAGES ?>/img13.png" width="449" height="320" alt="image description" /></a></div>
			<h2>Advanced Price Action Trading</h2>
			<p>We have analyzed price action patterns over the past 10 years and developed precision methods to trade these setups based on proprietary quantitative data never before published. These are all highly advanced strategies that are easy to learn, and yet they offer you high-probability price action forex setups along with intraday pivot point strategies to trade the markets.</p>
			<p>If you are looking to simplify your trading while having precision methods, this course is for you.</p>
		</div>
		<!-- testimonials -->
		<div class="testimonials-block">
			<h2>Here’s What Students Are Saying About The Course…</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>I really love the PA course and have learned so much about price behavior, order flow and what is really moving the markets. Slowly but surely I'm 
getting profitable with my trades, started in December with a small account and am already up over 20%!.</q>
						<cite>Arthur, Germany &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I've just finished the PA course and learned your strategies after watching the ton of valuable material. In Nov/13 when I started this course, I had to cut off some wrong doings from my previous courses I had. However, Now on Dec/13 I've already banked +620pips and I'm feeling like I'm finally getting it as I'm seeing the charts in a whole new way aside from typical price patterns like pin bars. Very happy about that :)</q>
						<cite>Wandy, Singapore &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>Loving the systems from the Price Action Course as they are all rule-based which completely takes out the guesswork. I’ve already had several winners Not many people if any can back up their strategies with quantified data.</q>
						<cite>Grant D, UK &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
			</div>
		</div>
		<!-- text content -->
		<h2>Advanced Pivot Point Trading</h2>
		<p>Even if you are not using pivot points now, you will be once you understand their potency, power and how they solve the most important issue for traders: entries and exits. We have tested more than 10 metrics on pivot points for the past 10 years with respect to:</p>
		<div class="list">
			<ul>
				<li>The percentage chance of any single pivot being broken on any given day</li>
				<li>The percentage chance that if one pivot is broken, the next one will be touched</li>
				<li>How close price has to get to a pivot before it is likely to touch it</li>
				<li>How to trade reversals with pivot points (reversion to the mean)</li>
				<li>How to find optimum breakouts using pivot points (RS3 breakouts)</li>
			</ul>
		</div>
		<p>And more <br /></p>
<p>Very few books have been written on pivot points or advanced price action techniques. The quantitative price action data and the precise methods make this course completely unlike any other course or book. The data, information, and rule-based systems to trade price action or pivot points are not available anywhere else. If you want to develop a unique yet simple trading edge and have the passion to become a successful forex trader, then this course is for you.</p>
		<span class="content-border clearfix"></span>
		<!-- including in the course -->
		<h2>Also Included With The Course</h2>
		<p>Sign up for the Pro Forex Trading course, and you will also receive:</p>
		<!-- numeric list -->
		<div class="numeric-list">
			<ol>
				<li>One private follow-up mentoring session with Chris Capre – After completing the course, you will receive a private follow-up mentoring session to leverage what you’ve learned in the course to enhance your strengths and minimize your weaknesses.</li>
				<li>Lifetime membership to the Private Forum – Get access to a library of knowledge on the forum.</li>
				<li>Full email support – If you have questions about the course or material, you’ll have full email access to Chris Capre. We pride ourselves on our outstanding customer support, and we respond to emails nearly 365 days a year, including weekends and holidays.</li>
			</ol>
		</div>
		<span class="content-border clearfix"></span>
		<h2>How Does The Course Work?</h2>
		<div class="shadow-box">
			<p>The course is also available on video and is accessible 24 hours a day so you can learn when it’s convenient for you. Once you sign up, you will receive login information to access the course to learn at your own pace.</p>
		</div>
		<h2>FAQ</h2>
		<div class="shadow-box clearfix">
			<ul class="accordion clearfix">
				<?php query_posts( 'cat=13' );?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<li>
						<a href="#" class="opener"><?php the_title();?></a>
						<div class="slide">
							<?php the_content();?>
						</div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
		<!-- order box -->
		<div class="order-box">
			<div class="container">
				<h2>Order the Advanced Price Action Course Here</h2>
				<div class="clearfix">
					<div class="img"><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1"><img src="<?php echo THEME_IMAGES ?>/img11.png" width="459" height="270" alt="image description" /></a></div>
					<div class="text-holder">
						<span class="title">Only 20 Seats Available</span>
						<a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1" class="btn-add">Add To Cart</a>
						<span class="price">*All this for only $399</span>
						<ul class="payment">
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1" class="visa">VISA</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1" class="mastercard">MasterCard</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1" class="american-express">American Express</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1" class="discover">DISCOVER</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=1" class="paypal">PayPal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="desc">
				<p><b>NOTE</b>: 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
		</div>
	</div>
<?php get_footer(); ?>