<?php
/*
Template Name: hl-twitter
*/
?>

<?php
/*
	Widget Theme for HL Twitter
	   To change this theme, copy hl_twitter_widget.php
	   to your current theme folder, do not edit this
	   file directly.

	Available Properties:
		$before_widget
		$after_widget
		$before_title
		$after_title
		$widget_title
		$show_avatars
		$show_powered_by
		$num_tweets: how many tweets to show
		$tweets: array of $tweet
		$tweet: object representing a tweet
			$tweet->twitter_tweet_id
			$tweet->tweet
			$tweet->lat
			$tweet->lon
			$tweet->created
			$tweet->reply_tweet_id
			$tweet->reply_screen_name
			$tweet->source
			$tweet->screen_name
			$tweet->name
			$tweet->avatar
		$user: represents the Twitter user (ONLY SET IF SHOWING A SINGLE USERS TWEETS!)
			$user->twitter_user_id
			$user->screen_name
			$user->name
			$user->num_friends
			$user->num_followers
			$user->num_tweets
			$user->registered
			$user->url
			$user->description
			$user->location
			$user->avatar
*/
?>

<?php get_header(); ?>

<div id="main" class="clearfix">

    <div id="content">
        
        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div>
		</div><!-- breadcrumbs container -->

    
	<?php if (have_posts()) : ?>

	<?php while (have_posts()) : the_post(); ?>
        <div class="default-template">

            <?php the_content(); ?>
        </div>
    <?php endwhile;

		global $wpdb;

		$sql_where = '';

		$total_objects = $wpdb->get_var('SELECT COUNT(*) FROM '.HL_TWITTER_DB_PREFIX.'tweets AS t '.$sql_where);
		$per_page = 20;
		$num_pages = ceil($total_objects/$per_page);
		$current_page = ($_GET['start']>0)?intval($_GET['start']):1;
		$pagination_url = $_SERVER["URL"].'/daily-news';

		$pagination = paginate_links(array(
		'base' => $pagination_url.'%_%',
		'format' => '?start=%#%',
		'total' => $num_pages,
		'current' => $current_page,
		'type'=>'list',
		));

		$objects = $wpdb->get_results($wpdb->prepare('
		SELECT u.screen_name, u.avatar, u.name, t.*
		FROM '.HL_TWITTER_DB_PREFIX.'tweets AS t
		LEFT JOIN '.HL_TWITTER_DB_PREFIX.'users AS u ON t.twitter_user_id = u.twitter_user_id
		'.$sql_where.'
		ORDER BY t.created DESC
		LIMIT %d, %d
		', ($current_page-1)*$per_page, $per_page));
		$num_objects = $wpdb->num_rows;
	?>

        <div class="wrap">
            <form method="get" action="<?php echo $pagination_url; ?>">
                <input type="hidden" name="page" value="hl_twitter" />

                <h1>Real Time Trading & Market News</h1>
			<a href="https://twitter.com/2ndskiesteam" class="twitter-follow-button" data-show-count="false">Follow @2ndskiesteam</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></br>
Follow this account on Twitter to get updates in real time.</p>
				<ul class="twitter-list">
					<?php if($num_objects>0): ?>
						<?php foreach($objects as $object): ?>
							<li>
								<div class="visual">
									<img alt="" src="<?php echo hl_twitter_get_avatar($object->avatar); ?>" />
								</div>
								<div class="text-holder">
									<p><?php echo hl_twitter_show_tweet($object->tweet); ?></p>
									<em class="date"><?php echo hl_e($object->created); ?> <?php echo hl_time_ago($object->created); ?> ago</em>
								</div>
							</li>
						<?php endforeach; ?>
						<?php else: ?>
							<li class="no-tweets">No tweets were found.</li>
					<?php endif; ?>
				</ul><!-- .twitter-list -->
                
				<div class="pager">
					<?php echo $pagination; ?>
					<span class="twitter-num">Displaying <?php echo number_format(($current_page-1)*$per_page+1); ?>&ndash;<?php echo number_format(($current_page-1)*$per_page+$num_objects); ?> of <?php echo number_format($total_objects); ?></span>
				</div><!-- .pager -->
			</form>
		</div><!-- .wrap -->

    <?php endif; ?>
    
	</div><!--content-->
    
    <?php get_sidebar('twitter'); ?><!-- sidebar -->

</div><!-- #main -->

<?php get_footer(); ?>