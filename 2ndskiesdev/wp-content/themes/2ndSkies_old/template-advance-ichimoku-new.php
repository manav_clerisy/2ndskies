<?php
/*
Template Name: Advanced Ichimoku Course NEW
*/
?>
<?php get_header(); ?>
	<!-- main content -->
	<div id="content">

		<!-- promo video -->
		<div class="promo-video-block">
			<?php the_post(); ?>
			<?php the_content(); ?>
            
            <?php if (get_post_meta($post->ID, "embed-video", true) != '')   { 
				?>
                <div class="video-holder_ video-holder-new">
                    <?php echo get_post_meta($post->ID, "embed-video", true); ?>
                </div>
            	<?php } 
			?>
            
		</div>
		<!-- promo text -->
		<div class="promo-text clearfix">
			<h2 class="center">Advanced Ichimoku Training - What You've Been Missing</h2>
            <p>Did you know the last "expert" - Hidenobu Sasaki, wrote only 1 chapter talking about the traditional "5 Lines"? What did he write the other 6 chapters on?</p>

<p>That is what we focus heavily on in our Advanced Ichimoku Course. Most of what you see out there is re-hashed material on how to trade the "5 lines".</p>

<p>We have translated Sasaki's book into English, and share this information with you, only in this course. This is just one reason why we are different than the rest.</p>


		</div><!-- promo text END -->
        
        
        <div class="shadow-box blue">
        <blockquote>
          <p class="lead">"Of the 10,000 that practice Ichimoku, less than a few handful really understand it."
<br><strong>- Goichi Hosada, Founder of Ichimoku Kinko Hyo</strong></p></blockquote>
        </div>
        
        
        
		<h2 class="center">What You Can Expect to Learn From My Course</h2>
		<!-- table list -->
		<div class="table-list">
			<ul>
                <li>Understanding Ichimoku Beyond the 5 Lines</li>
                <li>Ichimoku Time Theory</li>
                <li>Ichimoku Wave Theory</li>
                <li>Ichimoku Price Theory</li>
                <li>Reading the Chikou Span</li>
                <li>6 Ichimoku Strategies Beyond the Theories</li>
                <li>Building A Successful Trading Psychology</li>
                <li>How to Prepare for Your Trading Day</li>
                <li>Trading Like a Business</li>
                <li>How to Find Future Support &amp; Resistance Levels</li>
                <li>How to Trade both Intra-day &amp; Swing with Ichimoku</li>
                <li>Building the Mindset of Abundance</li>
			</ul>
		</div>
        
        
        
        
		<!-- testimonials -->
		<div class="testimonials-block">
			<h2>Here's What Students Are Saying About The Course...</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>Just to let you know I think the course is fantastic. It has improved my trading and understanding of Ichimoku tenfold. Instead of scalping for 5 to 10 pips and letting bad trades run, I am now seeing gains of 50+ pips consistently.</q>
						<cite>Phil, Australia &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I just want to put on record my thanks for the quality of the course content. It represents stupendous value and you've made yourself available on a constant basis to guide us forwards.</q>
						<cite>Steve, Singapore &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>It is clearly evident you are genuinely interested in helping people. Thanks for the encouragement Chris and the great course. You truly are the mentor.</q>
						<cite>Eric, UK &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
			</div>
		</div><!-- testimonials-block END -->
        
        
		<!-- text content -->
		<h2 class="center">Live Trade Setups Forum</h2>
        
        <p>Along with the trading lessons & tutorials, students are also sharing real time analysis and trades using our ichimoku methods. Students also share their impressive backtesting results on the strategies, and how much profit they used with specific modifications.</p> 
        
        <p>This is really a fantastic area, where new students get to learn from more senior traders. Members also share trade ideas and setups before they happen, while I give analysis, commentary and suggestions as to what I'm looking at.</p>
        
<br>
		<!-- testimonials -->
		<div class="testimonials-block adv-bg">
			<h2>Here's What Students Are Saying About The Course...</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>I have had my head in the Shadow System and your webinars.  I guess I am getting my PhD from Capre University.  Anyway its going great...been trading live and watching price action with the Shadow - just really nailing it down.  The results are great so far.  The impulsive/corrective video is like a bible of understanding price action.</q>
						<cite>Chris H, US &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I made 3% in 3 days since obtaining the courses, they are excellent...I appreciate what you have done for me, even in this little time, I feel like with hard work, I can actually make this work consistently :)</q>
						<cite>Ken T, Australia &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I am so happy that I found you and happy to learn a lot from you, now and next years. You were born to trade, teach and educate. Keep going your great mission.</q>
						<cite>Florin, Canada &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
			</div>
		</div><!-- testimonials-block END -->


		<h2 class="center">Frequently Asked Questions</h2>
		<div class="shadow-box clearfix">
			<ul class="accordion clearfix">
				<?php query_posts( 'cat=1166&posts_per_page=-1' );?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<li>
						<a href="#" class="opener"><?php the_title();?></a>
						<div class="slide">
							<?php the_content();?>
						</div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
        <br>
        
        
	<div class="subtle-box subtle-box-content">
<div class="new-headline-sub">
<p><u>YOU GET:</u> 12+ Instructional Videos & Tutorials, Live Trade Setups Forum, Private Follow Up Session, Full Email Support</p>
</div>
</div>


        

		<!-- order box -->
		<div class="order-box">
			<div class="container">
				<h2>Order the Advanced Ichimoku Course Here</h2>
				<div class="clearfix">
					<div class="img"><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2"><img src="<?php echo THEME_IMAGES ?>/img11b.png" width="459" height="270" alt="image description" /></a></div>
					<div class="text-holder">
				
						<a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="btn-add">Add To Cart</a>
						<span class="price"><span class="exprice">*was $399, <strong>NOW</strong> all this</span><br><span class="newpriceline">for a One Time Fee of <span class="newprice">$330</span></span></span>
						<ul class="payment">
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="visa">VISA</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="mastercard">MasterCard</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="american-express">American Express</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="discover">DISCOVER</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="paypal">PayPal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="desc">
				<p><span>NOTE:</span> 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
		</div>
	</div>
<?php get_footer(); ?>