<?php
/*
Template Name: popup
*/
?>

<?php get_header(); ?>
<!-- main content -->
<div id="content" >

    <div class="default-template">

		<?php

		    if(isset($_COOKIE['testApp']))
		        showResulst();

		    if((!isset($_POST) || empty($_POST)) && (!isset($_COOKIE['testApp'])) && (!isset($_GET['start'])))
            {
                echo 'Looking for a more customized learning experience appropriate to your skill level, and topics you want to focus on?
                      What we have created at 2ndSkies Forex is a series of questions that will be able to gauge your skill level, what topics you are wanting to learn, and how we can customize your learning
 	              experience. <br><br>
After answering the next few questions, our program will then give you a suggested list of videos and articles that will be tailored to your skill level for you to study.
                      This will help you streamline and accelerate your learning process by only studying material most appropriate to your learning and trading goals.<br><br>';
                
		echo '<a href="/customized-learning?start=1" class="testAppButton">Start Now</a>';

            }

        if((isset($_GET['start'])))
            showTestQuestions();

    if (isset($_POST["answers"]) && !empty($_POST["answers"]) && (!isset($_COOKIE['testApp'])))
        getRecommendedSet();
?>
    </div>
</div>

<?php get_footer();

function showResulst($setIds = null)
{
    echo '<h1>Recommended Reading and Viewing</h1>';

    if ($setIds !== null)
        showLinks($setIds);

    if (isset($_COOKIE['testApp']['items']))
    {
        showLinks($_COOKIE['testApp']['setIds']);

        $progressBarArr = $_COOKIE['testApp']['items'];
        $totalCount = $_COOKIE['testApp']['totalCount'];
        $ready = 0;
        $left = 0;
?>

        <div id="progress" style="width:500px;border:1px solid #ccc;"></div><div id="information" style="width"></div>
<?php
        foreach($progressBarArr as $pb)
            if($pb == 1)
                $ready++;

        $left = count($progressBarArr)-$ready;
        $percent = intval($ready/$totalCount * 100)."%";// Calculate the percentation

        // Javascript for updating the progress bar and information
        echo '<script language="javascript">
            document.getElementById("progress").innerHTML="<div style=\"width:'.$percent.';background-color:#ddd;\">&nbsp;</div>";
            document.getElementById("information").innerHTML="'.$left.' item(s) left.";
            </script>';
    }

    if(($ready == count($progressBarArr)) && ($progressBarArr && $ready))
    {
        $recommendedCourse = '';
        switch($_COOKIE['testApp']['setIds']){
            case '20': $recommendedCourse = 'Your Recommended Course is Advanced Price Action Course'; break;
            case '21': $recommendedCourse = 'Your Recommended Course is Advanced Ichimoku Course'; break;
        }
        if(!isset($_COOKIE['testApp']['code'])){
        ?>
        <script type="text/javascript">
            $(document).ready(function (){
                    var url = "http://courses.2ndskiesforex.com/courses/callback";
                    $.ajax({
                        type: 'GET',
                        url: url,
                        dataType: 'jsonp',
                        success: function(json){
                            $('#code').html(json);
                            var date = new Date;
                            date.setDate( date.getDate() + 365 );
                            document.cookie = "testApp[code]="+json+"; path=/; expires="+date.toUTCString();
                        }
                        });
                    });
        </script>
        <?php } ?>
        <b>Congratulations! You completed reading and watching the recommended articles and videos.</b><br><br>
        <?=$recommendedCourse?><br>
        <!--There is your Voucher Code for the Recommended Course:<br>
        <div id="code"><?php echo ($_COOKIE['testApp']['code']) ? $_COOKIE['testApp']['code'] : '' ?></div><br>
        <a href="http://www.courses.2ndskies.lh" target="_blank">Buy the Course!</a>-->
<?php
    }

    if($_COOKIE['testApp']['setIds'] == 1)
    {
        global $wpdb;
        $sql = "SELECT * FROM `wp_recommended_sets` WHERE `id`=1;";
        $items = $wpdb->get_results($sql);
        foreach($items as $item) echo $item->text;
    }
}

function showLinks($setIds)
{
    global $wpdb;
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id` IN (".$setIds.");";
    $items = $wpdb->get_results($sql);

    foreach($items as $item)
    {
        $links = explode(',',$item->links);
        $names = explode(';',$item->text);
        $totalItemCount = count($links);

        for($i = 0; $i< $totalItemCount; $i++)
            echo '<p><a href="'.$links[$i].'">'.$names[$i].'</a></p>';
    }
}

function getRecommendedSet()
{
    $id = $_POST['answers'];
    $usersAnswers = $_POST["usersAnswers"];

    global $wpdb;
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id`=".$id.";";
    $sets = $wpdb->get_results($sql);

    foreach($sets as $set)
        if($set->is_last)
        {
            $results = $set->text;
            if($set->links)
            {
                $linksArr = explode(',',$set->links);
                $setIds = $set->question_id;
                writeUsersCookie($linksArr,$setIds);
                showResulst($set->question_id);
            }
           else echo $results;

            if($set->id == 1) writeUsersCookie(0,1);
        }

    if(!isset($sets) || empty($sets) || empty($results))
    {
        (empty($usersAnswers)) ? $usersAnswers = $id : $usersAnswers .= ','.$id;

        $questionNumber = $_POST["questionNumber"] +1;
        showTestQuestions($questionNumber, $usersAnswers);
    }

}

function getRecommendedItems()
{
    global $wpdb;
    $sql = "SELECT * FROM `wp_recommended_sets` WHERE `question_id` IN (".$_POST['usersAnswers'].");";
    $items = $wpdb->get_results($sql);

    $linksArr = array();

    foreach($items as $item)
        $linksArr = array_merge($linksArr,explode(',',$item->links));

    writeUsersCookie($linksArr,$_POST['usersAnswers']);
    showResulst($_POST['usersAnswers']);
}

function showTestQuestions($questionNumber = 0, $usersAnswers = null)
{
    global $wpdb;

    $sql = "SELECT * FROM `wp_test_questions` WHERE `question_number`=".$questionNumber.";";
    $questions = $wpdb->get_results($sql);
    $num_objects = $wpdb->num_rows;

    if($num_objects > 0)
    {  ?>

    <h1>Question <?php echo $questionNumber +1; ?></h1>
    <FORM name ="form" method ="post" action ="/customized-learning/" >

<?php
    $value = 0;
    echo '<div id="question1" class="question-form">';

    foreach($questions as $question)
    {
        if(!$question->parent_id)
            echo '<p>'.$question->question_text.'</p>';
        else
			echo '<div class="row"><label><input type="radio" name="answers" value="'.$question->id.'" checked>  '.$question->question_text.'</label></div>';

        $value ++;
    }

	echo '<div class="btn-holder">
		<input type="hidden" name="questionNumber" value="'.$questionNumber.'">
		<input type="hidden" name="usersAnswers" value="'.$usersAnswers.'">
		<input type="submit" class="testAppButton" value="Next >>">
		</div></div></form>';

    }
    else
        getRecommendedItems();
}


function writeUsersCookie($recommendedItems, $setIds = null)
{
    if(!isset($_COOKIE['testApp']))
    {
        $totalCount = count($recommendedItems);
        $expires = strtotime('next year');

        echo '<script language="javascript">
                    document.cookie = "testApp[totalCount]='.$totalCount.'; path=/; expires='.$expires.'";
                    console.log(document.cookie);
                    </script>';

        if(isset($recommendedItems) && !empty($recommendedItems))
        {
            foreach($recommendedItems as $ri)
                echo '<script language="javascript">
                    document.cookie = "testApp[items]['.$ri.']=0; path=/; expires='.$expires.'";
                    </script>';
        }

        if(isset($setIds) && !empty($setIds))
            echo '<script language="javascript">
                    document.cookie = "testApp[setIds]='.$setIds.'; path=/; expires='.$expires.'";
                    </script>';
    }

}