<?php
/*
Template Name: strategies category
*/
?>
<?php get_header(); ?>

<!-- main -->
<div id="main" class="clearfix">
	<!-- content -->
	<div id="content">
		<!-- breadcrumbs container -->
		<div class="breadcrumbs-container clearfix">
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><?php the_title() ?></li>
				</ul>
			</div>
		</div>
		<!-- posts -->
		<div class="posts">
			<!-- main post -->
            <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                
                global $post;
				
				
                $slug = get_post( $post )->post_name;  
                //die(var_dump($slug));
                $posts = query_posts('category_name='.$slug.'&posts_per_page=5&paged=' . $paged);
                
                $mainPost = $posts[0];
                unset($posts[0]);
                setPostViews($mainPost->ID);
				
                $mainPostVideo = get_post_meta($mainPost->ID, 'embed-video');
            ?>
			<div class="main-post">
				<h1><a href="<?php echo get_permalink( $mainPost->ID );?>"><?php echo $mainPost->post_title; ?></a></h1>
				<!-- meta info -->
				<div class="meta">
					<ul>
						<li><strong class="date"><?php echo get_post_time('F jS, Y',false,$mainPost->ID); ?></strong></li>
						<li>in <?php the_category(', ','', $mainPost->ID); ?></li>
						<li>| <a href="<?php echo get_permalink( $mainPost->ID ).'#comments';?>"><?php echo $mainPost->comment_count; ?> comments</a></li>
					</ul>
				</div>
				<!-- video holder -->
                <?php if($mainPostVideo[0]) { ?>
				<div class="post-video-holder">
					<?php echo $mainPostVideo[0]; ?>
				</div>
                <?php } else { ?>
                    <?php echo get_the_post_thumbnail( $mainPost->ID, array(600,400) ); ?>
                <?php } ?>
				<!-- content of the post -->
				<div class="content">
					<?php preg_match('~^(?>(?><[^>]*>\s*)*[^<]){0,600}(?=\s)~s', $mainPost->post_content, $m);
					$content = preg_replace('/\<img([^>]+)([^>]*)\>/i','',$m[0]); 
					echo $content; ?>..... <a href="<?php echo get_permalink( $mainPost->ID );?>" class="more">Keep reading</a>
				</div>
			</div>
			<!-- other posts -->
			<div class="post-list">
			 <?php  foreach ($posts as $post) { ?>
				<div class="list-item">
					<div class="photo">
						<a href="<?php echo get_permalink( $post->ID );?>"><?php echo get_the_post_thumbnail( $post->ID, array(285,285) ); ?></a>
					</div>
					<!-- meta info -->
					<div class="meta">
						<ul>
							<li><strong class="date"><?php echo $post->post_date; ?></strong></li>
							<li>| <a href="<?php echo get_permalink( $post->ID ).'#comments';?>"><?php echo $post->comment_count; ?> comments</a></li>
						</ul>
					</div>
					<h2><a href="<?php echo get_permalink( $post->ID );?>"><?php echo $post->post_title; ?></a></h2>
				</div>
			<?php  } ?>
			</div>
			<?php wp_pagenavi();?>
		</div>
	</div>
	<!-- sidebar -->
	<?php 
	get_sidebar('recent-popular'); 
	?>  
</div>

<?php get_footer(); ?>
