<div class="top-menu main-nav-search search" id="txtHomeSearch">

<!--	<span class="hidden" class="label"><span aria-hidden="true" class="fa fa-search nav-icon-search dashicons_dashicons-search">&nbsp;</span><span class="cursor"></span></span>-->

<form role="search" method="get" id="searchform" class="search-form" action="<?php echo home_url( '/' ) ?>" >  
	<label class="screen-reader-text hidden" for="s">Search:</label>  
	<input type="text" placeholder="Search" name="s" id="s" />  
	<input type="submit" id="searchsubmit" value="search" />  
</form>
</div>

<script>
	jQuery(document).ready(function($) {
		$('.gobutton').click(function() {
			$('#searchform').submit();
		});

		$(document).click(function() {
			$('.top-menu.search').removeClass('selected');
			$('.close-form').addClass('hidden');
			setTimeout(function() {$('#header .subscribe-form').removeClass('hidden');}, 500);
			setTimeout(function() {$('#header .nav-icon-search').removeClass('hidden');}, 500);
		});

		$('.top-menu.search').click(function(event) {
			$(this).addClass('selected');
			$('#header .subscribe-form').addClass('hidden');
			$('#header .nav-icon-search').addClass('hidden')
			$('.close-form').removeClass('hidden');
			$('.selected .search-input').focus();
			event.stopPropagation();
		});
	});
</script>