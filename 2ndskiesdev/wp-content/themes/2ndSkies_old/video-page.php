<?php
/*
Template Name: Video page
*/
$type = @$_GET['type'];

$tax_query = array(/*
					array(
						'taxonomy' => 'videocategory',
                        'terms' => '',
						'field' => 'term_id',
					)*/
				);

switch ($type) {
    case 'price':
        $tax_query = array(
					array(
						'taxonomy' => 'videocategory',
						'terms' => PriceActionTradingVideos,
						'field' => 'term_id',
					)
				);
        break;
    case 'ichimoku':
        $tax_query = array(
					array(
						'taxonomy' => 'videocategory',
						'terms' => IchimokuCloudTradingVideos,
						'field' => 'term_id',
					)
				);
        break;
    case 'forex':
        $tax_query = array(
					array(
						'taxonomy' => 'videocategory',
						'terms' => ForexTradingTradingPsychologyVideos,
						'field' => 'term_id',
					)
				);
        break;
}

?>
<?php get_header(); ?>
		<!-- main content -->
		<div id="content" class="video-template">
			<!-- breadcrumbs container -->
			<div class="breadcrumbs-container clearfix">
				<div class="breadcrumbs">
					<ul>
						<li><a href="<?php echo home_url(); ?>">Home</a></li>
						<li><?php the_title() ?></li>
					</ul>
				</div>
			</div>
			<!-- promo text -->
			<div class="promo-text clearfix">
				<div class="video-holder">
					<div class="holder">
						<?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
					</div>
				</div>
				<?php the_post(); ?>
				<?php the_content(); ?>
				<div class="share-block clearfix">
					<!-- AddThis Button BEGIN -->
                <a href="http://api.addthis.com/oexchange/0.8/forward/facebook/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2F&amp;pubid=ra-52f4b153538fc7fd" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/facebook.png" height="20" width="20" border="0" alt="Facebook" />
                </a>
                <a href="http://api.addthis.com/oexchange/0.8/forward/twitter/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2F&amp;pubid=ra-52f4b153538fc7fd" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/twitter.png" height="20" width="20" border="0" alt="Twitter" />
                </a>
                <a href="http://www.addthis.com/bookmark.php?source=tbx32nj-1.0&amp;=300&amp;pubid=ra-52f4b153538fc7fd&amp;url=http%3A%2F%2F " target="_blank"  >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/more.png" height="20" width="20" border="0" alt="More..." /></a>
                <!-- AddThis Button END -->
				</div>
			</div>
			<!-- videos -->
			<div id="video-nav" class="videos-block">
				<!-- nav -->
				<div  class="nav">
					<ul>
						<li class="<?php if(!$type) echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?#video-nav"><span>All Videos</span></a></li>
						<li class="<?php if($type=='price') echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?type=price#video-nav"><span>Price Action Trading Videos</span></a></li>
						<li class="<?php if($type=='ichimoku') echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?type=ichimoku#video-nav"><span>Ichimoku Cloud Trading Videos</span></a></li>
						<li class="<?php if($type=='forex') echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?type=forex#video-nav"><span>Forex Trading &amp; Trading Psychology Videos</span></a></li>
					</ul>
				</div>
				<!-- video list -->
				<?php
                
				//$paged = $_GET['paged_custom'];
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts(array( 
				'post_type' => 'videos',
				'posts_per_page' => 9,//9
				'tax_query' => $tax_query,
				'order' => 'DESC',
				'paged' => $paged)
				);
				?>
				<?php if (have_posts()) : ?>
				<div class="video-list clearfix">
					<?php while (have_posts()) : the_post(); ?>
					<div class="list-item">
						<div class="video-holder">
							
                            <?php // get_the_ID() instead $post_id ?>	
                            <?php echo get_the_post_thumbnail(get_the_ID(), array( 288,166 ) );?>
							<a href="#popup-<?php echo $post->ID; ?>" rel="lightbox-opener" class="thumbnail-opener">View video</a>
							<a href="<?php the_permalink();?>" class="post-opener">Read more</a>
							<span class="mask"></span>
							<div class="lightbox-holder">
								<div id="popup-<?php echo $post->ID; ?>">
									<?php 
									$embed = get_post_meta($post->ID, "embed-video", true); 
									if($embed)
										echo $embed;
									else {
										$url = get_post_meta($post->ID, "et_videolink", true);
										preg_match(
												'/[\\?\\&]v=([^\\?\\&]+)/',
												$url,
												$matches
											);
										$id = $matches[1];
										 
										$width = '800';
										$height = '600';
										echo '<object width="' . $width . '" height="' . $height . '"><param name="movie" value="http://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed></object>';
										//var_dump($text);
										//echo $text;
									}
									?>
								</div>
							</div>
						</div>
						<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
					</div>
					<?php endwhile; ?>
				</div>
				<!-- pager -->
				<?php wp_pagenavi();?>
				<?php endif; ?>
			</div>
		</div>
		<!-- main banner -->
		<div class="main-banner">
			<a href="/forex-courses/"><img src="<?php echo THEME_IMAGES ?>/2ndSkies-Courses-Ad-Footer.png" width="960" height="126" alt="2ndskies Forex Courses" /></a>
		</div>
<?php get_footer(); ?>