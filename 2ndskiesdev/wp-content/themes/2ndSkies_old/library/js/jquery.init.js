// accordion menu init
jQuery(document).ready(function ($){
	$('ul.accordion').slideAccordion({
		opener: 'a.opener',
		slider: '.slide',
		animSpeed: 300
	});
});

// transparent iframe
jQuery(document).ready(function ($){
	$('iframe').each(function(){
		var url = $(this).attr("src")
		$(this).attr("src",url+"?autohide=1&amp;rel=0&amp;showinfo=0&amp;wmode=transparent")
	});
});


jQuery(document).ready(function ($){
	// Target your .container, .wrapper, .post, etc.
	$(".content").fitVids();
	$(".promo-video-block").fitVids(); 
	$(".v-pop").fitVids(); 
	$(".default-template").fitVids(); 
});


// content tabs init
jQuery(document).ready(function ($){
	$('ul.tabset').contentTabs({
		addToParent: true,
		animSpeed: 400,
		effect: 'fade',
		tabLinks: 'a'
	});
});

// remove scroll on click
jQuery(document).ready(function ($){
	$('#sidebar .tabs .post-list+.btn-more').click(function(e) {
		e.preventDefault();
		$(this).hide();
		$('#sidebar .post-list').css({'max-height': '9999px'});
	});
});





jQuery(document).ready(function ($){
	$('.popup-video').magnificPopup({
		type:'inline',
		closeBtnInside: true
	});
});



// cycle scroll gallery init
jQuery(document).ready(function ($){
	$('.slideshow').scrollAbsoluteGallery({
		mask: '.mask',
		slider: '.slideset',
		slides: '.slide',
		btnPrev: 'a.btn-prev',
		btnNext: 'a.btn-next',
		stretchSlideToMask: true,
		maskAutoSize: true,
		autoRotation: false,
		switchTime: 3000,
		animSpeed: 500
	});
});

jQuery(document).ready(function ($){
	$('.slideshow-testimonials').scrollAbsoluteGallery({
		mask: '.mask',
		slider: '.slideset',
		slides: '.slide',
		btnPrev: 'a.btn-prev',
		btnNext: 'a.btn-next',
		stretchSlideToMask: true,
		maskAutoSize: true,
		autoRotation: true,
		switchTime: 10000,
		animSpeed: 500
	});
});

// full clickable area
jQuery(document).ready(function ($){
	$('.chapter-list .list-item').click(function(){
		window.location=$(this).find("a").attr("href"); return false;
	});
});


jQuery(document).ready(function ($){
	lib.each(lib.queryElementsBySelector('#nav ul'), function(){
		new TouchNav({
			navBlock: this
		});
	});
});

jQuery(document).ready(function ($){
	$('.btn-subscribe').on('click', function(){
		$(this).hide();
		$('.subscribe-form form').slideDown();
	})
});

// initPositionDrop

jQuery(document).ready(function ($){
	function PositionDrop() {
		var navList = $('.center-container');
		var lis = navList.find('#nav ul> li');
		var navW = navList.width();
		
		lis.each(function() {
			var li = $(this);
			var drop = li.find('> .sub-menu');
			var dropW = drop.width();
			
			if(li.offset().left + dropW > navW) {
				li.addClass('right-side');
			}
			else{
				li.removeClass('right-side');
			}
		});
	}
	PositionDrop();
	$(window).resize(PositionDrop);
});

/* Fancybox overlay fix */
jQuery(document).ready(function($){
	// detect device type
	var isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
	var isWinPhoneDevice = navigator.msPointerEnabled && /MSIE 10.*Touch/.test(navigator.userAgent);

	if(!isTouchDevice && !isWinPhoneDevice) {
		// create <style> rules
		var head = document.getElementsByTagName('head')[0],
			style = document.createElement('style'),
			rules = document.createTextNode('#fancybox-overlay'+'{'+
				'position:fixed;'+
				'top:0;'+
				'left:0;'+
			'}');

		// append style element
		style.type = 'text/css';
		if(style.styleSheet) {
			style.styleSheet.cssText = rules.nodeValue;
		} else {
			style.appendChild(rules);
		}
		head.appendChild(style);
	}
});

jQuery(document).ready(function($){
	$('input, textarea').placeholder();
});

jQuery(document).ready(function($){
	$('#nav .opener').click(function(e) {
		e.preventDefault();
		$('#nav').toggleClass('active');
	});
});