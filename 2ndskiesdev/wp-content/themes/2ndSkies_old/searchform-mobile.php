<form role="search" method="get" id="searchform-mobile" class="search-form" action="<?php echo home_url( '/' ) ?>" >  
	<label class="screen-reader-text hidden" for="sm">Search:</label>  
	<input type="text" placeholder="Search..." name="sm" id="sm" />  
	<input type="submit" id="searchsubmit-mobile" value="search" />  
</form>