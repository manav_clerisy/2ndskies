<?php
/*
Template Name: No Sidebar Page
*/
?>
<?php get_header(); ?>

<div id="full" class="clearfix">

    <div id="content">

        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container -->

        <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

        <div class="default-template">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div><!-- .default-template -->

        <?php endwhile; ?>

        <?php else : ?>

        <div class="default-template">
            <h1>Not Found</h1>
            <p>Sorry, but you are looking for something that isn't here.</p>
        </div><!-- .default-template -->

        <?php endif; ?>

    </div><!-- #content -->
    
</div><!-- #main -->

<?php get_footer(); ?>