<?php
/*
Template Name: Online Courses
*/
?>
<?php get_header(); ?>
	<div id="content" class="inner">
		<div class="breadcrumbs-container clearfix">
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><?php the_title() ?></li>
				</ul>
			</div>
		</div>
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>