<?php
/*
Template Name: Advance Ichimoku Course
*/
?>
<?php get_header(); ?>
	<!-- main content -->
	<div id="content">

		<!-- promo video -->
		<div class="promo-video-block">
			<?php the_post(); ?>
			<?php the_content(); ?>
			<div class="video-holder__  video-holder-new">
				<?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
			</div>
		</div>
		<!-- promo text -->
		<div class="promo-text clearfix">
			<div class="clearfix">
				<div class="alignleft">
					<a href="#"><img src="<?php echo THEME_IMAGES ?>/img14.png" width="413" height="267" alt="image description" /></a>
				</div>
				<div class="text-holder">
					<h2>Advanced Ichimoku - Changing the Game</h2>
					<p>There is not much information available on the Ichimoku Cloud. There is only one book on the topic written for westerners, and it has rarely satisfied traders’ desire to understand the technique on an advanced level. Except for a few websites, much of the information from the original texts is unavailable to clients unless you write Japanese Hiragana and Kanji.</p>
					<p>This course changes all that. Chris Capre, one of the foremost experts on Ichimoku Cloud trading in the world. Chris  has built Ichimoku Systems for investment firms and consulted with various financial institutions using his proprietary Ichimoku methods.</p>
					<p>Testing strategy after strategy using proprietary analytics, this course gives traders who are passionate about the Ichimoku Cloud an advanced way to take Ichimoku trading to the next level.</p>
				</div>
			</div>
		</div>
		<h2 class="clearfix"><span class="col-2">What You Get</span> <span class="col-2 mobile-hide">What We Cover</span></h2>
		<!-- table list -->
		<div class="table-list alt">
			<ul>
				<li>Unique High Probability Ichimoku Strategies</li>
				<li>Quantitative Ichimoku Data &amp; Price Behavior</li>
				<li>Full Email Support and Access to Chris Capre</li>
				<li>Analysis of Live Trade Setups</li>
				<li>Access to Ichimoku Trade Setups</li>
				<li>Access to Ichimoku Traders Forum</li>
				<li>Trading Journal and Performance Worksheet</li>
				<li>How to Prepare For Your Trading Day</li>
				<li>Ongoing Lessons and Updates</li>
			</ul>
			<h2 class="mobile-show">What We Cover</h2>
			<ul>
				<li>The One Article You Must Read</li>
				<li>The Tenkan OCx Strategy (1hr charts)</li>
				<li>Inverse TKx Trading (4hr and Daily charts)</li>
				<li>Enhanced TKx Trading (4hr and Daily charts)</li>
				<li>Advanced Kumo Breaks (1hr and 4hr Charts)</li>
				<li>Kijun ATR Cross (5min charts)</li>
				<li>WX Shadow Trading (3min charts)</li>
				<li>Reading the Chikou Span (all time frames)</li>
				<li>Proper Risk Management</li>
			</ul>
		</div>
		<!-- testimonials -->
		<div class="testimonials-block">
			<h2>Here’s What Students Are Saying About The Course…</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>Just to let you know I think the course is fantastic. It has improved my trading and understanding of Ichimoku tenfold. Instead of scalping for 5 to 10 pips and letting bad trades run, I am now seeing gains of 50+ pips consistently.</q>
						<cite>Phil, Australia &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I just want to put on record my thanks for the quality of the course content. It represents stupendous value and you've made yourself available on a constant basis to guide us forwards.</q>
						<cite>Steve, Singapore &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>It is clearly evident you are genuinely interested in helping people. Thanks for the encouragement Chris and the great course. You truly are the mentor.</q>
						<cite>Eric, UK &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
			</div>
		</div>
		<h2>How Does The Course Work?</h2>
		<div class="shadow-box">
			<div class="tabs clearfix">
				<ul class="tabset">
					<li><a href="#tab-1">Before You Begin </a></li>
					<li><a href="#tab-2">What You Will Learn</a></li>
					<li class="active"><a href="#tab-3">Also Included With The Course</a></li>
					<li><a href="#tab-4">How Does The Course Work?</a></li>
				</ul>
				<div class="tab-container">
					<div id="tab-1">
						<p>This is the most advanced Ichimoku course available anywhere. There is simply no other course on the subject with this level of advanced material on the Ichimoku Cloud. If you are still learning the Ichimoku Cloud, we suggest reading Learning the Ichimoku Cloud to build your base of knowledge and gain an understanding of the important concepts before starting the course. </p>
					</div>
					<div id="tab-2">
						<p>This course has one purpose: to teach you advanced rule-based systems on the Ichimoku Cloud, all of which are generally unavailable.
You will learn six advanced proprietary rule-based systems with the Ichimoku Cloud. Each system has precise rules for entries and exits, finding targets, setting stops, and identifying the proper trade setups. These systems are built upon proprietary models never before published.
We are also constantly testing and adding new systems and material to the course. </p>
					</div>
					<div id="tab-3">
						<p>Sign up for the Advanced Ichimoku course, and you will also receive:</p>
						<ol>
							<li><strong>One private follow up mentoring session with Chris Capre</strong> – After completing the course, you will receive a private follow-up mentoring session to leverage what you’ve learned in the course to enhance your strengths and minimize your weaknesses.</li>
							<li><strong>Lifetime membership to the Private Forum</strong> – Gain access to a library of knowledge on our forum.</li>
							<li><strong>Full email support</strong> – You’ll get full email access to Chris Capre, to ask any questions about the course or material. We pride ourselves on our outstanding customer support, and we respond to emails every day, including weekends and holidays.</li>
						</ol>
					</div>
					<div id="tab-4">
						<p>The course is also available on video and is accessible 24 hours a day, so you can learn when it’s convenient for you. Once you sign up, you will receive login information to access the course and learn at your own pace. </p>
					</div>
				</div>
			</div>
		</div>
		<span class="content-border clearfix"></span>
		<h2>FAQ</h2>
		<div class="shadow-box clearfix">
			<ul class="accordion clearfix">
				<?php query_posts( 'cat=13' );?>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); ?>
					<li>
						<a href="#" class="opener"><?php the_title();?></a>
						<div class="slide">
							<?php the_content();?>
						</div>
					</li>
					<?php endwhile; ?>
				<?php endif; ?>
			</ul>
		</div>
		<!-- order box -->
		<div class="order-box">
			<div class="container">
				<h2>Order the Advanced Ichimoku Course Here</h2>
				<div class="clearfix">
					<div class="img"><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2"><img src="<?php echo THEME_IMAGES ?>/img11b.png" width="459" height="270" alt="image description" /></a></div>
					<div class="text-holder">
			
						<a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="btn-add">Add To Cart</a>
						<span class="price"><span class="exprice">*was $399, <strong>NOW</strong> all this</span><br><span class="newpriceline">for a One Time Fee of <span class="newprice">$325</span></span></span>
						<ul class="payment">
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="visa">VISA</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="mastercard">MasterCard</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="american-express">American Express</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="discover">DISCOVER</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=2" class="paypal">PayPal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="desc">
				<p><span>NOTE:</span> 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
		</div>
	</div>
<?php get_footer(); ?>