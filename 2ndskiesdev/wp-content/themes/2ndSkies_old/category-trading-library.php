<?php
/*
Template for Trading Library cat
*/
$type = @$_GET['type'];

$tax_query = array(
					array(
						'taxonomy' => 'category',
                        'terms' => 'forex-strategies',
						'field' => 'slug',
					)
				);

switch ($type) {
    case 'price-action-trading':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'price-action-trading-library',
						'field' => 'slug',
					)
				);
        break;
    case 'ichimoku-cloud-trading':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'ichimoku-cloud-trading',
						'field' => 'slug',
					)
				);
        break;
    case 'trading-mindset-risk-management':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'trading-mindset-risk-management',
						'field' => 'slug',
					)
				);
        break;
}

?>
<?php get_header(); ?>
		<!-- main content -->
		<div id="content" class="video-template">
			<!-- breadcrumbs container -->
			<div class="breadcrumbs-container clearfix">
				<div class="breadcrumbs">
					<ul>
						<li><a href="<?php echo home_url(); ?>">Home</a></li>
						<li><?php //the_title() ?>Trading Library</li>
					</ul>
				</div>
			</div>
			<!-- promo text -->
			<div class="promo-text clearfix">
            <h1><?php single_cat_title('',true); ?></h1>
            
            <?php echo category_description(); ?>
				<?php //the_post(); ?>
				<?php //the_content(); ?>
				<div class="share-block clearfix">
					<!-- AddThis Button BEGIN -->
                <a href="http://api.addthis.com/oexchange/0.8/forward/facebook/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2F&amp;pubid=ra-52f4b153538fc7fd" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/facebook.png" height="20" width="20" border="0" alt="Facebook" />
                </a>
                <a href="http://api.addthis.com/oexchange/0.8/forward/twitter/offer?pco=tbx32nj-1.0&amp;url=http%3A%2F%2F&amp;pubid=ra-52f4b153538fc7fd" target="_blank" >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/twitter.png" height="20" width="20" border="0" alt="Twitter" />
                </a>
                <a href="http://www.addthis.com/bookmark.php?source=tbx32nj-1.0&amp;=300&amp;pubid=ra-52f4b153538fc7fd&amp;url=http%3A%2F%2F " target="_blank"  >
                    <img src="http://cache.addthiscdn.com/icons/v1/thumbs/32x32/more.png" height="20" width="20" border="0" alt="More..." /></a>
                <!-- AddThis Button END -->
				</div>
			</div>
			<!-- videos -->
			<div class="videos-block">
				<!-- nav -->
				<div class="nav">
					<ul>
						<li class="<?php if(!$type) echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?"><span>All</span></a></li>
						<li class="<?php if($type=='price-action-trading') echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?type=price-action-trading"><span>Price Action Trading</span></a></li>
						<li class="<?php if($type=='ichimoku-cloud-trading') echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?type=ichimoku-cloud-trading"><span>Ichimoku Cloud Trading</span></a></li>
						<li class="<?php if($type=='trading-mindset-risk-management') echo 'active';?>"><a href="<?php echo home_url( 'trading-library' );?>?type=trading-mindset-risk-management"><span>Trading Mindset &amp; Risk Management</span></a></li>
					</ul>
				</div>
				<!-- video list -->
				<?php
                
				//$paged = $_GET['paged_custom'];
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts(array( 
					'post_type' => 'post',
					'posts_per_page' => 9,
					'tax_query' => $tax_query,
					'order' => 'DESC',
					'paged' => $paged
				));
				?>
				<?php if (have_posts()) : ?>
				<div class="video-list trending-list clearfix">
					<?php while (have_posts()) : the_post(); ?>
					<div class="list-item">
						<div class="video-holder">
							
                            <?php // get_the_ID() instead $post_id ?>	
                            
							<a href="<?php the_permalink();?>" class="trend-post-opener">
								<?php echo get_the_post_thumbnail(get_the_ID(), array( 288,166 ) );?>
                            </a>

						</div>
						<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
					</div>
					<?php endwhile; ?>
				</div>
				<!-- pager -->
				<?php wp_pagenavi();?>
				<?php endif; ?>
			</div>
		</div>
		<!-- main banner -->
		<div class="main-banner">
			<a href="/forex-courses/"><img src="<?php echo THEME_IMAGES ?>/2ndSkies-Courses-Ad-Footer.png" width="960" height="126" alt="2ndskies Forex Courses" /></a>
		</div>
<?php get_footer(); ?>