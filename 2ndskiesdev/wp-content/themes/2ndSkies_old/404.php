<?php get_header(); ?>

<div id="main" class="clearfix">
	<div id="content">
    
		<div class="breadcrumbs-container clearfix">
			<div class="breadcrumbs">
				<ul>
					<li><a href="<?php echo home_url(); ?>">Home</a></li>
					<li><?php the_title(); ?></li>
				</ul>
			</div><!-- .breadcrumbs -->
		</div><!-- .breadcrumbs-container -->

		<div class="default-template">
			<h1>404 / Page Not Found</h1>
			<p>The page you requested could not be found. Try refining your search, or use the navigation above to locate the post.</p>
		</div><!-- .default-template -->

	</div><!-- #content -->
</div><!-- #main -->
<?php get_footer(); ?>