<div id="sidebar">
		<!-- banner -->
		<div class="banner">
			<a href="/forex-courses/"><img src="<?php echo THEME_IMAGES ?>/2ndSkies-Courses-Ad-Sidebar.png" width="285" height="126" alt="2ndSkies Trading Courses" /></a>
		</div>
		<!-- tabs -->
		<div class="tabs">
			<ul class="tabset">
				<li class="active"><a href="#sidebar-tab01">Recent</a></li>
				<li><a href="#sidebar-tab02">Popular</a></li>
			</ul>
			<div class="tabs-scroll">
				<div id="sidebar-tab01">
					<div class="post-list">
						<?php
							global $post;
							$post_type = $post->post_type;
							$cat = get_the_category($post->ID);
							$slug = $cat[0]->name;
							
							if(!$slug)
								$slug = $post->post_name;
							
							if($post_type == 'videos')
								$posts = get_posts('post_type=videos&orderby=post_date&order=DESC&posts_per_page=20');
							else
								$posts = get_posts('category_name='.$slug.'&post_type=post&orderby=post_date&order=DESC&posts_per_page=20');
							
							foreach ($posts as $post) { ?>							
							<div class="list-item">
								<div class="photo"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(65, 50)); ?></a></div>
								<div class="text-holder">
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<div class="meta-info">
										<em class="date"><?php echo mysql2date('j M Y', $post->post_date); ?></em>
										<span class="ccomments"> <?php echo $post->comment_count; ?>  Comments</span>
									</div>
								</div>
							</div>
							<?php
							}
							wp_reset_query();
						?>	
					</div>
					<?php if(count($posts) > 10) { ?>
					<a href="#" class="btn-more">LOAD MORE</a>
					<?php } ?>
				</div>
				<div id="sidebar-tab02">
					<div class="post-list">
						<?php    
							if($post_type == 'videos')
								$posts = get_posts('post_type=videos&meta_key=post_views_count&orderby=meta_value_num&order=DESC&posts_per_page=20');
							else
								$posts = get_posts('category_name='.$slug.'&meta_key=post_views_count&orderby=meta_value_num&order=DESC&posts_per_page=20');
							foreach ($posts as $post) { ?>
							<div class="list-item">
								<div class="photo"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(65, 50)); ?></a></div>
								<div class="text-holder">
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<div class="meta-info">
										<em class="date"><?php echo mysql2date('j M Y', $post->post_date); ?></em>
										<span class="ccomments"> <?php echo $post->comment_count; ?>  Comments</span>
									</div>
								</div>
							</div>
							<?php
							}
							wp_reset_query();
						?>
					</div>
					<?php if(count($posts) > 10) { ?>
					<a href="#" class="btn-more">LOAD MORE</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>