<?php
/*
Template for forex-videos category
*/
$type = @$_GET['type'];

$tax_query = array(
					array(
						'taxonomy' => 'category',
                        'terms' => 'forex-videos',
						'field' => 'slug',
					)
				);

switch ($type) {
    case 'price':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'price-action-trading-videos',
						'field' => 'slug',
					)
				);
        break;
    case 'ichimoku':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						'terms' => 'ichimoku-cloud-trading-videos',
						'field' => 'slug',
					)
				);
        break;
    case 'forex':
        $tax_query = array(
					array(
						'taxonomy' => 'category',
						//'terms' => 'forex-trading-trading-psychology-videos',
						'terms' => 'forex-trading-psychology-videos',
						'field' => 'slug',
					)
				);
        break;
}

?>

<?php get_header(); ?>

    <div id="content" class="video-template">

        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php single_cat_title('',true); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container .clearfix-->
        
        
		<div class="promo-text clearfix">
        
            <div class="video-holder">
                <div class="holder">
                    <?php echo $embed = get_post_meta(4, "embed-video", true); ?>
                </div><!-- holder -->
            </div><!-- video-holder -->
            
            <div class="text-holder">
            <h1><?php single_cat_title('',true); ?></h1>
            
            <?php echo category_description(); ?>
            </div>
            
			<?php //the_post(); ?>
            <?php //the_content(); ?>
            
            
			<?php //if(!$type) { echo '<h1>This is ALL tab conetnt</h1><p>ALL Lorem Ipsum... all tab</p>'; } ?>
            <?php //if($type=='price') { echo '<h1>This is price tab content</h1><p>Price Lorem Ipsum... price tab</p>'; } ?>
            <?php //if($type=='ichimoku') { echo '<h1>This is ichimoku tab content</h1><p>Ichimoku Lorem Ipsum... ichimoku tab</p>'; } ?>
            <?php //if($type=='forex') { echo '<h1>... and this is forex tab content</h1><p>Forex Lorem Ipsum... forex tab</p>'; } ?>

		</div><!-- promo text -->
        
        <div id="video-nav" class="videos-block">

			<div class="nav">
                <ul>
                    <li class="<?php if(!$type) echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?#video-nav"><span>All Videos</span></a></li>
                    <li class="<?php if($type=='price') echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?type=price#video-nav"><span>Price Action Trading Videos</span></a></li>
                    <li class="<?php if($type=='ichimoku') echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?type=ichimoku#video-nav"><span>Ichimoku Cloud Trading Videos</span></a></li>
                    <li class="<?php if($type=='forex') echo 'active';?>"><a href="<?php echo home_url( 'forex-videos' );?>?type=forex#video-nav"><span>Forex Trading &amp; Trading Psychology Videos</span></a></li>
                </ul>
			</div><!-- .nav -->
            
			<?php      
                //$paged = $_GET['paged_custom'];
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                query_posts(array( 
                    'post_type' => 'post',
                    'posts_per_page' => 9,
                    'tax_query' => $tax_query,
                    'order' => 'DESC',
                    'paged' => $paged
                ));
            ?>
            
			<?php if (have_posts()) : ?>
            
			<div class="video-list clearfix_">
<!--            <div class="video-list packery js-packery_">-->
				<?php while (have_posts()) : the_post(); ?>
                
				<div class="list-item">
                
					<div class="video-holder">
                 		<?php // get_the_ID() instead $post_id ?>	
                 		<?php echo get_the_post_thumbnail(get_the_ID(), array( 288,166 ) );?>
						<a href="#popup-<?php echo $post->ID; ?>" class="thumbnail-opener lightbox-opener_ popup-video">View video</a>
						<a href="<?php the_permalink();?>" class="post-opener">Read more</a>
						<span class="mask"></span>
                        
						<div class="lightbox-holder">
                            <div id="popup-<?php echo $post->ID; ?>" class="white-popup v-pop">
							<?php 
                                $embed = get_post_meta($post->ID, "embed-video", true); 
                                if($embed)
                                    echo $embed;
                                else {
//                                    $url = get_post_meta($post->ID, "et_videolink", true);
//                                    preg_match(
//                                            '/[\\?\\&]v=([^\\?\\&]+)/',
//                                            $url,
//                                            $matches
//                                        );
//                                    $id = $matches[1];
//                                     
//                                    $width = '800';
//                                    $height = '600';
                                   // echo '<object width="' . $width . '" height="' . $height . '"><param name="movie" value="http://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' . $id . '&amp;hl=en_US&amp;fs=1?rel=0" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="' . $width . '" height="' . $height . '"></embed></object>';

                                    //var_dump($text);
                                    //echo $text;
									
									$video_embed_code = hybrid_media_grabber( array( 'type' => 'video' ) );
									echo $video_embed_code;
                            		}
							?>
                            </div><!-- #popup-<?php echo $post->ID; ?>-->
						</div><!-- .lightbox-holder-->
                            
					</div><!-- .video-holder-->
                    
					<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                        
				</div><!-- .list-item -->
                
				<?php endwhile; ?>
                
			</div><!-- .video-list .clearfix-->
            

			<?php wp_pagenavi();?>
			<?php endif; ?>
            
		</div><!-- .promo-text .clearfixr -->
        
	</div><!-- .content -->
        
    
    <div class="main-banner">
        <a href="/forex-courses/"><img src="<?php echo THEME_IMAGES ?>/2ndSkies-Courses-Ad-Footer.png" width="960" height="126" alt="2ndskies Forex Courses" /></a>
    </div><!-- .main-banner -->
        
<?php get_footer(); ?>