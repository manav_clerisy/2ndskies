<?php
/*
Template Name: Pro Forex Trading Course
*/
?>
<?php get_header(); ?>

	<!-- main content -->
	<div id="content">

		<!-- promo video -->
		<div class="promo-video-block">
			<?php the_post(); ?>
			<?php the_content(); ?>
			<div class="video-holder_ video-holder-new">
				<?php echo $embed = get_post_meta($post->ID, "embed-video", true); ?>
			</div>
		</div>
		<!-- promo text -->
		<div class="promo-text clearfix">
			<h2>Pro Forex Trading</h2>
			<div class="clearfix">
				<div class="alignright">
					<a href="#"><img src="<?php echo THEME_IMAGES ?>/img09.png" width="474" height="273" alt="image description" /></a>
				</div>
                <div class="text-holder">
				<p>These techniques are not complicated or difficult to learn. All of the strategies, systems, tactics, and methods you will learn are proprietary, and some of the quantitative analysis you will learn has never been published. This course is designed to give you an edge while keeping your trading simple. In this course, you will learn the shadow systems, our oldest systems to date (since 2004). They are incredibly robust yet simple and will help you trade intraday momentum moves</p>
				<p>EURUSD     GBPUSD     EURGBP AUDUSD     NZDUSD      Spot Silver vs. USD (XAGUSD – coming soon)</p>
				<p>We just added our Volatility Striker System on the 1hr time frame to trade AUDUSD &amp; NZDUSD. Also added our Sniper Shadow System on the 3min time frame</p>
				<p>Learn rule-based systems that make it easy to enter and exit on any trade.</p>
                </div>
			</div>
		</div>
		<h2>What We Cover</h2>
		<!-- table list -->
		<div class="table-list">
			<ul>
				<li><b>Newly Added</b> - 6NT Pullback System</li>
				<li>RS3 Pivot Breakout System</li>
				<li><b>Newly Added</b> - 3min Sniper Shadow System</li>
				<li>Quantitative Rule-Based Strategies and Data</li>
				<li><b>Newly Added</b> - Volatility Striker System (1hr)</li>
				<li>Quantitative Pivot Point Data over the last 10 years</li>
				<li>Our Shadow Systems on the 1,2 and 4hr time frames</li>
				<li>Advanced Pbar System for Trading Tops and Bottoms</li>
				<li>High Probability Intraday Momentum/Trending Systems</li>
				<li>BR Trading</li>
				<li>Advance Your Pattern Recognition Skills</li>
				<li>3 Dimensional Risk-Reward Models</li>
				<li>How to Gauge Your Growth</li>
				<li>Full Email Access to Chris Capre</li>
				<li>How to Prepare for your Trading Day</li>
				<li>Private Follow Up Session with Chris Capre</li>
				<li>Know When to Trade and When Not To</li>
				<li>Pressure, Breakouts and with Trend Trading</li>
				<li>Lifetime Membership</li>
				<li>Ongoing Lessons and Free Updates</li>
			</ul>
		</div>
		<!-- testimonials -->
		<div class="testimonials-block">
			<h2>Here’s What Students Are Saying About The Course…</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>I have had my head in the Shadow System and your webinars.  I guess I am getting my PhD from Capre University.  Anyway its going great...been trading live and watching price action with the Shadow - just really nailing it down.  The results are great so far.  The impulsive/corrective video is like a bible of understanding price action.</q>
						<cite>Chris H, US &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I made 3% in 3 days since obtaining the courses, they are excellent...I appreciate what you have done for me, even in this little time, I feel like with hard work, I can actually make this work consistently :)</q>
						<cite>Ken T, Australia &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
				<div class="list-item">
					<blockquote>
						<q>I am so happy that I found you and happy to learn a lot from you, now and next years. You were born to trade, teach and educate. Keep going your great mission.</q>
						<cite>Florin, Canada &ndash; Course Member</cite>
					</blockquote>
					<img src="<?php echo THEME_IMAGES ?>/img10.png" width="66" height="66" alt="image description" />
				</div>
			</div>
		</div>
		<!-- including in the course -->
		<h2>Also Included With The Course</h2>
		<p>Sign up for the Pro Forex Trading course, and you will also receive:</p>
		<!-- numeric list -->
		<div class="numeric-list">
			<ol>
				<li>One private follow-up mentoring session with Chris Capre – After completing the course, you will receive a private follow-up mentoring session to leverage what you’ve learned in the course to enhance your strengths and minimize your weaknesses.</li>
				<li>Lifetime membership to the Private Forum – Get access to a library of knowledge on the forum.</li>
				<li>Full email support – If you have questions about the course or material, you’ll have full email access to Chris Capre. We pride ourselves on our outstanding customer support, and we respond to emails nearly 365 days a year, including weekends and holidays.</li>
			</ol>
		</div>
		<!-- order box -->
		<div class="order-box">
			<div class="container">
				<h2>Order the Pro Forex Trading Course Here</h2>
				<div class="clearfix">
					<div class="img"><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3"><img src="<?php echo THEME_IMAGES ?>/img11c.png" width="459" height="270" alt="image description" /></a></div>
					<div class="text-holder">
				
						<a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3" class="btn-add">Add To Cart</a>
						<span class="price"><span class="exprice">*was $399, <strong>NOW</strong> all this</span><br><span class="newpriceline">for a One Time Fee of <span class="newprice">$325</span></span></span>
						<ul class="payment">
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3" class="visa">VISA</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3" class="mastercard">MasterCard</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3" class="american-express">American Express</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3" class="discover">DISCOVER</a></li>
							<li><a href="http://courses.2ndskiesforex.com/courses/startPayment?id=3" class="paypal">PayPal</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="desc">
				<p><span>NOTE:</span> 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
		</div>
	</div>
    

<?php get_footer(); ?>