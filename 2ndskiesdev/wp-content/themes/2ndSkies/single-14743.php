<?php
/*
Template Name: Self Image
*/
global $authordata;
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
    <head>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
        <?php wp_head(); ?>
        <script src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script>
            $.noConflict();
            // Code that uses other library's $ can follow here.
        </script>
        <!--[if (lt IE 9) & (!IEMobile)]>
            <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/html5shiv-3.7.2.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/style.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_site_url(); ?>/wp-includes/css/dashicons.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style-self-image-templ.min.css" />
        <!--[if (lt IE 9) & (!IEMobile)]>
            <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/style-self-image-templ.css" />
            <script src="<?php echo get_template_directory_uri(); ?>/js/fallbacks/respond-1.4.2.min.js"></script>
        <![endif]-->
        <script src="<?php echo get_template_directory_uri(); ?>/js/helpers/bootstrap.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/app-self-image-templ.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/library/js/jquery.sharrre.min.js"></script>
        <script type="text/javascript">WebFontConfig = {
                    google: { families: [ 'Open Sans:400,300,400italic,600:latin', 'Open Sans:300italic,400italic,600italic,300,400,600:latin,latin-ext', 'Source Sans Pro:400,400italic,700:latin', 'Open Sans:300,400,600,700:latin' ] }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();</script>
        <!-- Start Visual Website Optimizer Asynchronous Code -->
        <script type='text/javascript'>
        var _vwo_code=(function(){
        var account_id=61633,
        settings_tolerance=2000,
        library_tolerance=2500,
        use_existing_jquery=false,
        // DO NOT EDIT BELOW THIS LINE
        f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
        </script>
        <!-- End Visual Website Optimizer Asynchronous Code -->
    </head>
    <body>
        <header class="header">
            <div class="container">
                <ul class="header--list l-stacked l-align-center">
                    <li>
                        <a href="http://2ndskiesforex.com/" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo-dark.png" alt=""></a>
                    </li>
                </ul>
            </div><!-- /.container -->
        </header><!-- /.header -->
        <main class="main">
            <section class="section-hero" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/self-image/section-hero--bg.jpg')">
                <div class="container l-align-center">
                    <h1 class="title">Why the Self-Image & Comfort Zone Determine<br />Your Success in Trading</h1>
                    <h2 class="subtitle">Grab the popcorn as I'm going back in time, a long way back<br />(approximately 600M years).</h2>
                </div>
            </section><!-- /.section-hero -->
            <section class="section section-one">
                <div class="container">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-one.png" alt="" class="image" />
                    </div>
                    <div class="col__right col__typography">
                        <p><strong>I'm going to tell you a brief story about your central nervous system.
                            <br /><br />
                            Why?
                            <br /><br />
                            Because as your brain evolved over the last few million years affects your success (or struggles) in trading today.</strong>
			    <br /><br />
                            Then I'm going to talk about <b>EDN</b>, or <b>Experience Dependent Neuroplasticity</b> and how this builds the foundation for creating trading success (or failure) in your brain.
                            <br /><br />
                            From here I'll get into the <b>self-image</b>, what this is, and why you keep making the same mistakes.
                            <br /><br />
                            I'll expand on this by getting into the <b>comfort zone</b> and how you can expand this to increase your trading performance.</p>
                    </div>
                    <div class="clear"></div>
                    <div class="section--heading" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/self-image/section-one--heading.jpg')">
                        But back to that period of around 600 million years ago.
                        <ul class="history_route l-stacked">
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                </div>
            </section><!-- /.section-one -->
            <section class="section section-two">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>From the First to Apes</h3>
                        <p>Around <b>600 million years</b> ago, the very first nervous system was born. It was quite crude in nature as it could only respond and react to basic stimuli in the environment.
                            <br /><br />
                            Another <b>400 or so million years</b> passed before the first mammals arrived on this earth with primates coming into the planet's history about 60 million years ago.
                            <br /><br />
                            About <b>2.5 million years</b> ago from today, some of our early ancestors came about (Homo Habilis) where they could make basic tools out of stone. The very concept of 'leverage' we use today was just being introduced to our early fore-fathers.
                            <br /><br />
                            Fast forward to about <b>200K years</b> ago and the clever ape (Homo Sapiens) started to emerge.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-two.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-two -->
            <section class="section section-three">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-three.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>Hunter-Gatherers, Small Groups<br />and the 1 in 8 ratio</h3>
                        <p>Only 10,000 years ago did we shift from hunter-gatherer wandering groups to create small bands and our first farming collectives.
                            <br /><br />
                            During this shift, on average <b>1 in 8 men died</b> from protecting their families and communities. Contrast this to the 20th century, and the number is 1 in 100.
                            <br /><br />
                            Now with that small story of our evolutionary history, why does this matter for me as a trader?
                            <br /><br />
                            Because even though our brain tripled in volume over the last several million years, most of these experiences have an impact on our brains today.
                            <br /><br />
                            None of us are generally protecting our families from our neighbors, yet we still have the same wiring in our brain from the last 100 millenia or so.
                            <br /><br />
                            To translate this, it means <b>we still often act (or react) to threats today</b> as if our life is under constant attack, yet we are rarely under this scenario.
                            <br /><br />
                            Part of this is reflected in <b>our fight or flight response</b>, but the simplest way to express this is we react far more intensely to negative threats than the positive experiences.
                            <br /><br />
                            And this has a massive impact on your trading success today (particularly how you relate to losing a trade or losing money).
                            <br /><br />
                            As the author of Buddha's Brain (Rick Hanson) states, <strong>our brains are like Velcro for negative experiences and Teflon for good ones</strong>.</p>
                    </div>
                </div>
            </section><!-- /.section-three -->
            <section class="section section-four">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>It Was About Survival</h3>
                        <p>To pass on our genes, we had to avoid threats first. Only after we eliminated these could we start to focus on pleasurable things.</p>
                        <blockquote><i class="icon-quote"></i> The rule was <b>'eat lunch, don't be lunch'.</b></blockquote>
                        <p>This created a <b>'negativity bias'</b> in the brain, and it dominates most of your thinking, reactions and trading decisions today.
                            <br /><br />
                            If we were to map out the real-estate in the brain and the neural networks, we'd notice a disproportionate amount of wiring dedicated towards avoiding these negative threats.
                            <br /><br />
                            On average, our central nervous system and brain can react to a threat with lightning speed (<.1 secs) while it can take us 5-7+ seconds to respond to a positive stimuli.
                            <br /><br />
                            Hence <b>our brain is hyper-sensitive to every pip that goes against our trade,</b> but takes time to fully relax and digest a positive one.
                            <br /><br />
                            Going back to the Teflon - Velcro analogy of the brain, you've already had this experience many times.</p>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-four.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-four -->
            <section class="section section-five">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <p>Case in point, have you ever had a day where (for example) you took say 10 trades on a day, and perhaps 8 were winners, one was for breakeven, yet you made one mistake that took out almost all your gains for the day?
                            <br /><br />
                            Who hasn't had a day like this?
                            <br /><br />
                            Not many.
                            <br /><br />
                            Now ask yourself this - at the end of the day, what do you remember most?
                            <br /><br />
                            The 8 trades you executed well, <b>or the one big loser</b> which wiped out almost all your gains?
                            <br /><br />
                            Most likely you remember that big loss, and probably remember big losses more today than your big wins.
                            <br /><br />
                            You've probably even thought about your big losses far more often than your big wins.
                            <br /><br />
                            If so, then you are directly experiencing this negativity bias and how the brain is teflon for the positive and velcro fro the negative.
                            <br /><br />
                            We actually <a href="http://2ndskiesforex.com/advanced-traders-mindset-course">built an entire course on how to re-wire your brain for trading success</a> and get past this bias.
                            <br /><br />
                            But before we get into ways to correct this, we have to cover one key point about the brain.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-five.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-five -->
            <section class="section section-six">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-six.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>Experience Dependent Neuroplasticity</h3>
                        <p><b>EDN</b>, or <b>Experience Dependent Neuroplasticity</b> is a long way to say <b>'the brain learns, adapts and changes from experience'</b>.
                            <br /><br />
                            It means <b>we can wire new habits</b> in our brain and correct mental errors which affect our trading performance.
                            <br /><br />
                            There are two key points which underline EDN. They are;
                        </p>
                        <ol>
                            <li>Neurons that Fire Together, Wire Together</li>
                            <li>Consistent Passing Mental States Create Lasting Neural Traits</li>
                        </ol>
                        <p>
                            To summarize the latter point, the <b>dominant mental activity</b> (or thoughts) that run through our brain <b>can and will change/wire the structure of our brain</b> and neural networks.
                            <br /><br />
                            In regards to the first point, the networks that are strongest will help to </p>
                        <ul>
                            <li>a) determine how we are most likely to act,</li>
                            <li class="li__white l-align-center">and</li>
                            <li>b) help strengthen the neural networks we build for trading
                                success (or failure).</li>
                        </ul>
                        <p>Hence with point #2, <b>if you are constantly worrying about a loss,</b> being fearful, doubtful, thinking about that last loss, <b>you will create a neural structure</b> that will most likely continue that line of thinking and acting.
                            <br /><br />
			    This as you can imagine will have a negative effect on your trading mindset and performance.
                            <br /><br />
                            And with point #1, it means <b>if we want to build new neural networks</b> which go beyond this, we have to employ different neural networks to <b>help re-wire our brain for trading success.</b>
                            <br /><br />
			    So <b>if we want to pull the trigger consistently</b> when our trade setup is right in front of us, we'll need to wire that neural network to become a dominant network.
                            <br /><br />
                            We'll explore more of this later in the article, but I'd like to venture into...<!--the Self-Image and how it is affecting (and defining) your performance right now.--></p>
                    </div>
                </div>
            </section><!-- /.section-six -->
            <section class="section section-seven">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>Everyone Wants to Win the Lottery Right?</h3>
                        <p>Of course you want to win the lottery. It's the golden ticket to getting out of your financial situation. Or is it?
                            <br /><br />
                            <b>Callie Rogers won the UK lottery at the age of 16</b> snagging a solid 1.9M pounds. Not a bad high school graduation gift eh?
                            <br /><br />
                            One would think she was set for a great start in life.
                            <br /><br />
                            What did she do with her winnings?
                            <br /><br />
                            Partied, went on vacations, gave out gifts, and had 2 children with people she is not with today.
                            <br /><br />
                            <b>Within 3 years,</b> she completely went through her winnings and was last reported to work as a cleaning lady.
                            <br /><br />
                            Now it's easy to say (and I'm guessing many of you are) 'she was immature'.
                            <br /><br />
                            But let's look at an example of a 'mature' person and see how this didn't matter.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-seven.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-seven -->
            <section class="section section-eight">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-eight.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>A Pentacostal Preacher</h3>
                        <p><b>Billy Bob Harrell Jr</b> was a pentacostal preacher in the south west of the US. If Billy Bob Harrell Jr isn't a name for a pentacostal preacher, then I don't know what is :-)
                            <br /><br />
                            He was a man of the faith who had simple tastes and was by all means 'mature'.
                            <br /><br />
                            <b>He worked a side job at Home Depot</b> as a stock laborer.
                            <br /><br />
                            One day his prayers were answered and <b>he won $31M in a lottery.</b>
                            <br /><br />
                            He made what many would consider to be 'smart investments' in real estate, a ranch, and a few cars.
                            <br /><br />
                            Eventually he lost it all with handouts and bad real estate, and yet he was a 'mature' individual who always kept his word according to his friends.
                            <br /><br />
                            Eventually his wife divorced him and he did a very un-preacher like thing...he took his own life.
                            <br /><br />
                            So if 'maturity' wasn't the issue here, then what was?<br />
                            The Self-Image and how it is affecting (and defining) your performance right now.</p>
                    </div>
                </div>
            </section><!-- /.section-eight -->
            <section class="section section-nine">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>A Larger Than Life Ego</h3>
                        <p>In my opinion, this person has what may be the largest (and not exactly 'mature') ego on the planet.
                            <br /><br />
                            His stories about success are generally fabricated a tad (IMO) and through some poor business decisions, went from being several $B (billion) in profit to -$1B in debt.
                            <br /><br />
                            If it weren't for creditors and banks restructuring his debt, you would never know his name today as you do now.
                            <br /><br />
                            A few years later, he was in the black and <b>now is worth $8.7B.</b>
                            <br /><br />
                            His name is Donald Trump.
                            <br /><br />
                            How did he get himself out of this giant hole and climb higher than before?</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-nine.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-nine -->
            <section class="section section-ten">
                <div class="container">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-ten.png" alt="" class="image" />
                    </div>
                    <div class="col__right col__typography">
                        <h3>The Self-Image</h3>
                        <p>The answer comes down to his <b>'self-image'</b> and it is one of the greatest measures of your performance.
                            <br /><br />
                            It sets the <b>upper and lower boundaries of your success and performance</b> (in trading and life) and it is how you see yourself.
                            <br /><br />
                            We could define it as 'the <b>sum of your habits, attitudes and beliefs'</b> and it is what you think is 'like you' to do well (or not well).
                            <br /><br />
                            Do you feel confident you are good at numbers? Then it is in your self-image to be good at numbers and I'm guessing if I threw some number related challenges, you'd do fine.
                            <br /><br />
                            Do you feel it is not 'like you' to be a good dancer? I'm guessing unless you practice a lot or undergo some dance training/classes, you won't be able to move that well if I asked you to dance right now.
                            <br /><br />
                            Why?
                            <br /><br />
                            Because it's in your self-image, and it's how you define what it is 'like you' to do well (or not do well).</p>
                    </div>
                    <div class="clear"></div>
                    <div class="section--heading" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/self-image/section-ten--heading.jpg');">
                        Back to EDN
                        <ul class="history_route l-stacked">
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                </div>
            </section><!-- /.section-ten -->
            <section class="section section-eleven">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <p>Remember how the base models of experience-dependent-neuroplasticity talked about habits and lasting neural traits?
                            <br /><br />
                            Habits are simply things that are wired into our brain and neural networks which represent dominant neural networks.
                            <br /><br />
                            They are 'dominant' based on their strength and being stronger than most neural networks regarding specific related actions.
                            <br /><br />
                            Habits is what determines how we act, and the Self-Image reflects this consistency.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-eleven.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-eleven -->
            <section class="section section-twelve">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twelve.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>Like Me</h3>
                        <p>Anything that is a habit is in the self-image as being something <b>'like me'</b> to do with regular consistency.
                            <br /><br />
                            What this also means is <b>when you change the self-image, you change your performance.</b>
                            <br /><br />
                            However we have to be careful with the self-image because there are many things which <b>'imprint'</b> upon your self-image.
                            <br /><br />
                            Learning what imprints upon you, and how, is critical to re-building your self-image to think it is 'like you' to trade successfully.</p>
                    </div>
                </div>
            </section><!-- /.section-twelve -->
            <section class="section section-thirteen">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>Why Positive Affirmations Don't Work</h3>
                        <p>This is one of the main reasons why positive affirmations are not sufficient to change your self-image. I'm guessing many of you have tried them already and said them till you are blue in the face.
                            <br /><br />
                            Did it work?
                            <br /><br />
                            No, <b>because positive affirmations (although useful) are not sufficient to change your mindset and self-image for trading.</b>
                            <br /><br />
                            There are other critical reasons why positive affirmations won't work, but we'll get into that another day.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-thirteen.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-thirteen -->
            <section class="section section-fourteen">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-fourteen.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>The Comfort Zone & Your Trading Performance</h3>
                        <p>What does the <b>'comfort zone'</b> sound like to you? I'm guessing most of you will regard it as what you are 'comfortable' doing, and that would be an accurate description of it.
                            <br /><br />
                            The real question is how does this zone get defined and what does it mean for your trading performance?
                            <br /><br />
                            This zone gets defined by your daily actions, whether you take consistently repeatable actions or not.
                            <br /><br />
                            Do you generally take the same way to work each day? If so, that is an action which is within your comfort zone.
                            <br /><br />
                            Is dancing in front of people you don't know something you feel fully comfortable to do right now without a single thought worrying about how you'll look?
                            <br /><br />
                            If so, then it is within your comfort zone.
                            <br /><br />
                            Whether you are above your comfort zone or below it, <b>your performance will bring you back into your comfort zone.</b>
                            <br /><br />
                            The words <b>'over-performing'</b> and <b>'under-performing'</b> relate to this exactly.
                            <br /><br />
                            How many people consistently over-perform and maintain that for years on end?
                            <br /><br />
                            Very few.
                            <br /><br />
                            Yet it is far easier to under-perform if one's mindset and self-image are off.</p>
                    </div>
                </div>
            </section><!-- /.section-fourteen -->
            <section class="section section-fifteen">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>Since 2009 Without A...</h3>
                        <p>Along those lines, does anyone really think that Tiger Woods still does not have the skills to win a major tournament?
                            <br /><br />
                            I'm guessing y'all are going to say NO. So why hasn't he won a major since circa 2009?
                            <br /><br />
                            <strong>Mindset and self-image</strong>, both of which were forced to change in the wake of his personal issues.
                            <br /><br />
                            Until he can re-build and re-define that, he will unlikely win a major again.
                            <br /><br />
                            But to restate the main underlying point of the self-image from a mindset or psychological perspective, your self-image really defines what you feel it is 'like you' and 'not like you' to do consistently.
                            <br /><br />
                            Along the lines of what it is 'like me' to do, I'd like to share a story about someone who exemplified this.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-fifteen.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-fifteen -->
            <section class="section section-sixteen">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-sixteen.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>Michael & the Shutout</h3>
                        <p>My brother and I both played football (or soccer in the US). We also both played the same position (Goalkeeper).
                            <br /><br />
                            My brother was a perfectionist who both trained and played by this credo.
                            <br /><br />
                            In his senior year of high school, <b>he set the state record for most shutouts in a single season (18).</b> He did this in 25 games, so statistically, 72% of all his games nobody could score on him.
                            <br /><br />
                            Sometimes even getting a shutout didn't mean 'perfection' to him.
                            <br /><br />
                            One day I was watching a game of his where in the first 5 minutes of the game, someone kicked him. He made a noise when it happened and you could clearly see he was in pain.
                            <br /><br />
                            The opponent ended up kicking his left hand which is one of the most important tools for a keeper. Mike clearly knew something was wrong.
                            <br /><br />
                            What did he do?
                            <br /><br />
                            <b>Put his hand behind his back</b> and played the rest of the game one handed.
                            <br /><br />
                            Amazingly, <b>he got the shutout,</b> yet that wasn't the most impressive part of this story.
                            <br /><br />
                            When we got in the car, he took his glove off and told my dad to go to the hospital.
                            <br /><br />
                            The moment he pulled off his glove, we could see what was wrong.
                            <br /><br />
                            <b>His little finger knuckle was under the next finger.</b> He played with a broken hand 90% of the entire game and still got the shutout.
                            <br /><br />
                            Why? Because it was part of his self-image that it was 'like him' to get the shutout regardless of the circumstances.
                            <br /><br />
                            Later when asked about the injury, he simply called it 'a badge of honor'.
                            <br /><br />
                            I am grateful for having such a role model of the self-image in my life and what it means to train and play with such diligence.</p>
                    </div>
                </div>
            </section><!-- /.section-sixteen -->
            <section class="section section-seventeen">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>The 90/10 Rule</h3>
                        <p>When it comes to the comfort zone and the self-image, <b>most people spend 90% of their time</b> inside the comfort zone and 10% outside.
                            <br /><br />
                            <b>You have to reverse this equation if you want to change and trade successfully.</b>
                            <br /><br />
                            I don't suggest spending 90% of your time outside your comfort zone, but you will have to change the ratio.
                            <br /><br />
                            If it is uncomfortable for you to hold a trade to your profit target, it is simply not 'like you' (at least not yet) to hold to your trading plan and profit target.
                            <br /><br />
                            Doing so would be going outside your comfort zone and thus force your self-image and brain to adjust.
                            <br /><br />
                            Done enough times and it will become <b>'like you'</b> to follow your trading plan and hold for your profit target.
                            <br /><br />
                            But until you do things that are relatively uncomfortable for you in trading (or life), change likely will not happen, and the results will be the same.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-seventeen.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-seventeen -->
            <section class="section section-eighteen">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-eighteen.png" alt="" class="image" />
                    </div>
                    <div class="col__right col__typography">
                        <h3>Winning Streaks & the Limits to Your Self-Image</h3>
                        <p>Ever been on a winning streak of sorts which went for 5+ trades, perhaps even a few days or weeks on end?
                            <br /><br />
                            I'm guessing many of you at one point.
                            <br /><br />
                            Now what was the thing that happened at the end of this winning streak?
                            <br /><br />
                            I'm guessing if many of you shared the answer, the most common one would be 'a big loss' that probably wiped out most, if not all of your gains from the winning streak.
                            <br /><br />
                            Why is that?
                            <br /><br />
                            Because <strong>the winning streak took you outside of your comfort zone. If you go too far outside of it, or remain too long above it, you'll snap back into it</strong>.
                            <br /><br />
                            It is the de-facto mean reversion system of your mindset.
                            <br /><br />
                            This is why many students who are on a winning streak will email me a similar sentiment.
                            <br /><br />
                        </p>
                    </div>
                </div>
            </section><!-- /.section-eighteen -->
            <section class="section section-eighteen-two">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-eighteen-two.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <p>Almost all of them are something like the ones below:</p>
                        <blockquote><i class="icon-quote"></i> I'm on a really good winning streak and afraid it's going to end</blockquote>
                        <blockquote><i class="icon-quote"></i> I'm wondering if it's all luck and how long it will last</blockquote>
                        <blockquote><i class="icon-quote"></i> I'm thinking I should stop because I'm worried I won't be able to maintain it</blockquote>
                        <p>Have you ever said or felt something like this when you were on a winning streak?
                            <br /><br />
                            The good thing is, if you did, <strong>it means your unconscious mind is trying to communicate to your conscious mind you are outside your comfort zone</strong>.
                            <br /><br />
                            It is telling you ahead of time 'hey, you likely won't be able to keep this up, so slow down'.
                            <br /><br />
                            In summary, whatever it is comfortable for you to do easily is inside your comfort zone.
                            <br /><br />
                            If you feel your mind get busy, worried about a loss, fear of losing money, or cannot concentrate well while trading (or doing any task), it is likely because you are outside your comfort zone.
                            <br /><br />
                            In summary, the self-image has a well formed definition of what it is 'like you' to do.
                            <br /><br />
                            This is part neurological and part psychological.</p>
                    </div>
                </div>
            </section><!-- /.section-eighteen-two -->
            <section class="section section-nineteen">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>How Neural Networks Affect Your Ability to Change</h3>
                        <p>The <b>reason why you snap back into your comfort zone</b> and revert to your baseline self-image is heavily dependent upon your brain.
                            <br /><br />
                            The neural networks which are most dominant are the ones which you'll most likely activate when you execute a mental task (i.e. make a trading decision).
                            <br /><br />
                            Remember, <strong>passing consistent mental states create LASTING mental traits</strong>.
                            <br /><br />
                            It is more efficient for your brain to use specific neural networks which are already dominant because it uses less resources to engage them and execute a task.
                            <br /><br />
                            This is part of the reason why it's hard to change, <strong>because changing your habits actually requires your brain to activate lesser/weaker neural networks</strong>.
                            <br /><br />
                            It also means making physical changes in your brain along by activating new chemicals (neurotransmitters) to fire and build up such networks.
                            <br /><br />
                            It is part psychological because your self-image is well defined, and this translates into it being hard to change. The self-image will resist changing because it's more work and 'uncomfortable' (there's that word again).
                            <br /><br />
                            Only after consistent well defined imprinting will your self-image start to change, and that takes time.
                            <br /><br />
                            If I could sum it up regarding the self-image, performance and the comfort zone, it would be via the following statement below:
                            <br /><br />
                            <strong>Performance & self-image + comfort zone are almost always equal.</strong></p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-nineteen.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-nineteen -->
            <section class="section section-twenty">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twenty.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>Where Most Do the Least Amount of Work</h3>
                        <p>Lamentably, the <b>self-image is where most struggling traders</b> do the least amount of work. They stay inside their comfort zone, and thus never can find success in their trading.
                            <br /><br />
                            Most who never succeed in trading (or life) fail here - they simply fail to change their self-image.
                            <br /><br />
                            I can understand this as changing the self-image is a lot of work and it causes an internal friction.
                            <br /><br />
                            I've personally experienced this with changing my diet which has had a massive impact upon my health. I still struggle with it, but am gaining ground each day as I'm continually working with my self-image to change this.
                            <br /><br />
                            Nevertheless, I personally experience this friction anytime sugar is on the table.
                            <br /><br />
                            In the <strong>yoga sutras</strong>, they called this '<strong>tapas</strong>' which means to experience an inner friction.
                            <br /><br />
                            <b>Friction in trading comes from the self-image pushing back</b> as it gets uncomfortable. The self-image will resist change, so it will reject new imprints.
                            <br /><br />
                            In fact <b>your self-image will often attack the new positive imprints when trying to change.</b>
                            <br /><br />
                            This can often come when you feel uncomfortable doing something new, have doubts about your abilities, are fearful of the consequences (which generally aren't so bad after all).</p>
                    </div>
                </div>
            </section><!-- /.section-twenty -->
            <section class="section section-twentyone">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>Repetition is Crucial</h3>
                        <p>This is where you have to employ a weapon. That weapon is <b>'repetition',</b> and it is one of the most powerful factors behind changing your brain & mindset via neuroplasticity.
                            <br /><br />
                            All neural networks are created with repetition being a constant. You simply cannot wire change into your brain by doing something once.
                            <br /><br />
                            To form a dominant neural network, you have to continually fire the neurons which make up said network.
                            <br /><br />
                            This is why <b>repetition is crucial.</b> It also wears down the self-image and the current imprints in place.
                            <br /><br />
                            This is why it is super important NOT to beat yourself up as this will only increase the negative imprints in your self-image and brain.
                            <br /><br />
                            <b>Beating yourself down causes the self-image</b> to think it is 'not like me' to do something.
                            <br /><br />
                            We have to reverse this by <b>building ourselves up</b>, not beating ourselves down.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentyone.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-twentyone -->
            <section class="section section-twentytwo">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentytwo.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>They Have Histories</h3>
                        <p>Like your genes, the self-image has a history and that starts before you were born and what your parents helped to shape what it is 'like you' to think and do consistently.
                            <br /><br />
                            Your self-image will become what your thoughts/actions/feelings that happen now (and in the future are).
                            <br /><br />
                            In essence, this is a good thing as you are creating your self-image every day. If you want to create a new self-image for success in trading (or life), you can build that starting right now.
                            <br /><br />
                            <b>Even imprints you 'imagine'</b> are imprints nonetheless, so make them uplifting and supportive of what you want to create in your life.
                            <br /><br />
                            However you have to <b>incorporate feeling and emotion</b> into this as they offer an extra fuel to the neuroplasticity in  your brain.
                            <br /><br />
                            <b>Emotions and strong feelings create a stronger chemical cocktail</b> in the brain which helps to wire the thoughts, feelings and experiences deeper into your brain.</p>
                    </div>
                </div>
            </section><!-- /.section-twentytwo -->
            <section class="section section-twentythree">
                <div class="container">
                    <div class="col__left col__typography">
                        <h3>Why Positive Affirmations Don't Work</h3>
                        <p>This is another reason why positive affirmations don't work regardless of how long you say them.
                            <br /><br />
                            If you don't feel it is 'like you' to trade successfully, you could say them till you are blue in the face and it will never happen.
                            <br /><br />
                            Your <b>brain and heart both create electro-magnetic fields</b> (which we can measure). If your brain has a dissonant feeling from your heart, no change will happen because you won't believe it.
                            <br /><br />
                            It will just be conceptual and superficial, devoid of feeling, so your self-image will not imprint it, nor engage it.</p>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentythree.png" alt="" class="image" />
                    </div>
                    <div class="clear"></div>
                    <div class="section--heading" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentythree--bg.png');">
                        More in the Now Than...
                        <ul class="history_route l-stacked">
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                </div>
            </section><!-- /.section-twentythree -->
            <section class="section section-twentyfour">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <p>Ironically, your self-image is more in the <b>'now'</b> than your conscious mind is (in most cases).
                            <br /><br />
                            By default, the self-image only knows the 'now'.
                            <br /><br />
                            This is why <b>recalling past negative events</b> (or big lossess) is one of the WORST things you can do.
                            <br /><br />
                            <strong>Forgiveness and taking in positive experiences is critical for changing the self-image.</strong> Making mistakes in trading is not a problem. Self-referencing (and not learning from them) is.
                            <br /><br />
                            The negativity bias is strong in you my young (or older) padwan. Remember the brain is like teflon for good experiences and velcro for the bad.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentyfour.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-twentyfour -->
            <section class="section section-twentyfive">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentyfive.png" alt="" class="image" />
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__typography">
                        <h3>Remember When...</h3>
                        <p>Ever had a near death experience while driving a car? Notice how it takes a while to let go of that feeling?
                            <br /><br />
                            That is because our brain is hyper-sensitive to negative experiences and has a harder time to let go of them. This is a complete 180 to positive experiences which seem to fade quickly and be barely remembered.
                            <br /><br />
                            So stop <strong>thinking about your big mistakes and losses</strong>. Identify the habits you need to change in your self-image and change them.</p>
                    </div>
                </div>
            </section><!-- /.section-twentyfive -->
            <section class="section section-twentysix">
                <div class="container clearfix">
                    <div class="col__left col__typography">
                        <h3>Building A New Self-Image</h3>
                        <p>The <b>blueprint for building a new and successful self-image</b> is straightforward (just not easy). You have to continuously imprint new images onto it which are specific to what you want to create.
                            <br /><br />
                            The self-image by default is always in flux and always in one of these three states:</p>
                        <ol>
                            <li>Growing</li>
                            <li>Homeostasis (staying the same)</li>
                            <li>Dying (shrinking)</li>
                        </ol>
                        <p>Of the three above, you want to engage the first the most, and the second one for small periods of time. But the last one you never want to engage as it means decreasing the capacity of what it is 'like you' to do.
                            <br /><br />
                            <b>New imprints = a direct conflict with the old ones</b> (and thus create friction).
                            <br /><br />
                            Friction can manifest as tension in the body or mind, and often occurs chemically.
                            <br /><br />
                            You can generally feel confident you are experiencing this 'friction' when your mind gets busy, emotional, fearful, exhausted, goes into survival mode, or says 'I cannot do this' when trying on a new imprint.
                            <br /><br />
                            If you succumb to these, you will just default back to your hold imprints. This is why you repeat the same mistakes over and over and over again in trading.
                            <br /><br />
                            You have to <strong>weaken the relationship between the current neural networks which currently make up your self-image</strong>.
                            <br /><br />
                            You have to <strong>make the teflon become the velcro</strong> and vice versa in terms of your experiences for your mindset and self-image to change.</p>
                        <ul class="history_route l-stacked">
                            <li class="top"></li>
                            <li class="body"></li>
                            <li class="bottom"></li>
                        </ul>
                    </div>
                    <div class="col__right col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentysix.png" alt="" class="image" />
                    </div>
                </div>
            </section><!-- /.section-twentysix -->
            <section class="section section-twentyseven">
                <div class="container clearfix">
                    <div class="col__left col__image">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/self-image/section-twentyseven.png" alt="" class="image" />
                    </div>
                    <div class="col__right col__typography">
                        <h3>Key Questions to Ask About Your Trading Performance</h3>
                        <p>Instead of doing a summary of all the points above, I thought to end with asking some key questions which will help point to where you are at in this process, and what you need to work on the most.
                            <br /><br />
                            Here they are below:</p>
                        <ul>
                            <li>#1 - Do you really feel you have the self-image now to become a successful trader?</li>
                            <li>#2 - If you had to grade the sum of your habits, attitudes and beliefs right now about your trading, how would you grade them on a scale of 1-10 (with 10 being the best and 1 being highly negative)?</li>
                            <li>#3 - What have you told yourself it is 'not like you' to do?</li>
                            <li>#4 - How many wins was your longest winning streak?</li>
                            <li>#5 - Do you have clear and precise expectations for yourself and what you need to do to get there?</li>
                            <li>#6 - What are your most common/negative statements you say to yourself (internally and externally)?</li>
                            <li>#7 - Where do you feel friction in your life?</li>
                            <li>#8 - How consistent is your feedback loop?</li>
                        </ul>
                        <p>With that being said, I'd like to know how long your longest winning streak was (demo and live). Are they the same?
                            <br /><br />
                            I'd also like to know if you've had the same experience after a big winning streak of it ending with a large drawdown.
                            <br /><br />
                            Let me know in your comments below.
                            <br /><br />
                            Until then, I sincerely wish you the best of success in trading and life.</p>
                    </div>
                </div>
            </section><!-- /.section-twentyseven -->
        </main><!-- /.main -->
        <div id="wrapper" class="customize-support">
            <div id="main" class="clearfix">
                <div id="content">
                    <div class="posts">
                        <?php if (have_posts()) { ?>
                            <?php while (have_posts()) { the_post(); ?>
                                <?php global $post, $wp_query; ?>
                                <div class="main-post">
                                    <?php get_template_part('inc', 'share-print'); ?>
                                </div>
                                <?php global $authordata; ?>
                                <?php get_template_part('author', 'box'); ?>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <div class="comment-form-holder">
                    <?php
                        $args = array(
                            //  'fields' => apply_filters( 'comment_form_default_fields', $fields )
                            'comment_field' => '<div class="row clearfix">
                                                  <div class="photo">'.get_avatar('', 55).'</div>
                                                  <div class="textfield">
                                                  <textarea name="comment" id="comment" cols="30" rows="10" placeholder="Leave a comment...."></textarea></div></div>'
                            ,'must_log_in' => '<p class="must-log-in">' .  sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>'
                            ,'logged_in_as' => ''
                            ,'comment_notes_before' => '<p class="comment-notes">' . __( 'Your email address will not be published.' ) . ( $req ? $required_text : '' ) . '</p>'
                            ,'comment_notes_after' => ''
                            ,'id_form' => 'commentform'
                            ,'id_submit' => 'submit'
                            ,'title_reply' => __( 'Comments on the article' )
                            ,'title_reply_to' => __( 'Leave a Reply to %s' )
                            ,'cancel_reply_link' => __( 'Cancel reply' )
                            ,'label_submit' => __( 'Post Comment' )
                        );
                        comment_form( $args, $post->ID );


                        $args = array(
                                'post_id'  => $post->ID,
                                'status'   => 'approve',
                                'order'    => 'ASC',
                        );
                        $wp_query->comments = get_comments( $args );
                        ?>
                    </div>
                    <div class="post-comments" id="comments">
                        <h2 class="comments-title">Discussion</h2>
                        <!-- comment form -->
                        <?php
                            echo '<h3>'.count($wp_query->comments).' Comments on the article</h3>';
                            echo '<ul class="comment-list">';
                            wp_list_comments('callback=skies_comment');
                            echo '</ul>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer l-align-center">
            Copyright &copy; 2007 - 2015 2ndSkies Forex. All rights reserved.
        </footer><!-- /.footer -->

        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
        <!-- End Google Tag Manager -->
    </body>
</html>
