<?php
/*
Template Name: Course - Advanced Ichimoku
*/
?>
<?php get_header(); ?>
 <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/library/css/sales-templ-new.min.css" />
	<!-- main content -->
<main class="main">
	
    
		<!-- promo video -->
    <section id="" class="section_promo">
            <div class="container no_padding">
                <div class="col-xs-12 col-md-6">
                    <div class="promo-video-block">
                        <div class="custom_quote"> 
                    <?php the_post(); ?></div>
                        <div class="custom_list"> 
                        <?php the_content(); ?>
                        </div>
                    </div>
                    </div>
                    <div class="col-xs-12 col-md-6 l-align-right">
                        <div class="video-container">
                            <?php if (get_post_meta($post->ID, "embed-video", true) != '')   { 
                            ?>
                            <?php echo get_post_meta($post->ID, "embed-video", true); ?>
                        </div>
                        <?php } 
                        ?>
            
                    </div>
                
        </div>
    </section>
    
		<!-- promo text -->
    
    
    
    <section id="" class="section_hero" style="background-image: url('http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/background-promo-2.jpg')">
        <div class="container">
        <div class="l-align-center">
            <h1 class="section_hero--title">Advanced Ichimoku Training - What You've Been Missing</h1>
            <ul class="section_hero--inner_custom_list l-align-left" style="padding:0 30px 0 50px;">
                <br><br>
                <li>Did you know the last "expert" - Hidenobu Sasaki, wrote only 1 chapter talking about the traditional "5 Lines"? What did he write the other 6 chapters on?</li>
                <li>That is what we focus heavily on in our Advanced Ichimoku Course. Most of what you see out there is re-hashed material on how to trade the "5 lines".</li>
                <li>We have translated Sasaki's book into English, and share this information with you, only in this course. This is just one reason why we are different than the rest.</li>
            </ul>
        </div> 
        </div>
    </section>
        
    <div class="container"><div class="testimonial" style="background-image: url('http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/background-testimonials-2.png');"><p>"Of the 10,000 that practice Ichimoku, less than a few handful really understand it."</p><div class="author">Goichi Hosada, Founder of Ichimoku Kinko Hyo</div></div><!-- /.testimonial --></div>
    
        
        <section id="" class="section_coursebenefits">
            <div class="container">
                <div class="section_coursebenefits__bordered">
                    <h3 class="section_coursebenefits--title l-align-center l-text-capitalize" style="background-image: url('http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/background-benefits.jpg');"><span>What you'll get in this course</span></h3>
                    <div class="section_coursebenefits--list--wrap">
		<!-- table list -->
			<ul class="section_coursebenefits--list l-floated clearfix">
                <li><span>Understanding Ichimoku Beyond the 5 Lines</span></li>
                <li><span>Ichimoku Time Theory</span></li>
                <li><span>Ichimoku Wave Theory</span></li>
                <li><span>Ichimoku Price Theory</span></li>
                <li><span>Reading the Chikou Span</span></li>
                <li><span>6 Ichimoku Strategies Beyond the Theories</span></li>
                <li><span>Building A Successful Trading Psychology</span></li>
                <li><span>How to Prepare for Your Trading Day</span></li>
                <li><span>Trading Like a Business</span></li>
                <li><span>How to Find Future Support &amp; Resistance Levels</span></li>
                <li><span>How to Trade both Intra-day &amp; Swing with Ichimoku</span></li>
                <li><span>Building the Mindset of Abundance</span></li>
			</ul>
        </div>
                </div>
            </div>
    </section>
        
        
        
		<!-- testimonials -->
    <section id="" class="section_coursebenefits">
            <div class="container">
		<div class="testimonials-block adv-bg">
			<h2>Here's What Students Are Saying About The Course...</h2>
			<div class="testimonials-list">
				<div class="list-item">
					<blockquote>
						<q>Just to let you know I think the course is fantastic. It has improved my trading and understanding of Ichimoku tenfold. Instead of scalping for 5 to 10 pips and letting bad trades run, I am now seeing gains of 50+ pips consistently.</q>
						<cite>Phil, Australia &ndash; Course Member</cite>
					</blockquote>
				</div>
				<div class="list-item">
					<blockquote>
						<q>I just want to put on record my thanks for the quality of the course content. It represents stupendous value and you've made yourself available on a constant basis to guide us forwards.</q>
						<cite>Steve, Singapore &ndash; Course Member</cite>
					</blockquote>
				</div>
				<div class="list-item">
					<blockquote>
						<q>It is clearly evident you are genuinely interested in helping people. Thanks for the encouragement Chris and the great course. You truly are the mentor.</q>
						<cite>Eric, UK &ndash; Course Member</cite>
					</blockquote>
				</div>
			</div>
		</div><!-- testimonials-block END -->
        </div>
    </section>
        
		<!-- text content -->
    <section id="" class="section_coursebenefits">
            <div class="container">
		<h2 class="center">Live Trade Setups Forum</h2>
        
        <p>Along with the trading lessons & tutorials, students are also sharing real time analysis and trades using our ichimoku methods. Students also share their impressive backtesting results on the strategies, and how much profit they used with specific modifications.</p> 
        
        <p>This is really a fantastic area, where new students get to learn from more senior traders. Members also share trade ideas and setups before they happen, while I give analysis, commentary and suggestions as to what I'm looking at.</p>
        
<br>
        </div>
    </section>

		<section id="faq" class="section_faq">
            <div class="container">
                <div class="section_faq__bordered">
                    <h3 class="section_faq--title">Frequently asked questions</h3>
                    <ul class="section_faq--list l-stacked">
                        <li>
                            <h4 class="section_faq--question">Is this a one time fee and do I have unlimited access to the videos?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Yes, it's a one-time fee and you have lifetime access to the course and videos.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">What kind of support do I get and how is the follow up session conducted?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">You get full email support from me, a free follow up session from me, and access to the traders forums. The follow up session is usually done via Skype after a student logs at least 20 trades in their trading journal and presents it to me beforehand.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">I am a complete newbie in trading. How will this course benefit me?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Of all the trading skills you can have, price action would be the most important base you can have. In this course I start with the most basic models of how to read and trade price action. From here there is a progression of lessons which start from the beginning and work their way up. I have every level of trader from beginners to fund traders, so the course is suitable for all levels.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">I have a full-time job. Can I still use your systems and are there certain times of the day they only work on?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">A large majority of my students have full time jobs and still trade successfully using these systems. If they can, so can you. I have strategies that work throughout the day as there is a huge range of strategies from the 5m, 15m, 30m, 1hr, 4hr, daily and weekly charts, so regardless of your time zone and availability, there are strategies for you to trade.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">What are the hit rates of your systems and are they subjective or rule-based and can be programmed?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">System accuracy is a meaningless figure without reward to risk ratios. I have over 11+ systems across 8+ instruments, across 6+ time frames, and accuracy varies per pair, time frame and system, so to list them all would be ridiculous.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">I trade index futures, currency futures, and commodities. Do your systems work in these markets?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Our systems work with any markets that have sufficient liquidity. I personally use them on forex, index futures and commodities.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">If you are such a successful trader, then why do you teach?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Good question - please read my article <a href="http://2ndskiesforex.com/trade-signals/forex-strategies/why-i-do-this/" target="_blank">Why I Do This</a>.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">Do you post trade alerts in real time?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">No, as students would just hitch a free ride. My daily trade setups commentary is done after the NY Close Sun-Thurs.<br />My students and top traders however post their trades before, during and after daily.</p>
                            </div>
                        </li>
                        <li>
                            <h4 class="section_faq--question">Are there any independent reviews of your course?</h4>
                            <div class="l-no-overflow">
                                <p class="section_faq--answer">Yes, you can find them here on the following link (<a href="http://www.forexpeacearmy.com/public/review/2ndskiesforex.com" target="_blank">2ndSkiesForex Reviews</a>).</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section><!-- /.section_faq -->
        <br>
 
    
    <div class="container">
        <div class="notice clearfix"> 
            <span class="left">You get:</span>
            <p>12+ Instructional Videos & Tutorials, Live Trade Setups Forum, Private Follow Up Session, Full Email Support</p>
        </div>
    </div>


        

		<!-- order box -->
    <section id="buycourse" class="section_order">
        <div class="container">
            <h3 class="section_order--title">Order the Advanced Ichimoku Course Here</h3>
            <div class="section_order--column-left col-xs-12 col-md-6 no_padding">
                <div class="section_order--column--inner">
                    <img class="image" src="http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/banner-order-2.png" alt="">
                </div>
            </div>
            <div class="section_order--column-right col-xs-12 col-md-6 no_padding">
                <div class="section_order--column--inner">
                    <a href="http://courses.2nd-skies-forex.com/courses/startPayment?id=2<?php if(isset($_GET['orid'])) echo '&orid='.$_GET['orid']; ?>" class="button">Add To Cart</a>
                    <p class="section_order--description l-align-center"> *Normally <del>$499</del><strong>NOW</strong> all this<br> for a One Time Fee of <strong> $315.</strong>
                    <br><strong> - Ends August 31st</strong></p>
                    
						
						<ul class="section_order--list l-block l-align-center"><li><img src="http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/visa.png" alt=""></li><li><img src="http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/mastercard.png" alt=""></li><li><img src="http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/discover.png" alt=""></li><li><img src="http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/americanexpress.png" alt=""></li><li><img src="http://2ndskiesforex.com/wp-content/themes/2ndSkies/images/sales-new/paypal.png" alt=""></li></ul>
                </div>
            </div>
            <div class="clear"></div>
        </div> 
        <div class="order-box">
        <div class="desc">
				<p><span>NOTE:</span> 10+% of all net revenue from course sales goes towards various charities and non-profit organizations around the world. To us, making money is one thing,
but having a positive impact on the world is another thing entirely. We work to uplift the world.</p>
			</div>
        </div>
    </section>
    
    
	
</main>
<script type="text/javascript">
$( document ).ready(function() {
$( ".section_faq--question" ).click(function(){
    $("ul.section_faq--list.l-stacked li").removeClass("open");
	$(this).parent().addClass("open");
});

});
</script>
<?php get_footer(); ?>
