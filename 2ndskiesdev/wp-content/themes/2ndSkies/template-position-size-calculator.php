<?php
/*
Template Name: CALC - Position size
*/
?>

<?php get_header(); ?>

<div id="main" class="clearfix">
<!-- main content -->
<div id="content">
    <!-- breadcrumbs container -->
    <div class="breadcrumbs-container clearfix">
        <div class="breadcrumbs">
            <ul>
                <li><a href="<?php echo home_url(); ?>">Home</a></li>
                <li><?php the_title(); ?></li>
            </ul>
        </div>
    </div>
     <div class="default-template">
	<h1><?php the_title(); ?></h1>
    
    
    
    
  <script type="text/javascript">
var FirstSelCur;
var SecondSelCur;
var FirstAffCur;
var SecondAffCur;
var stype;

var CurrencyPriority = new Array();

//CurrencyPriority["XAU"] = 103;
//CurrencyPriority["XAG"] = 102;
//CurrencyPriority["OIL"] = 101;
CurrencyPriority["EUR"] = 100;
CurrencyPriority["GBP"] = 90;
CurrencyPriority["AUD"] = 80;
CurrencyPriority["NZD"] = 70;
CurrencyPriority["USD"] = 60;
CurrencyPriority["CAD"] = 50;
CurrencyPriority["CNH"] = 47;
CurrencyPriority["CHF"] = 46;
CurrencyPriority["SGD"] = 45;
CurrencyPriority["DKK"] = 44;
CurrencyPriority["PLN"] = 43;
CurrencyPriority["CCK"] = 42;
CurrencyPriority["HKD"] = 41;
CurrencyPriority["HUF"] = 40;
CurrencyPriority["LVL"] = 39;
CurrencyPriority["NOK"] = 38;
CurrencyPriority["ZAR"] = 36;
CurrencyPriority["SEK"] = 35;
CurrencyPriority["HRK"] = 34;
CurrencyPriority["LTL"] = 33;
CurrencyPriority["MXN"] = 32;
CurrencyPriority["RUB"] = 31;
CurrencyPriority["JPY"] = 30;
function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function SwapRiskMoney()
{
with (document.f)
{

if (swapriskmoney.value == "Change for Money Value")
{
document.getElementById("riskt").className = "visible";
document.getElementById("moneyt2").className = "visible";
document.getElementById("riskt2").className = "invisible";
document.getElementById("moneyt").className = "invisible";
swapriskmoney.value = "Change for Risk Value";
}
else
{
document.getElementById("riskt").className = "invisible";
document.getElementById("moneyt2").className = "invisible";
document.getElementById("riskt2").className = "visible";
document.getElementById("moneyt").className = "visible";
swapriskmoney.value = "Change for Money Value";
}
var t = money.value;
money.value = risk.value;
risk.value = t;

}
}

function ShowForm()
{
with (document.f)
{
document.getElementById("moneycur").innerHTML = currency.value;
document.getElementById("moneycur2").innerHTML = currency.value; // added by DD
FirstSelCur = pair.value.substring(0,3);
SecondSelCur = pair.value.substring(4,7);
if (currency.value != SecondSelCur)
{
if (CurrencyPriority[currency.value] > CurrencyPriority[SecondSelCur])
{
 FirstAffCur = currency.value;
 SecondAffCur = SecondSelCur;
}
else
{
 FirstAffCur = SecondSelCur;
 SecondAffCur = currency.value;
}
document.getElementById("pricet").className = "visible";
document.getElementById("divinput").className = "visible";
document.getElementById("divreq").className = "visible";
if (currency.value == FirstAffCur) {stype = " Ask price";} else {stype = " Bid price"};
document.getElementById("pricet").innerHTML = "Current " + "<span class='curr first-cur'>" + FirstAffCur + "</span>" + "/" + "<span class='curr sec-cur'>" + SecondAffCur + "</span>" + "<span class='ab-price'>" + stype + "</span>";
}
else
{
document.getElementById("pricet").className = "invisible";
document.getElementById("divinput").className = "invisible";
document.getElementById("divreq").className = "invisible";
FirstAffCur = 0;
SecondAffCur = 0;
}
}
}

function CalcPV()
{
with (document.f)
{
	if(!size.value)
	{
//		alert('Enter your account size, please.');

            var alert = alertify.alert( 'Enter your account size, please.' );
            alert.show();
			
		return false;

	}
	else if(!risk.value)
	{
		var alert = alertify.alert('Enter Equity Risk, please.');
		alert.show();
		
		return false;
	}
	else if(!stop.value)
	{
		var alert = alertify.alert('Enter your stop-loss, please.');
		alert.show();
		
		return false;
	}
	else if((!price.value) && (currency.value != SecondSelCur))
	{
		var alert = alertify.alert('Enter the current price, please.');
		alert.show();
		
		return false;
	}
   	size.value = size.value.replace(",", ".");
   	risk.value = risk.value.replace(",", ".");
   	stop.value = stop.value.replace(",", ".");
   	price.value = price.value.replace(",", ".");

	var t;
	if (swapriskmoney.value == "Change for Money Value")
	{
	t = (size.value * risk.value / 100);
	money.value = t.toFixed(2);
	}
	else 
	{
	t = (risk.value / size.value) * 100;
	money.value = t.toFixed(2);
	}

	var UnitCosts = 0.0001;
	if (SecondAffCur != 0)
	{
	 if (stype == " Ask price") {if (SecondAffCur == "JPY") UnitCosts = UnitCosts / (price.value / 100); else UnitCosts = UnitCosts / price.value;}
	 else {if (SecondAffCur == "JPY") UnitCosts = UnitCosts * price.value / 100; else UnitCosts = UnitCosts * price.value;}
	}
	if (swapriskmoney.value == "Change for Money Value")
		t = ((money.value / stop.value) / UnitCosts);
	else t = ((risk.value / stop.value) / UnitCosts);
	units.value = t.toFixed(0);
	t = t / 100000;
	lots.value = t.toFixed(3);
	createCookie('earnforex_psc_currency',currency.value,31);
	return true;
}
}
</script>

        <form name="f" id="f">
          <table class="form-tbl calc-input-values">
            <tbody>
              <tr>
                <td class="left">Account currency</td>
                <td><select name="currency" id="currency" onchange="window.ShowForm()">
                    <option value="USD">USD</option>
                    <option value="EUR">EUR</option>
                    <option value="GBP">GBP</option>
                    <option value="JPY">JPY</option>
                    <option value="CHF">CHF</option>
                    <option value="CAD">CAD</option>
                    <option value="AUD">AUD</option>
                    <option value="NZD">NZD</option>
                    <option value="SGD">SGD</option>
                    <option value="DKK">DKK</option>
                    <option value="PLN">PLN</option>
                    <option value="CCK">CCK</option>
                    <option value="HKD">HKD</option>
                    <option value="HUF">HUF</option>
                    <option value="LVL">LVL</option>
                    <option value="NOK">NOK</option>
                    <option value="ZAR">ZAR</option>
                    <option value="SEK">SEK</option>
                    <option value="HRK">HRK</option>
                    <option value="LTL">LTL</option>
                    <option value="MXN">MXN</option>
                    <option value="RUB">RUB</option>
                    <!--<option value="XAU">XAU</option>
<option value="XAG">XAG</option>
<option value="OIL">OIL</option>-->
                    <option value="CNH">CNH</option>
                  </select></td>
                <td class="comm"><span class="required">*</span></td>
              </tr>
              <tr>
                <td class="left">Account size</td>
                <td><input name="size" id="size" class="num-field" type="text"></td>
                <td class="comm"><span class="required">*</span></td>
              </tr>
              <tr>
                <td class="left"><div id="riskt2" name="riskt2" class="visible">Equity % risked</div>
                  <div id="moneyt2" name="moneyt2" class="invisible">Money, <span id="moneycur2" name="moneycur2"></span></div></td>
                <td><input name="risk" id="risk" class="num-field" type="text"></td>
                <td class="comm"><span class="required">*</span>
                  <div class="dashicons dashicons-update dash-update"></div><input name="swapriskmoney" id="swapriskmoney" value="Change for Money Value" onclick="SwapRiskMoney()" type="button">
                  
                  </td>
              </tr>
              <tr>
                <td class="left">Stop-Loss, standard pips</td>
                <td><input name="stop" id="stop" class="num-field" type="text"></td>

                <td class="comm"><span class="required">*</span></td>
              </tr>
              <tr>
                <td class="left">Currency pair</td>
                <td><select name="pair" id="pair" onchange="window.ShowForm()">
                    <option value="EUR/USD">EUR/USD</option>
                    <option value="GBP/USD">GBP/USD</option>
                    <option value="USD/JPY">USD/JPY</option>
                    <option value="USD/CHF">USD/CHF</option>
                    <option value="USD/CAD">USD/CAD</option>
                    <option value="EUR/JPY">EUR/JPY</option>
                    <option value="GBP/JPY">GBP/JPY</option>
                    <option value="AUD/USD">AUD/USD</option>
                    <option value="NZD/USD">NZD/USD</option>
                    <option value="EUR/GBP">EUR/GBP</option>
                    <option value="EUR/CHF">EUR/CHF</option>
                    <option value="EUR/AUD">EUR/AUD</option>
                    <option value="EUR/CAD">EUR/CAD</option>
                    <option value="EUR/NZD">EUR/NZD</option>
                    <option value="GBP/CHF">GBP/CHF</option>
                    <option value="GBP/AUD">GBP/AUD</option>
                    <option value="GBP/CAD">GBP/CAD</option>
                    <option value="GBP/NZD">GBP/NZD</option>
                    <option value="AUD/CHF">AUD/CHF</option>
                    <option value="CAD/CHF">CAD/CHF</option>
                    <option value="CHF/JPY">CHF/JPY</option>
                    <option value="NZD/CHF">NZD/CHF</option>
                    <option value="AUD/CAD">AUD/CAD</option>
                    <option value="AUD/JPY">AUD/JPY</option>
                    <option value="AUD/NZD">AUD/NZD</option>
                    <option value="CAD/JPY">CAD/JPY</option>
                    <option value="NZD/CAD">NZD/CAD</option>
                    <option value="NZD/JPY">NZD/JPY</option>
                    <option value="AUD/DKK">AUD/DKK</option>
                    <option value="AUD/PLN">AUD/PLN</option>
                    <option value="AUD/SGD">AUD/SGD</option>
                    <option value="CHF/SGD">CHF/SGD</option>
                    <option value="EUR/CCK">EUR/CCK</option>
                    <option value="EUR/DDK">EUR/DDK</option>
                    <option value="EUR/HKD">EUR/HKD</option>
                    <option value="EUR/HUF">EUR/HUF</option>
                    <option value="EUR/LVL">EUR/LVL</option>
                    <option value="EUR/NOK">EUR/NOK</option>
                    <option value="EUR/PLN">EUR/PLN</option>
                    <option value="EUR/SEK">EUR/SEK</option>
                    <option value="EUR/SGD">EUR/SGD</option>
                    <option value="EUR/ZAR">EUR/ZAR</option>
                    <option value="GBP/DKK">GBP/DKK</option>
                    <option value="GBP/NOK">GBP/NOK</option>
                    <option value="GBP/SEK">GBP/SEK</option>
                    <option value="GBP/SGD">GBP/SGD</option>
                    <option value="GBP/ZAR">GBP/ZAR</option>
                    <option value="NZD/SGD">NZD/SGD</option>
                    <option value="SGD/JPY">SGD/JPY</option>
                    <option value="USD/CCK">USD/CCK</option>
                    <option value="USD/CNH">USD/CNH</option>
                    <option value="USD/DKK">USD/DKK</option>
                    <option value="USD/HKD">USD/HKD</option>
                    <option value="USD/HRK">USD/HRK</option>
                    <option value="USD/HUF">USD/HUF</option>
                    <option value="USD/LTL">USD/LTL</option>
                    <option value="USD/LVL">USD/LVL</option>
                    <option value="USD/MXN">USD/MXN</option>
                    <option value="USD/NOK">USD/NOK</option>
                    <option value="USD/PLN">USD/PLN</option>
                    <option value="USD/RUB">USD/RUB</option>
                    <option value="USD/SEK">USD/SEK</option>
                    <option value="USD/SGD">USD/SGD</option>
                    <option value="USD/ZAR">USD/ZAR</option>
                    <!--<option value="XAU/USD">XAU/USD</option>
<option value="XAG/USD">XAG/USD</option>
<option value="OIL/USD">OIL/USD</option>-->
                  </select></td>
                <td class="comm"><span class="required">*</span></td>
              </tr>
              <tr>
                <td class="left"><div id="pricet" name="pricet" class="invisible"></div></td>
                <td><div id="divinput" name="divinput" class="invisible">
                    <input name="price" id="price" class="num-field" type="text">
                  </div></td>
                <td class="comm"><div id="divreq" name="divreq" class="invisible"><span class="required">*</span></div></td>
              </tr>
              <tr>
                <td class="left"></td>
                <td colspan="2"><input class="clear-value" value="Clear" onclick="document.getElementById('f').reset();" type="button">&nbsp;&nbsp;&nbsp;<input class="button" value="Calculate" onclick="CalcPV(f);" type="button"></td>
              </tr>
            </tbody>
          </table>
 
          <h3 class="form-calc"><span><i class="dashicons dashicons-chart-bar dash-chart"></i> Results</span></h3>
          
          <table class="form-tbl wholecolored calc-results">
            <tbody>
              <tr>
                <td class="left"><div id="moneyt" name="moneyt" class="visible">Money, <span id="moneycur" name="moneycur"></span></div>
                  <div id="riskt" name="riskt" class="invisible">Equity % risked</div></td>
                <td><input name="money" id="money" type="text" readonly></td>
              </tr>
              <tr>
                <td class="left">Units</td>
                <td><input name="units" id="units" type="text" readonly></td>
              </tr>
              <tr>
                <td class="left">Lots</td>
                <td><input name="lots" id="lots" type="text" readonly></td>
              </tr>
            </tbody>
          </table>
        </form>
 
<script type="text/javascript">
	with (document.f)
	{
	 var t = readCookie('earnforex_psc_currency');
	 if (t != null) currency.value = t;
	}
	ShowForm();
</script>

<script type="text/javascript">
function setCookie(c_name, value, expiredays)
{
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + expiredays);
	document.cookie = c_name+"="+escape(value)+";path=/"+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}

function ForgetMessageBar(mb, id)
{
	setCookie(mb,id,5*365);
}

function removeMessageBar()
{
 var MB = document.getElementById("MessageBar");
 MB.className = 'invis';
}

function dontshow(mb, id)
{
 ForgetMessageBar(mb, id);
 removeMessageBar();
}
</script>

            <div class="zx-separator"></div>
       		<?php get_template_part('inc', 'share-print'); ?>
   


		</div><!-- #content -->
	</div><!-- #primary -->
    
    
    
    
    <div id="alertifyCover" class="alertify-cover alertify-hidden"></div>
    <section id="alertifyDialog" class="alertify alertify-close" aria-labelledby="alertifyTitle" aria-hidden="true">
        <div class="alertify-body">
            <p id="alertifyTitle" class="alertify-title"></p>
            <input type="text" id="alertifyInput" class="alertify-input" aria-hidden="true">
            <nav id="alertifyButtons" class="alertify-buttons">
                <button id="alertifyButtonCancel" class="alertify-button alertify-button-cancel" aria-hidden="true"></button>
                <button id="alertifyButtonOk" class="alertify-button alertify-button-ok" aria-hidden="true"></button>
            </nav>
            <a id="alertifyFocusReset" class="alertify-focus-reset" href="#" aria-hidden="true"></a>
        </div>
    </section>
    
   
<script>
jQuery(document).ready(function($) {
	$("#f select").chosen({
		disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%"
	});
});

//jQuery(document).ready(function($) {
//	$('.num-field').on('keypress', function(evt) {
//		var charCode = (evt.which) ? evt.which : event.keyCode;
//		return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
//	});
//});

//jQuery(document).ready(function($) {
//	$('.num-field').keypress(function(event) {
//	  if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57 ) && ( event.which != 8) ) {
//		event.preventDefault();
//	  }
//	});
//});

jQuery(document).ready(function($) {
	$("input.num-field").numeric();
});



//jQuery(document).ready(function($) {     
  //$("#swapriskmoney").click(function() { 
   // $(".dashicons-update").addClass("active");      //add the class to the clicked element
	//$( ".dashicons-update" ).fadeOut( 300 ).delay( 100 ).fadeIn( 400 );
	
	//$(".dashicons-update").rotate({ angle:0,animateTo:180,easing: $.easing.easeInOutExpo })
	
	//$(".dashicons-update").rotate({ angle:0,animateTo:180,easing: $.easing.easeInOutExpo })
	
	//$(".dashicons-update").animate({rotate:180},{duration:500})
	
	
  //});
//});
</script>


<script> 
jQuery(document).ready(function($) {
	

	var elclick = "#swapriskmoney";
    var el = ".dashicons-update";

    $(el).css({
        "transition": "all 400ms ease-in-out"
    });

    var rot = 0;

    $(elclick).click(function () {
        if (rot === 0) {
            rot = 180;
        } else {
            rot = 0;
        }

        $(el).css({
            "transform": "rotate(" + rot + "deg)"
        });
    });


});
</script>   
    
    
    



	<?php get_sidebar('pages'); ?><!-- sidebar -->
    
</div><!-- #main -->

<?php get_footer(); ?>
