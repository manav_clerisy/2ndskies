<?php
/*
Template Name: Affiliate Signup
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <link media="all" rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/library/css/affiliate-templ.css"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body style="background-color:#f2f2f2 !important;">

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->


<div class="container-affiliate">

    <div class="header">
        <a href="<?php echo home_url( '/' ); ?>" class="logo"> Second Skies</a>
    </div>

    <div class="main-content">
        <div class="img-holder">
            <img src="<?php bloginfo( 'template_url' ); ?>/library/images/chris_chart.png" alt="Advanced Price Action Course">
        </div>
        <br/>
        <br/>
        <p>The 2ndSkiesForex Affiliate program is led by Chris Capre, a well-known and respected member of the trading community.</p>
        <p>2ndSkies is offering the #1 online course for Price Action and Ichimoku trading strategies.</p>
        <p>With full support from the 2ndSkies team, drive traffic to a program that is top rated by the trading community.</p>
        <ul>
            <li>Life-Time Membership & Ongoing Training</li>
            <li>14,000 hours behind the charts, 5,000 live trades & 6,000 hours of meditation</li>
            <li>Custom built platform to support learning</li>
        </ul>
        <a href="#scroll">Sign Up Today</a>
    </div>
    <!--
    <div id="signup">
        <h1 id="page-heading">Affiliate Sign Up	</h1></div>

<script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genbootstrap.php?method=script&uid=p2c26856f8&version=1"></script>


<div id="footer" class="grid_12">
    <p>
    </p></div>
<div class="clear"></div>
</div>

</body>
</html>
