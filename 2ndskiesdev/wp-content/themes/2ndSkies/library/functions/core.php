<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * The functions file is used to initialize everything in the theme.  It controls how the theme is loaded and
 * sets up the supported features, default actions, and default filters.
 *
 */


/* Register all the bundled scripts */
add_action('wp_enqueue_scripts', 'ssfx_register_scripts', 0);
/* Enqueue scripts */
add_action('wp_enqueue_scripts', 'ssfx_enqueue_scripts', 5);

add_shortcode('email', 'shortcode_email');

add_action('init', 'register_my_menus');
add_action('init', 'video_register');

add_filter('the_content', 'filter_template_url');
add_filter('get_the_content', 'filter_template_url');
add_filter('widget_text', 'filter_template_url');

// Copyright Date
add_filter('widget_text', 'filter_copyright_date');

add_filter('the_content', 'filter_site_url');
add_filter('get_the_content', 'filter_site_url');
add_filter('widget_text', 'filter_site_url');

add_filter('the_content', 'filter_images_url');
add_filter('get_the_content', 'filter_images_url');
add_filter('widget_text', 'filter_images_url');

add_filter('nav_menu_css_class', 'change_menu_classes');

add_shortcode('tmp-url', 'tmpurl');

add_shortcode('images-url', 'imgurl');

add_shortcode('share-this', 'shareThis');

add_filter('pre_get_posts','SearchFilter');

add_action( 'show_user_profile', 'themelia_show_extra_profile_fields' );

add_action( 'edit_user_profile', 'themelia_show_extra_profile_fields' );

add_action( 'personal_options_update', 'themelia_save_extra_profile_fields' );

add_action( 'edit_user_profile_update', 'themelia_save_extra_profile_fields' );

//Remove or Disable comment-reply.js code
add_action('init','themelia_clean_header');

/* Remove query string from static files for better caching */
add_filter( 'style_loader_src', 'themelia_remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'themelia_remove_cssjs_ver', 10, 2 );

// BAD THING
//remove_filter('the_content', 'wpautop');

/**
 * Registers JavaScript files for the theme.   *
 */
 
function ssfx_register_scripts() {
	wp_register_script('main', trailingslashit ( THEME_JS ) . 'jquery.main.js', array('jquery'), '1.0.0', true);
	//wp_register_script('fitvids', trailingslashit ( THEME_JS ) . 'jquery.fitvids.js', array('jquery'), '1.0.0', true);
	//wp_register_script( 'cookie', trailingslashit ( THEME_JS ) . 'jquery.cookie.js', array('jquery'), '1.4.1', true );
	wp_register_script( 'alertify', trailingslashit ( THEME_JS ) . 'alertify.min.js', array(), '1.0', true );
	wp_register_script( 'chosen', trailingslashit ( THEME_JS ) . 'chosen.jquery.min.js', array(), '1.0', true );
	wp_register_script( 'numeric', trailingslashit ( THEME_JS ) . 'numeric.js', array(), '1.0', true );
	
	wp_register_script('init', trailingslashit ( THEME_JS ) . 'jquery.init.js', array('jquery'), '1.0.0', true);
	wp_register_script('test-app', trailingslashit ( THEME_JS ) . 'test-app.js', array('jquery'), '1.0.0', true);
	
	wp_register_script('sharrre', trailingslashit ( THEME_JS ) . 'jquery.sharrre.min.js', array('jquery'), '1.3.5', false);
	
	wp_register_script( 'easyListSplitter', trailingslashit ( THEME_JS ) . 'jquery.easyListSplitter.js', array(), '1.0', true );
}

function ssfx_enqueue_scripts() {
	wp_enqueue_script('main');
	//wp_enqueue_script('fitvids');
	wp_enqueue_script('init');
	//wp_enqueue_script('cookie');
	
	wp_enqueue_script('test-app');
	
	wp_enqueue_style('dashicons');
	
	if (is_page('position-size-calculator')) { 
		wp_enqueue_style( 'alertify-core', trailingslashit ( THEME_CSS )  . 'alertify.core.css' );
		wp_enqueue_style( 'chosen-style', trailingslashit ( THEME_CSS )  . 'chosen.min.css' );
		wp_enqueue_script('alertify');
		wp_enqueue_script('chosen');
		wp_enqueue_script('numeric');
	}
	
	if (is_page('risk-of-ruin-calculator')) { 
		wp_enqueue_script('numeric');
	}
	
	if ( is_single() || is_page('forex-trading-tools') || is_category('forex-videos') || is_category('trading-library') ) { 
		wp_enqueue_script('sharrre');
	}
	
	wp_enqueue_style( 'unsemantic-grid', trailingslashit ( THEME_CSS )  . 'unsemantic-grid.css' );
	
	if ( is_page_template( 'popup.php' ) ) {
		wp_enqueue_style( 'customized-learning', trailingslashit ( THEME_CSS )  . 'customized-learning.css' );
		wp_enqueue_script('easyListSplitter');
	}
	
	
	$font_url_1 = webfont_font_url_1();
	$font_url_2 = webfont_font_url_2();	
	wp_enqueue_style( 'sans-pro-font', esc_url_raw( $font_url_1 ), array(), null );
	wp_enqueue_style( 'open-sans-font', esc_url_raw( $font_url_2 ), array(), null );
}


/**
 * Register Open Sans.
 *
 * @return string
 */


function webfont_font_url_1() {
	$font_url_1 = add_query_arg( 'family', 'Source+Sans+Pro:400,400italic,700', "//fonts.googleapis.com/css" );
	return $font_url_1;
}
/**
 * Register Web Font.
 *
 * @return string
 */
function webfont_font_url_2() {
	$font_url_2 = add_query_arg( 'family', 'Open+Sans:300,400,600,700', "//fonts.googleapis.com/css" );
	return $font_url_2;
}

/*Various default functions*/
/* NEED */

//add [email]...[/email] shortcode
function shortcode_email($atts, $content) {
	$result = '';
	for ($i=0; $i<strlen($content); $i++) {
		$result .= '&#'.ord($content{$i}).';';
	}
	return $result;
}

// register tag [template-url]
function filter_template_url($text) {
	return str_replace('[template-url]',get_bloginfo('template_url'), $text);
}

// register tag [images-url]
function filter_images_url($text) {
	return str_replace('[images-url]',get_bloginfo('template_url') . '/library/images', $text);
}

// register tag [copyright-date]
function filter_copyright_date($text) {
	return str_replace('[copyright-date]', date('Y'), $text);
}

// register tag [site-url]
function filter_site_url($text) {
	return str_replace('[site-url]',get_bloginfo('url'), $text);
}

/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
		$css_classes = str_replace("current-menu-item", "active", $css_classes);
		$css_classes = str_replace("current-menu-parent", "active", $css_classes);
		return $css_classes;
}

//allow tags in category description
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
	remove_filter($filter, 'wp_filter_kses');
}


//Make WP Admin Menu HTML Valid
function wp_admin_bar_valid_search_menu( $wp_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch"><div>';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" tabindex="10" type="text" value="" maxlength="150" />';
	$form .= '<input type="submit" class="adminbar-button" value="' . __('Search') . '"/>';
	$form .= '</div></form>';

	$wp_admin_bar->add_menu( 
		array(
			'parent' => 'top-secondary',
			'id'     => 'search',
			'title'  => $form,
			'meta'   => array(
				'class'    => 'admin-bar-search',
				'tabindex' => -1,
			)
		)
	);
}

function register_my_menus() {
	register_nav_menus (
		array(
			'header-menu' => 'main navigation', 
			'footer-menu' => 'footer navigation'
		)
	);
}


function video_register() {
	$labels = array(
		'name' => _x('Videos', 'post type general name'),
		'singular_name' => _x('Videos', 'post type singular name'),
		'add_new' => _x('Add New', 'Videos'),
		'add_new_item' => __('Add New Videos'),
		'edit_item' => __('Edit Videos'),
		'new_item' => __('New Videos'),
		'all_items' => __('All Videos'),
		'view_item' => __('View Videos'),
		'search_items' => __('Search Videos'),
		'not_found' =>  __('No Videos found'),
		'not_found_in_trash' => __('No Videos found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Videos'
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null,
		 'taxonomies' => array('post_tag'),
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields','page-attributes')
	  ); 	

	//register what we just set up above
	register_post_type('videos', $args);
	
	$labels = array(
		'name'              => _x( 'Video categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'Video category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Video categories' ),
		'all_items'         => __( 'All Video categories' ),
		'parent_item'       => __( 'Parent Video category' ),
		'parent_item_colon' => __( 'Parent Video category:' ),
		'edit_item'         => __( 'Edit Video category' ),
		'update_item'       => __( 'Update Video category' ),
		'add_new_item'      => __( 'Add New Video category' ),
		'new_item_name'     => __( 'New Video category Name' ),
		'menu_name'         => __( 'Video category' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'videocategory' ),
	);

	register_taxonomy( 'videocategory', array( 'videos' ), $args );
}


function tmpurl() {
	return get_template_directory_uri() . '/library/';
}
function imgurl() {
	return get_template_directory_uri() . '/library/images/';
}


function shareThis() {
	echo '<script src="http://w.sharethis.com/button/buttons.js"></script>
		<script>stLight.options({publisher: "cb4b643c-040e-4411-8e13-957deba59844", doNotHash: true, doNotCopy: true, hashAddressBar: false});</script>';
	return '<div class="social-networks-holder"><span class="title">Share this</span>
		<ul class="social-networks">
			<li><span class="facebook st_facebook_large"></span></li>
			<li><span class="twitter st_twitter_large"></span></li>
			<li><span class="youtube st_youtube_large" st_username="SecondSkies"></span></li>
			<li><span class="google-plus st_googleplus_large"></span></li>
		</ul>
	</div>';
}


function skies_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;        
	?>
	<li id="comment-<?php comment_ID(); ?>">
        <div <?php comment_class('clearfix'); ?>>
			<div class="photo">
				<?php echo get_avatar( $comment, 55 ); ?>
			</div>
			<div class="text-holder">
				<div class="meta-info">
					<span class="author"><?php echo get_comment_author_link();?></span> - <em class="date"><?php echo get_comment_date().' '.get_comment_time(); ?></em>
				</div>
				<?php comment_text(); ?>
				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div>
			</div>
		</div>
	
	<?php
}


function getPostViews($postID){
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
		return "0 View";
	}
	return $count.' Views';
}


function setPostViews($postID) {
	$count_key = 'post_views_count';
	$count = get_post_meta($postID, $count_key, true);
	if($count==''){
		$count = 0;
		delete_post_meta($postID, $count_key);
		add_post_meta($postID, $count_key, '0');
	}else{
		$count++;
		update_post_meta($postID, $count_key, $count);
	}
}

function SearchFilter($query) {
	if ($query->is_search) {
		$query->set('post_type', array('post', 'videos'));
		$query->set( 'category__not_in' , array( 13 ) );
	}
	return $query;
}



/**
 * Extra profile fields
 *
 * @since Themelia 1.0
 */
function themelia_show_extra_profile_fields($user) {
	?>
	<h3>Extra profile information</h3>
	<table class="form-table">
		<tr>
			<th><label for="position"><?php _e('Position', 'themelia'); ?></label></th>
			<td>
				<input type="text" name="position" id="position" value="<?php echo esc_attr(get_the_author_meta('position', $user->ID)); ?>" class="regular-text" /><br />
				<span class="description"><?php _e('Please enter your position in company.', 'themelia'); ?></span>
			</td>
		</tr>
	</table>
	<?php
}
//add_filter('user_contactmethods','my_new_contactmethods',10,1);


function themelia_save_extra_profile_fields($user_id) {
	if (!current_user_can('edit_user', $user_id))
		return false;

	update_user_meta($user_id, 'twitter', $_POST['twitter']);
	update_user_meta($user_id, 'facebook', $_POST['facebook']);
	update_user_meta($user_id, 'position', $_POST['position']);
}

// Remove query string from static files for better caching
function themelia_remove_cssjs_ver( $src ) {
	if( strpos( $src, '?ver=' ) )
	$src = remove_query_arg( 'ver', $src );
	return $src;
}

//Remove or Disable comment-reply.js code
function themelia_clean_header(){
	wp_deregister_script( 'comment-reply' );
}