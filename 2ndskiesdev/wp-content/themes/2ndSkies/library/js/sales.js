jQuery(document).ready(function($){
	initAdminblock();
	$(document).on('click','.header-box .nav a', function(event){
		event.preventDefault();
		var elementClick = $(this).attr("href");
		var destination = $(elementClick).offset().top;
		if (elementClick != '#about_chris' && elementClick != '#buy_course'){
			destination -= 100;
		}
		if($('div').is('#wpadminbar')){
			destination -= $('div#wpadminbar').height();
		}
		if ($.browser.safari || $.browser.chrome) {
		    $('body').animate({ scrollTop: destination }, 1100);
		} else {
		    $('html').animate({ scrollTop: destination }, 1100);
		}
		return false; 
	});
});

function initAdminblock(){
	if($('div').is('#wpadminbar')){	
		$('.header-box').css({'padding-top': $('div#wpadminbar').height()+'px'});
	}
}