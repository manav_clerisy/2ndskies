<?php
/*
Template Name: Online Courses
*/
?>
<?php get_header(); ?>
	<div id="content-course" class="inner">
		</div>
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>
<?php get_footer(); ?>