<?php
/*
 * Template Name: New York Close Forex Charts
 */
?>
<?php get_header(); ?>

<div id="main2">

    <div id="content2" class="new-york-close-forex">
        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container -->

        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>

                <div class="col-box">
                    <div class="col">
                        <h1><?php the_title(); ?></h1>
                        <p>Access Pro Forex Trading Charts with Real-Time Pricing and a FREE $50,000 practice account. Simply fill out the form, download the platform and start trading.</p>
                        <div class="box">
                            <ul class="list">
                                <li><span class="ico-1">$50,000 FREE Practice Account</span></li>
                                <li><span class="ico-2">Pro Forex Charting</span></li>
                                <li><span class="ico-3">Real-Time Pricing</span></li>
                            </ul>
                            <span class="link"><span>try it</span></span>
                        </div>
                    </div>
                    <div class="col-2 view-full-form">
                        <div class="form-wrapper">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>

            <?php endwhile; ?>
        <?php else : ?>

            <div class="default-template">
                <h1>Not Found</h1>
                <p>Sorry, but you are looking for something that isn't here.</p>
            </div><!-- .default-template -->

        <?php endif; ?>

    </div><!-- #content -->

</div><!-- #main -->

<?php get_footer(); ?>