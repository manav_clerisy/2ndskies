<style type="text/css">

.my-mfp-zoom-in .zoom-anim-dialog{opacity:0;-webkit-transition:all 0.2s ease-in-out;-moz-transition:all 0.2s ease-in-out;-o-transition:all 0.2s ease-in-out;transition:all 0.2s ease-in-out;-webkit-transform:scale(0.8);-moz-transform:scale(0.8);-ms-transform:scale(0.8);-o-transform:scale(0.8);transform:scale(0.8);}
.my-mfp-zoom-in.mfp-ready .zoom-anim-dialog{opacity:1;-webkit-transform:scale(1);-moz-transform:scale(1);-ms-transform:scale(1);-o-transform:scale(1);transform:scale(1);}
.my-mfp-zoom-in.mfp-removing .zoom-anim-dialog{-webkit-transform:scale(0.8);-moz-transform:scale(0.8);-ms-transform:scale(0.8);-o-transform:scale(0.8);transform:scale(0.8);opacity:0;}
.my-mfp-zoom-in.mfp-bg{opacity:0;-webkit-transition:opacity 0.3s ease-out;-moz-transition:opacity 0.3s ease-out;-o-transition:opacity 0.3s ease-out;transition:opacity 0.3s ease-out;}
.my-mfp-zoom-in.mfp-ready.mfp-bg{opacity:0.55;}
.my-mfp-zoom-in.mfp-removing.mfp-bg{opacity:0;}
.pop-call, .pop-call *{-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;}
.pop-call{position:relative;padding:0;width:auto;width:870px;max-width:90%;min-height:430px;margin:20px auto;background-color:#cfdbe3;-webkit-box-shadow:-1px 3px 12px 0px rgba(0,0,0,0.75);-moz-box-shadow:-1px 3px 12px 0px rgba(0,0,0,0.75);box-shadow:-1px 3px 12px 0px rgba(0,0,0,0.75);}
.pop-call .mfp-close{background-color:#151e2d !important;color:#ccd8e0;font-family:Arial,Baskerville,monospace;font-size:22px;font-style:normal;height:28px;line-height:28px;opacity:1;position:absolute;right:-14px !important;top:-14px !important;width:28px;-webkit-box-shadow:0px 0px 0px 1px #ccd8e0;box-shadow:0px 0px 0px 1px #ccd8e0;}
.pop-call .mfp-close:active{background-color:#151e2d !important;color:#ccd8e0;height:28px;line-height:28px;right:-14px !important;top:-14px !important;width:28px;}
.home .mfp-bg, .page-template-template-home-php .mfp-bg{opacity:0.55;}
.pop-call{}
.grid-50, .grid-33, .grid-66{float:left;overflow:hidden}
.grid-50{width:50%;}
.grid-33{width:33.3333%;}
.grid-66{width:66.6666%;}
.pop-call h2{font-family:"Open Sans";color:#0074a9;text-shadow:1px 1px 0 rgba(255,255,255,0.6);font-size:1.95em;line-height:1.1em;font-weight:600;letter-spacing:-2px;}
.pop-call h4{font-family:"Open Sans";color:#445878;text-shadow:1px 1px 0 rgba(255,255,255,0.6);font-size:1.6em;font-weight:400;font-style:italic;margin-top:-5px}
.pop-call .pop-left{padding:20px;padding-left:35px;padding-right:25px;}
.pop-call .pop-left{line-height:1.2em;background-image:url(<?php echo THEME_IMAGES ?>/popup-bg.png);background-position:center top;}
.pop-call .pop-left > div{margin-top:40px;}
.pop-call .pop-left > div img{max-width:100%;height:auto}
.pop-call .pop-left ul{list-style:none}
.pop-call .pop-left li{padding:8px 5px 8px 30px;position:relative}
.pop-call .pop-left li:before{background:url("<?php echo THEME_IMAGES ?>/sprite.png") no-repeat scroll -90px 0 rgba(0, 0, 0, 0);content:"";height:19px;left:0;margin:9px 0 0 0px;position:absolute;top:0;width:18px;}
.pop-call .pop-right{background-color:#445878;background-image:url(<?php echo THEME_IMAGES ?>/popup-right-bg.png);min-height:430px;padding:8px;}
.pop-call .pop-right p{color:#FFF;}
p.signup-title{font-family:"Open Sans";font-size:1.2em;font-weight:300;text-transform:uppercase;text-align:center}
p.signup-title span{font-weight:600}
.pop-right-inner{padding-top:20px;border:1px solid #2d3e58;display:block;overflow:hidden;min-height:414px;}
.pop-right-content{padding-left:16px;padding-right:16px}
.pop-right-content p{font-size:.875em;line-height:1.2em;}
.popup-form{margin-top:20px}
.popup-form .moonray-form form input[type="email"],
.popup-form .moonray-form form input[type="text"]{font:13.6px/18px "Source Sans Pro", Arial, Helvetica, sans-serif;background-color:#1e2c41 !important;width:100%;border:1px solid #667c9f !important;padding-top:6px;padding-right:5px;padding-bottom:6px;padding-left:8px;margin: 0 0 10px !important;border-radius:4px !important;color:#FFF;-webkit-box-shadow:inset 1px 1px 2px 0px rgba(0,0,0,0.65);-moz-box-shadow:inset 1px 1px 2px 0px rgba(0,0,0,0.65);box-shadow:inset 1px 1px 2px 0px rgba(0,0,0,0.65);}
.popup-form .moonray-form form input.moonray-form-state-error{border-color: #c20000 !important;}
#popup .popup-form .moonray-form form input[type="submit"]{font-family:"Open Sans";border-radius:4px;background-color:#d20707;background-image:-webkit-linear-gradient(top, #d20707, #a90303);background-image:linear-gradient(to bottom, #d20707, #a90303);width:100% !important;border:1px solid #9D0202;text-transform:uppercase;color:#FFF !important;text-align:center;padding-top:7px;padding-bottom:7px;font-size:15.6px;line-height:22px;font-weight:bold;-webkit-box-shadow:1px 1px 2px 0px rgba(0,0,0,0.1);-moz-box-shadow:1px 1px 2px 0px rgba(0,0,0,0.1);box-shadow:0px 0px 2px 0px rgba(0,0,0,0.1);-webkit-transition:all .5s ease-out;-moz-transition:all .5s ease-out;-ms-transition:all .5s ease-out;-o-transition:all .5s ease-out;margin: 0 0 15px !important;}
#popup .popup-form .moonray-form form input[type="submit"]:hover{background-color:#d20707;background-image:-webkit-linear-gradient(top, #d20707, #a40505);background-image:linear-gradient(to bottom, #d20707, #a40505);}
.popup-form .privacy{background-image:url(<?php echo THEME_IMAGES ?>/lock.png);background-repeat:no-repeat;background-position:left center;padding-left:20px;margin-left:10px}
.popup-form .privacy a{color:#F9F9F9;opacity:.9}
@media only screen and (min-width:768px) and (max-width:1024px){.pop-call .pop-left{padding:15px;padding-left:30px;}
.pop-call .pop-left{line-height:1em}
.pop-call .pop-left > div{margin-top:30px;}
.pop-call .pop-left p{font-size:.975em;}
.pop-call .pop-left li{font-size:.975em;}
p.signup-title{font-size:1em;padding-left:10px;padding-right:10px;}
.pop-call h2{font-size:2em;letter-spacing:-2px;}
.pop-call h4{font-size:1.2em;margin-top:-5px}
}
@media only screen and (max-width:767px){.pop-call .pop-left{padding:15px;padding-left:30px;}
.pop-call .pop-left{line-height:1em}
.pop-call .pop-left > div{margin-top:30px;}
.pop-call .pop-left p{font-size:.875em;}
.pop-call .pop-left li{font-size:.875em;}
.pop-call .pop-right{min-height:216px;margin-bottom:30px;}
.pop-right-inner{padding-top:20px;border:1px solid #2d3e58;display:block;overflow:hidden;min-height:200px;}
p.signup-title{font-size:1em;padding-left:10px;padding-right:10px;}
.pop-call h2{font-size:2em;letter-spacing:-2px;}
.pop-call h4{font-size:1.2em;margin-top:-5px}
.grid-50{width:100%;}
.grid-33{width:100%;}
.grid-66{width:100%;}
}

</style>

<span></span>
<div id="popup" class="pop-call mfp-hide animated animated-Delay-1s fadeInDown ">
    <div class="grid-66 pop-left">
        <h2>Getting an Edge in the Forex Markets</h2>
        <h4>(Free Ebook)</h4>
		<div class="grid-50">
        	<img src="<?php echo THEME_IMAGES ?>/book-ipad.png" alt="Book-iPad" width="267" height="261" /> 
        </div>
        <div class="grid-50">
        	<p>Read our FREE eBook on how to get an edge trading the forex markets:</p>
            <ul>
            	<li>Learn What it Means to Have an Edge</li>
                <li>Learn What are the Key Moves in the Market</li>
                <li>Discover an Intra-day Trading Tool For Precise Entries & Exits</li>
            </ul>
        </div>
    </div>
    <div class="grid-33 pop-right">
    	<div class="pop-right-inner">
         	<p class="signup-title">Sign Up for Our<br> Monthly <span>Newsletter</span></p>
            
            <div class="pop-right-content">
            	<p style="text-align:center">...and get our Free E-Book</p>

                <div class="popup-form">
                    <div class="moonray-form-p2c26856f1 ussr"><div class="moonray-form moonray-form-label-pos-stacked">
                            <form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
                                <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text"><label for="mr-field-element-153568200068" class="moonray-form-label"></label><input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-153568200068" required value="" placeholder=" Your Name"/></div>
                                <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-email"><label for="mr-field-element-558496658457" class="moonray-form-label"></label><input name="email" type="email" class="moonray-form-input" id="mr-field-element-558496658457" required value="" placeholder="Email Address"/></div>
                                <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit"><input type="submit" name="submit-button" value="Sign Up" class="moonray-form-input" id="mr-field-element-520094126695" src/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
                                <div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c26856f1"/></div>


                            </form>
                        </div>
                    </div>
                    <p class="privacy"><a href="/privacy-policy/">We respect your privacy</a></p>
                </div><!-- .subscribe-form -->
                
            </div>
        </div>
    </div>
</div>