<?php
/*
Template Name: Testimonials
*/
?>
<?php get_header(); ?>

<div id="main" class="clearfix">

    <div id="content">

        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container -->

        <div class="default-template">
                <?php wp_reset_postdata(); ?>
                <?php
                    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                    $wp_query = new WP_Query(array(
                        'post_type' => 'testimonial',
                        'paged' => $paged,
                        'posts_per_page' => 10,
                        'post_status'      => 'publish',
                        'orderby'          => 'post_date',
                        'order'            => 'DESC'
                        )
                    );
                ?>
                <h1 class="page-title"><?php the_title(); ?></h1>

                <ul class="list-testimonials">
                    <?php $i = 1; ?>
                    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                        <?php if($i == 5): ?>
                            <li class="inner_banner">
                                <!--<?php echo $i; ?>-->
                                <a href="<?php get_site_url(); ?>/advanced-price-action-course/"><img src="<?php bloginfo('template_directory'); ?>/images/banners3/banner.png" alt="" /></a>
                            </li>
                            <li class="testimonial">
                                <i class="icon-quote"></i>
                                <p class="testimonial--quote"><?php $content = get_the_content(); print_r($content); ?></p>
                                <h3 class="testimonial--title"><?php the_title(); ?></h3>
                            </li>
                        <?php elseif($i == 10): ?>
                            <li class="testimonial">
                                <i class="icon-quote"></i>
                                <p class="testimonial--quote"><?php $content = get_the_content(); print_r($content); ?></p>
                                <h3 class="testimonial--title"><?php the_title(); ?></h3>
                            </li>
                            <li class="inner_banner">
                                <!--<?php echo $i; ?>-->
                                <a href="<?php get_site_url(); ?>/advanced-price-action-course/"><img src="<?php bloginfo('template_directory'); ?>/images/banners3/banner-2.png" alt="" /></a>
                            </li>
                        <?php else: ?>
                            <li class="testimonial">
                                <i class="icon-quote"></i>
                                <p class="testimonial--quote"><?php $content = get_the_content(); print_r($content); ?></p>
                                <h3 class="testimonial--title"><?php the_title(); ?></h3>
                            </li>
                        <?php endif; ?>
                        <?php $i++; ?>
                    <?php endwhile; ?>
                </ul>
                <?php wp_pagenavi(); ?>
                <?php wp_reset_postdata(); ?>
        </div><!-- .default-template -->


    </div><!-- #content -->


	<?php get_sidebar('pages'); ?><!-- sidebar -->

</div><!-- #main -->

<?php get_footer(); ?>