<?php
/*
Template Name: Advanced Traders Page
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <link media="all" rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/library/css/traders-templ.css"/>
	<link media="all" rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/library/css/magnific-popup.css"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
    <?php wp_head(); ?>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>

    <!--  -->
    <div id="mindsetpopup" class="mfp-hide animated fadeInDown">
		<!--<a href="#" class="cancel">&times;</a>-->
		<div class="popup-info">
			<h2>The Advanced Traders Mindset Course</h2>
			<h3>Build a Successful Trading Mindset</h3>
			<img src="<?php bloginfo( 'template_url' ); ?>/library/images/popupimage.png" alt="Advanced Traders">
			<ul>
				<li>Think &amp; Trade Like A Pro</li>
				<li>Train Your Brain, Change Your Life</li>
				<li>Become A Consistently Successful Trader</li>
			</ul>
		</div>
		<div class="popup form">

			<div><strong>Version 1</strong> is closed as of <strong>April 2nd</strong>. But Version 2 Opens later this year. Sign up to get early seating priority for <strong>Version 2</strong> as it will fill up.</div>
			<link rel="stylesheet" href="//app.ontraport.com/js/formeditor/moonrayform/paymentplandisplay/production.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.default.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/formeditor/formeditor/css/form.publish.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/minify/?g=moonrayCSS" type="text/css" /><link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" type="text/css" /><link rel="stylesheet" href="//forms.ontraport.com/v2.4/include/formEditor/gencss.php?uid=p2c26856f2" type="text/css" /><script type="text/javascript" src="//forms.ontraport.com/v2.4/include/formEditor/genjs-v3.php?html=false&uid=p2c26856f2"></script><div class="moonray-form-p2c26856f2 ussr"><div class="moonray-form moonray-form-label-pos-stacked">
			<form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
				<input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-338647091994" required value="" placeholder="Name"/>
				<input name="email" type="email" class="moonray-form-input" id="mr-field-element-478505307342" required value="" placeholder="Email"/>
				<input type="submit" name="submit-button" value="Sign Up" class="moonray-form-input" id="mr-field-element-542974830139" src data-lastDisplayVal="Sign Up"/>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
				<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c26856f2"/></div>
			</form>
			</div>
			</div>

			<p class="privacy">
				<a href="/privacy-policy/">We respect your privacy</a>
			</p>
		</div>
	</div>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->
<div class="header-box">
    <div class="holder">
        <div class="nav-box">
            <ul class="nav">
                <li><a href="#what_you_learn">What You'll Learn</a></li>
                <li><a href="#what_you_get">What You Will Get</a></li>
                <li><a href="#about_chris" >About Chris</a></li>
            </ul>
            <a class="logo"> Second Skies</a>
            <ul class="nav">
                <li><a href="#sample_lesson">Sample Lesson</a></li>
                <li><a href="#enroll">Enroll Now</a></li>
            </ul>
        </div>
        <div class="mobile-nav">
            <div class="menu-btn" id="menu-btn">
                <div></div>
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="responsive-menu">
                <ul>
                    <li><a href="#what_you_learn">What You'll Learn</a></li>
                    <li><a href="#what_you_get">What You Will Get</a></li>
                    <li><a href="#about_chris" >About Chris</a></li>
                    <li><a href="#traders">What Traders Are Saying</a></li>
                    <li><a href="#sample_lesson">Sample Lesson</a></li>
                    <li><a href="#enroll">Enroll</a></li>
                </ul>
            </div>
        </div>
        <h1>The Advanced Traders Mindset Course</h1>
        <h2>Think Successfully &amp; Trade Profitably</h2>
        <div class="box-holder">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/advanced_traders.png" alt="Advanced Traders">
            </div>
            <ul class="list">
                <li>Build a Mindset To Take On Any Obstacle</li>
                <li>Develop Real Confidence, Make Money Trading</li>
                <li>Cultivate Success in Your Trading &amp; Life</li>
            </ul>
        </div>
    </div>
</div>
<div class="text-box enroll tighter">
    <div class="holder testimonial">
        <blockquote><p><span class="block-quote">“</span>  &nbsp; I've learned more about being successful in trading, business &amp; life from this course in the last few weeks
          than I have in my past 28 years.  There is finally a light at the end of the tunnel.  &nbsp; <span class="block-quote">”</span></p><cite>Justin, Canada</cite></blockquote>
    </div>
    <div class="holder">
        <a href="http://courses.2ndskiesforex.com/courses/startPayment?id=4&type=creditCard">Yes, I Want To Wire My Brain For Success</a>
    </div>
    <div class="paypal">
         <img src="<?php bloginfo( 'template_url' ); ?>/library/images/paypal.png" alt="Cards">
    </div>
</div>

<div class="text-box chris">
    <div class="holder">

        <div class="img-holder">
            <img src="<?php bloginfo( 'template_url' ); ?>/library/images/chris.png" alt="">
        </div>

        <div class="info">
            <ul>
                <li>Wondering why you keep making the same mistakes over and over again?</li>
                <li>Constantly getting out of your winners too early?</li>
                <li>Doubt you can make money trading?</li>
            </ul>

            <hr/>

            <blockquote>
                <p>Hi, I'm Chris Capre.  I've spent the last two decades studying one thing &mdash; using Neuroscience and 
                   meditation to create a peak performance brain and mindset.</p>
                <p>During this time, I've been a broker on Wall Street, traded for a hedge fund, and been meditating every day.</p>
                <p>In this Advanced Traders Mindset Course, I'm using my last two decades of experience and training for one goal
                   &mdash; to help you build a winning mindset &amp; make money trading.</p>
            </blockquote>
        </div>
    </div>
</div>
<div id="what_you_learn" class="holder testimonial-atm">
    <h1>What You'll Learn in the ATM Course</h1>
        <h2>The Advanced Traders Mindset Course is a <span>32 Lesson Video + Audio Based Training</span>, which gives you the tools, strategies and training to build a successful trading mindset.</h2>
        <div class="lesson">
            <div class="head">
                <p>Lesson 1: <span>Neuroplasticity & Understanding Your Trading Brain</span></p>
                Learn How Your Brain Adapts, Changes & Wires New Skills
            </div>
            <div class="content">
                <div class="column left">
                    <h3>The Story of Success You Were Told Was Wrong</h3>
                    <ul>
                        <li>Success is Not A Lottery</li>
                        <li>Success is in the Brain (find out how)</li>
                        <li>How Neuroplasticity Affects My Performance</li>
                    </ul>
                    <h3>Using This Training to Improve Our Brains For...</h3>
                    <ul>
                        <li>Increasing Clarity</li>
                        <li>Building Mental Sharpness for Better Trading</li>
                        <li>Beating & Transforming Negativity</li>
                        <li>Achieving Your Trading Goals</li>
                    </ul>
                    <h3>Inside Is the Key</h3>
                    <ul>
                        <li>How Our Brains Work</li>
                        <li>Why & How To Use A Trading Feedback Loop</li>
                        <li>Increasing Your Focus, Intention & Clarity</li>
                        <li>How to Re-Wire Your Brain For Success</li>
                        <li>Overcoming the fears & doubts you have</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>How to Really Leverage Repetition</h3>
                    <ul>
                        <li>Avoid Being A Parrot Which Repeats w/o Thinking</li>
                        <li>Why Self-Talk has Failed You Before</li>
                        <li>Understanding Mirror Neurons</li>
                        <li>The Repetition List You Need to Know</li>
                    </ul>
                    <h3>The Core Rules of Neuroplasticity</h3>
                    <ul>
                        <li>What Are the Main Rules?</li>
                        <li>What is SDN & Why You Need to Learn It?</li>
                        <li>The First Tool & Keys to Enhancing Neuroplasticity</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 2: <span>The Whole Brain State</span></p>
                What is it & Why You Need It
            </div>
            <div class="content">
                <div class="column left">
                    <h3>The Left & Right Brain Myth</h3>
                    <ul>
                        <li>Why this myth led you astray about the brain</li>
                        <li>What is a more ideal state for success in trading</li>
                        <li>How Einstein's brain was different</li>
                        <li>Why our common state fails to produce success</li>
                    </ul>
                    <h3>Stress Kills This State</h3>
                    <ul>
                        <li>Your body's most common reaction to stress</li>
                        <li>How this decreases higher cognitive performance</li>
                        <li>How trading success is extremely difficult in this state</li>
                        <li>Why you are in this state more often than you think</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>Neuroscience + Ancient Traditions</h3>
                    <ul>
                        <li>Why knowing something intellectually doesn't produce results</li>
                        <li>Neuroscience + Ancient Traditions coming together</li>
                        <li>How to get into the whole-brain state</li>
                        <li>How the whole brain state relates to the 'zone'</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 3: <span>Getting To Know Your Brain</span></p>
                Why Learning About The Brain Increases Cognitive Performance
            </div>
            <div class="content">
                <div class="column left">
                    <h3>Awareness Of the Brain & Cognitive Performance</h3>
                    <ul>
                        <li>How the experiment of the maids relates to your cognitive performance</li>
                        <li>Learning to Visualize & Correlate Trading towards Brain Functions</li>
                    </ul>
                    <h3>The Basics of A Trading Thought In Your Brain</h3>
                    <ul>
                        <li>Nerve Cells, Thoughts Per Day, & The Basic Thought Process</li>
                        <li>Brain Cells & How They Communicate Trading Thoughts</li>
                        <li>Thoughts being Physical, Chemical & Electrical</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>Neural Networks</h3>
                    <ul>
                        <li>The basics of Neural Networks & Trading</li>
                        <li>How learning occurs in Neural Networks</li>
                        <li>How to Strengthen Neural Networks</li>
                        <li>Why It's Hard to Change</li>
                        <li>How to Wire Neural Networks for Trading</li>
                    </ul>
                    <h3>The Most Important Law for Acquiring Skills In the Brain</h3>
                    <ul>
                        <li>The 4 elements to wire skills into our brain</li>
                        <li>Why you haven't fully wired in successful trading skills yet</li>
                        <li>What was my secret to wiring trading skills in my brain</li>
                    </ul>

                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 4: <span>The Three Parts of Your Mind for Trading</span></p>
                Why You Have to Know These For Successful Trading
            </div>
            <div class="content">
                <div class="column left">
                    <h3>What are the 3 Parts of Your Mind?</h3>
                    <ul>
                        <li>Which one we use the most</li>
                        <li>Which two we use the least</li>
                        <li>Why what you think about the brain is wrong</li>
                        <li>How these 3 parts tell you everything about your performance</li>
                        <li>Why you have analysis paralysis</li>
                    </ul>
                    <h3>Which Parts Self-Talk Affects</h3>
                    <ul>
                        <li>Understand how Self-Talk fails you</li>
                        <li>How to use Self-Talk for Trading Success</li>
                        <li>Moving from Negative to Constructive Thinking</li>
                        <li>Where your performance really comes from</li>
                    </ul>
                    <h3>Building Your Trading Skills</h3>
                    <ul>
                        <li>How to make trading skills automatic</li>
                        <li>Getting past analysis paralysis</li>
                        <li>How to pull the trigger when you need to</li>
                        <li>Who will win the trading fight & why</li>
                        <li>Where the 'Zone' comes from & How to get there</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>The Inner Conflicts You Have</h3>
                    <ul>
                        <li>How they affect your trading performance</li>
                        <li>Where your cognitive dissonance comes from</li>
                        <li>Why you will perform like this 95% of the time</li>
                    </ul>
                    <h3>Labeling While Trading</h3>
                    <ul>
                        <li>What part of the mind Labeling affects</li>
                        <li>How you can make labels work for you in trading</li>
                        <li>How much of your successful trading hinges upon this</li>
                    </ul>
                    <h3>Your Comfort Zone in Trading</h3>
                    <ul>
                        <li>Why you always make the same mistakes	</li>
                        <li>What really is the comfort zone</li>
                        <li>What does your comfort zone mean about you</li>
                        <li>How to expand your comfort zone for success</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 5: <span>Mental +R For Trading</span></p>
                How to Execute What You Want & Need to While Trading
            </div>
            <div class="content">
                <div class="column left">
                    <h3>How Vacation led this Man to A Russian Prison</h3>
                    <ul>
                        <li>What 9 Years in a Russian Prison Did to this Man</li>
                        <li>What mental activity he did for 9 years</li>
                        <li>How this man build neural networks for success in Prison</li>
                        <li>From a Russian Prison to Beating A World Champion</li>
                    </ul>
                    <h3>A Superbowl Champion& Golfing Legend</h3>
                    <ul>
                        <li>How Using Mental R created 86% accuracy</li>
                        <li>Practicing Mental R from the 6th Grade</li>
                        <li>Using Mental R to become a 2x MVP </li>
                        <li>How Every Shot He Used Mental R</li>
                    </ul>
                    <h3>When Does the Root Of Your Performance Start?</h3>
                    <ul>
                        <li>Best times of the day to use Mental R</li>
                        <li>Making Success More Real than Failure</li>
                        <li>How to wire a trading skill in 21 days</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>Emotions in Trading</h3>
                    <ul>
                        <li>How to have emotions actually help our trading</li>
                        <li>What you have to avoid emotionally when trading</li>
                    </ul>
                    <h3>Good vs. Bad Performances In Your Mind</h3>
                    <ul>
                        <li>What to see in your trading day & what not to</li>
                        <li>Mental R & The 3 Phases of Trading</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 6: <span>The 3 Phases of Trading You Must Know</span></p>
                Why you need to understand these to properly train
            </div>
            <div class="content">
                <div class="column left">
                    <h3>What are the 3 Phases of Trading?</h3>
                    <ul>
                        <li>How your C, SC & UC Play Into These 3 Phases</li>
                        <li>How to apply a Mind Program to Each Phases</li>
                        <li>What to do specifically during each phase of trading</li>
                        <li>How to use cues during these phases</li>
                        <li>How the 3 Phases Relate to the 3 Parts of Your Mind</li>
                        <li>What is the Most Important Phase?</li>
                        <li>Building Constructive Habits for All 3 Phases</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>Correcting Trading Errors</h3>
                    <ul>
                        <li>Why you haven't been able to correct your trading errors</li>
                        <li>Which phase & part of your mind for correcting trading errors</li>
                        <li>Increasing Your Edge for Success</li>
                        <li>Decreasing the likelihood of failing</li>
                        <li>Why your trading journal is inadequate</li>
                        <li>What you are missing in your trading journal</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 7: <span>The Self-Image, Performance &amp; Your Comfort Zone</span></p>
                Why you continue to make the same mistakes
            </div>
            <div class="content">
                <div class="column left">
                    <h3>Why You May Not Want to Win the Lottery</h3>
                    <ul>
                        <li>Learn about the Preacher who won the lotto</li>
                        <li>This man was over $1 Billion in Debt, Now Worth Billions</li>
                    </ul>
                    <h3>The Self-Image</h3>
                    <ul>
                        <li>What is the Self-Image?</li>
                        <li>How does the Self-Image affect my trading performance?</li>
                        <li>Where do my habits, attitudes & Self-Beliefs reside?</li>
                        <li>How the Self-Image relates to the 3 parts of my mind</li>
                        <li>Using Neuroplasticity for the Self-Image</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>The Comfort Zone</h3>
                    <ul>
                        <li>How the Self-Image affects Your Comfort Zone</li>
                        <li>The Self-Image you think you have is likely not accurate</li>
                        <li>What Kind of Comfort Zone an Elite Performer Has</li>
                        <li>How to build a Comfort Zone for Elite Performance</li>
                    </ul>
                    <h3>Performance in Trading</h3>
                    <ul>
                        <li>Why most fail at trading</li>
                        <li>How to avoid being the 90% who fail</li>
                        <li>What you need to change to be in the top 10%</li>
                        <li>What you should never do if you want to be successful</li>
                        <li>The 3 phases of performance, growth & life</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 8: <span>The DA Method for Re-Writing Your Self-Image</span></p>
                A Misunderstood Method Which Made You A Parrot
            </div>
            <div class="content">
                <h3>The DA Method</h3>
                <ul>
                    <li>How you used this before and were just being a Parrot</li>
                    <li>What is the DA Method & how does it work?</li>
                    <li>The difference between Present & Future tense with this method</li>
                    <li>Building a Map to Success with this Method</li>
                    <li>Why it is 'Like Me' to trade successfully or not</li>
                    <li>How to correct trading errors with the DA method</li>
                    <li>How to optimize the DA method</li>
                </ul>
                <div class="column right">

                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 9: <span>Isolating &amp; Building Specific Trading Skills</span></p>
                How to Build the Trader You Want to Be
            </div>
            <div class="content">
                <div class="column left">
                    <h3>Elite Performers</h3>
                    <ul>
                        <li>How they train to become elite</li>
                        <li>What differentiates elite training from yours</li>
                        <li>How their learning process is different</li>
                        <li>How you can become an elite performer</li>
                        <li>What most new traders fail to realize & do</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>Breaking A Strategy or Trading Skill Down</h3>
                    <ul>
                        <li>What you need to think, train & learn to break a skill down</li>
                        <li>How to break a strategy down</li>
                        <li>The 5 Components of Each Strategy</li>
                        <li>Using this as a model for your training process</li>
                        <li>How training like this will change your mind for success</li>
                        <li>From a +250% return to a +540% return</li>
                        <li>Taking the Deep Dive in Strategies & Trading Skills</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 10: <span>Avoiding Problem Focus &amp; Solving Your Biggest Challenges</span></p>
                Wiring Neurological Success Into Your Brain
            </div>
            <div class="content">
                <h3>Changing Your Focus From What You've Been Doing</h3>
                <ul>
                    <li>Why changing your focus this way can improve neurological performance</li>
                    <li>What you have to avoid with your focus</li>
                    <li>Why you should give up 90% of these things you are focusing on</li>
                    <li>What successful people focus on daily</li>
                    <li>Focus & the 3 Parts of Your Mind</li>
                    <li>One of the most valuable practices for your trading performance</li>
                </ul>
                <div class="column right">

                </div>
            </div>
        </div>

        <div class="testimonial">
            <div class="testimonial-bg">
                <blockquote>
                    <p><span class="block-quote">“</span> &nbsp; The ATM course is phenomenal!  Taking part in this course is the most important thing I've 
            ever done in trading.  By far the best investment I've ever made. &nbsp; <span class="block-quote">”</span></p>
                </blockquote>
            </div>
            <div class="author-bg">
                <cite>James, UK</cite>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 11: <span>Force Multipliers for Trading</span></p>
                Using Physics &amp; These Methods to Increase Your Training Results
            </div>
            <div class="content">
                <h3>Physics &amp; an Old Institution Use This</h3>
                <ul>
                    <li>What are the 5 Force Multipliers for Trading</li>
                    <li>What is the ultimate force multiplier?</li>
                    <li>How Elon Musk Uses Force Multipliers to Become A Billionaire</li>
                    <li>What you can learn from Jelly Beans About Force Multipliers</li>
                    <li>Why you need to Ban this to become successful in trading</li>
                    <li>What is the 4/6/10 rule for force multipliers?</li>
                    <li>How I got from A-Z faster than most</li>
                    <li>Where does Intelligence fit into my training process</li>
                    <li>Different Ways to Have More Money</li>
                    <li>Two Laws Affecting Your Trading Performance</li>
                </ul>
                <div class="column right">

                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 12: <span>Building Your Own Mindset Program</span></p>
                Putting it All Together To Create Your Own Mindset Program
            </div>
            <div class="content">
                <div class="column left">
                    <h3>Building Your Own Mindset Program</h3>
                    <ul>
                        <li>Why all the training I've done before got me to where I am now</li>
                        <li>Identifying what you need to strengthen & build the most</li>
                        <li>What should you work on first to turn my trading performance around</li>
                        <li>How to use all these methods to overcome your biggest challenges</li>
                    </ul>
                </div>
                <div class="column right">
                    <h3>Accelerating Your Training Process</h3>
                    <ul>
                        <li>How long should you train to optimize my learning process</li>
                        <li>How to avoid over-training</li>
                        <li>Creating the shortest learning curve for successful trading</li>
                        <li>How much trading and training should you do in a year</li>
                        <li>How to measure my trading progress</li>
                        <li>The proper way to create goals for trading success</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 13: <span>Rewiring The Negativity Bias</span></p>
                Removing the biggest obstacle to your success in trading &amp; life
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>What is the RAD practice?</li>
                        <li>How can you eliminate negative precursors?</li>
                        <li>How to build a positive mindset for success?</li>
                        <li>What is the sandwich practice and why you need it?</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 14: <span>Building Emotional IQ for Success in Trading</span></p>
                How to build up emotional resilience &amp; IQ for trading
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>Neurological effects/basis behind emotions</li>
                        <li>Changing your relationship to emotions</li>
                        <li>Why & how you need to build up your Emotional IQ for success in trading</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 15: <span>Forging Mental Toughness</span></p>
                Building an unstoppable mindset
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>How to face challenges from trading and life</li>
                        <li>The frequency of challenges and why this matters</li>
                        <li>How to re-wire your brain for overcoming obstacles in trading</li>
                        <li>Getting rid of excuses and performing at your best</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 16: <span>Building A No-Limit Mindset</span></p>
                How to break through obstacles and take things to the next level
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>A Seal story about swimming</li>
                        <li>Why you have a limited mindset right now & how that kills your success</li>
                        <li>How to expand beyond any problem in trading or life</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 17: <span>Building A Mindset of Abundance</span></p>
                How to build abundance for yourself and others
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>What an 'abundant' mindset really is</li>
                        <li>Why you need an abundant mindset for trading & financial success</li>
                        <li>Visualization techniques to build a mindset of abundance</li>
                        <li>Key techniques for building financial abundance in your life</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 18: <span>Goal Setting for Success &amp; Attainment</span></p>
                Why you are not reaching your goals &amp; what you should be targeting for
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>What is CL & why this affects your training</li>
                        <li>Why your current trading goals need to be thrown in the trash</li>
                        <li>How to build a winning mindset</li>
                        <li>How to attain your goals (it's not what you think)</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head">
                <p>Lesson 19: <span>Cultivating Success, Winning In Your Mind</span></p>
                Training to be an elite performer and why you aren't training like this now
            </div>
            <div class="content">
                <div class="column left">
                    <ul>
                        <li>Training to be an elite performer and why you aren't training like this now</li>
                        <li>How to train properly</li>
                        <li>How to win from the beginning</li>
                        <li>Why your current vision of success won't manifest</li>
                        <li>Being successful starting today</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="lesson">
            <div class="head hide_lesson">
                <p>Lesson 20: <span>Bonus Lesson</span></p>
            </div>
        </div>

        <div class="testimonial">
            <div class="testimonial-bg">
                <blockquote>
                    <p><span class="block-quote">“</span> &nbsp; It is now clear to me why I've had inconsistent performance in my trading.  I've learned more about myself in a few lessons than
          I have in all the years and books combined. &nbsp; <span class="block-quote">”</span></p>
                </blockquote>
            </div>
          <div class="author-bg">
          <cite>Ivan, Bulgaria</cite>
          </div>
        </div>

        <!-- <br />
        <br />
        <a href="http://2ndskiesforex.com/course-files/Full-ATM-Course-Schedule.pdf" target="_blank">Click Here for Full Course Schedule</a> -->

</div>

<div id="meditation">
    <div class="holder">
        <h1>Meditation Lesson Series</h1>
        <article>
            <p>Tailored for trading, this 12 lesson series (audio based) begins with the foundational practice and works your way up through a progression.</p>
            <p>Each lesson gives you a base, technique and experience which builds upon the prior lesson.</p>
            <p>Carefully created for trading, this series is built from my 6500+ hours of meditation practice, combined with my 15+ years of trading live money.</p>
            <p>In literally a few weeks, you could physically have a new trading brain &mdash; one that is more focused, aware, mindful, clear, with greater pattern recognition and increased emotional IQ.</p>
        </article>

        <div class="img-holder">
            <img src="<?php bloginfo( 'template_url' ); ?>/library/images/meditation.jpg" alt="Meditation Series">
        </div>
    </div>
</div><!-- END meditation -->

<div id="what_you_get">
    <div class="holder">
        <h1>What You'll Get in the ATM Course</h1>
        <ul class="news-headlines">
            <li class="selected">20 Video Lessons</li>
            <li>12 Lesson Meditation Series</li>
            <li>Once A Month Member Calls</li>
            <li>Ongoing Member Webinars </li>
            <!-- li.highlight gets inserted here -->
        </ul>

        <div class="news-preview">

            <div class="news-content top-content">
                <p><span>Get Lifetime-Access</span> to the 20 Video Training Lessons, the private members area, the Brain Vault, and all bonus video lessons.</p>
            </div><!-- .news-content -->

            <div class="news-content">
                <p class="two"><span>Get Lifetime-Access</span> to the 12 Lesson Meditation Series, and all future bonus meditation lessons, along with Q&A access to Chris Capre about the specific meditation practices.</p>
            </div><!-- .news-content -->

            <div class="news-content">
                <p class="three">Every month, get on a call with Chris, Aruna & other members, where you can personally ask your questions, share your experiences, and connect with other members.</p> <p>* All calls are scheduled in advance, recorded and made available for members</p>
            </div><!-- .news-content -->

            <div class="news-content">
                <p class="four"><span>Get Lifetime Access</span> to our monthly member webinars, going over key topics, new lessons, tackling your biggest challenges and providing ongoing lessons so the training never stops.</p> <p>* All webinars are recorded and made available for members</p>
            </div><!-- .news-content -->

        </div><!-- .news-preview -->
    </div>
</div>

<div class="holder" id="about_chris">

        <div class="testimonial">
          <div class="testimonial-bg">
          <blockquote>
            <p><span class="block-quote">“</span> &nbsp; The ATM course is unbelievable.  I don't think I've ever been exposed to so much new and valuable information about trading psychology and how my mind works in such a 
          short space of time. &nbsp; <span class="block-quote">”</span></p>
          </blockquote>
      </div>
      <div class="author-bg">
          <cite>Paul, UK</cite>
      </div>
        </div>

    <div class="img-holder">
        <img src="<?php bloginfo( 'template_url' ); ?>/library/images/chris_capre.jpg" alt="Chris Capre">
    </div>

    <article>
        <h2>The Passion Behind This Course</h2>
        <p>Hi, I'm Chris Capre.  I've been trading professionally since 2001.  I've practiced yoga and meditation every day for the last 15 years, completed a 1 year meditation retreat
           and done almost 7000 hours of meditation practice.</p>
        <p>I've been studying <strong>Neuroscience and Peak Performance</strong> for the last 20 years with one focus &mdash; to improve ones' brain, mindset and life.</p>
        <p>My goal with this course is simple &mdash; to <strong>change the way you think, trade and perform.</strong></p>
        <p>Without a doubt, the mind you come into this course with will not be the mind you leave with.</p>
        <p>If you want to <strong>re-wire your brain for success in trading,</strong> join this course and change your life.</p>
    </article>

</div>
<?php
if (false):
?>
<div id="traders">
    <div class="holder">
        <h1>What Traders Are Saying About Us</h1>
        <div class="testimonials-slider">

            <div class="slide">
                <div class="testimonials-carousel-context">
                    <div class="testimonials-carousel-content"><p>"I appreciate all your articles about trading mindset and psychology. It influenced my professional and private life as well. Even my friends noticed, that I am going through some changes. I think, I see much better now, what is and what is not important in our lives. I am really glad I found you, so I just wanted to say 'Thank you!' :)"</p></div>
                    <div class="testimonials-name">Petra S.</div>
                </div>
            </div>

            <div class="slide">
                <div class="testimonials-carousel-context">
                    <div class="testimonials-carousel-content"><p>"I've learned that I haven't always approached things with the right mindset or even the right motivation. That's a hard truth. Even though there is still much work to be done, I believe I've become a better person. As far as trading goes, I've been able to take the losers less personally than I normally would have. I've been approaching my days with a little more enthusiasm, standing a little taller. Little steps..."</p></div>
                    <div class="testimonials-name">Jonathan H.</div>
                </div>
            </div>

            <div class="slide">
                <div class="testimonials-carousel-context">
                    <div class="testimonials-carousel-content"><p>"This training has far surpassed my expectations. I don't beat myself up anymore when I make a mistake, for one. That's huge for me. My negative self talk has pretty much stopped."</p></div>
                    <div class="testimonials-name">Dom D.</div>
                </div>
            </div>

            <div class="slide">
                <div class="testimonials-carousel-context">
                    <div class="testimonials-carousel-content"><p>"I am so happy that I found you and happy to learn a lot from you, not just about trading, but about becoming a better person in life. You were born to teach and educate, you have a gift. Keep going on your great mission."</p></div>
                    <div class="testimonials-name">Florin</div>
                </div>
            </div>

        </div>

    </div>
</div>
<?php
endif;
?>

<div id="sample_lesson">
    <div class="holder">
        <div class="img-holder">
            <iframe width="500" height="330" src="//www.youtube.com/embed/pR9t7jZNV0c?autohide=1&amp;rel=0&amp;showinfo=0&amp;wmode=transparent;hd=1" frameborder="0" allowfullscreen></iframe>
        </div>
        <h2>Preview three sample lessons from the course.</h2>
    </div>
</div>

<div id="enroll">
<div class="testimonial-container">
    <div class="testimonial">
      <blockquote>
        <p><span class="block-quote">“</span> &nbsp; I just got funded $100,000 from an investor.  The ATM &amp; Price Action course were so helpful during this process. You are
       a great inspiration to me and this is the first step on my road to success.&nbsp; <span class="block-quote">”</span></p>
      </blockquote>
      <cite>Harkanwalpreet, India</cite>
    </div>
</div>
<div class="enroll-bg">
    <div class="holder course">
        <h1>Enroll in the Advanced Traders Mindset Course</h1>
        <div class="box-holder">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/mindset.png" alt="Advanced Trader Mindset">
            </div>

            <div class="text">
                <h2>ONLY OPEN UNTIL <span>DECEMBER 31ST, 2015</span></h2>
                <p>Tuition:</p>
                <p><span>$329 USD</span> for Current Course Members</p>
                <p><span>$399 USD</span> for Non-Course Members</p>
                <p>*One-time fee</p>
                <p>*Includes Life-Time Access</p>
                <p>*Ongoing Monthly Member Webinars</p>
            </div>
        </div>
        <a class="join" href="http://courses.2ndskiesforex.com/courses/startPayment?id=4&type=creditCard">Yes, I Want To Wire My Brain for Success</a>
        <div class="paypal"> <img src="http://2nd-skies-forex.com/wp-content/themes/2ndSkies/library/images/paypal.png" alt="Cards"></div>

    </div>
</div>
</div>
<div class="footer">
    <div class="holder">
        <span class="copyright">Copyright  	&copy; 2007 - <?php echo date("Y"); ?> 2ndSkies Forex. All rights reserved.</span>
    </div>
</div>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/scroller.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/vertical.slider.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/jquery.bxslider.min.js"></script>
<!--[if IE]><script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/ie.js"></script><![endif]-->

<script type="text/javascript">
    jQuery(function($){
        $( '.menu-btn' ).click(function(){
            $('.responsive-menu').toggleClass('expand')
        })
    })
</script>

<!-- DEMO JS -->
<script>
    $('.testimonials-slider').bxSlider({
        slideWidth: 800,
        minSlides: 2,
        maxSlides: 2,
        slideMargin: 32,
        auto: true,
        autoControls: true
    });
</script>
<script>
    $(document).ready(function(){
        $('div.lesson').hover(function(e){
            e.preventDefault();
            if ($(this).hasClass( "current" ) == true ) {
                $(this).removeClass('current');
            } else {
                $(this).addClass('current');
            }
            $(this).closest('div').find('.content').not(':animated').slideToggle(1000);
        });
    });
</script>
</script>

</script>
<script type="text/javascript">
    adroll_adv_id = "PHTC33ZBENG7JL6Y47TGXA";
    adroll_pix_id = "7RZ45EMH7ZBZ3LZE2H5COL";
    (function () {
        var oldonload = window.onload;
        window.onload = function(){
            __adroll_loaded=true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if(oldonload){oldonload()}};
    }());
</script>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
    twttr.conversion.trackPid('l5in3');</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l5in3&p_id=Twitter" />
    <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l5in3&p_id=Twitter" /></noscript>
<?php wp_footer(); ?>
</body>
</html>
