<?php
/*
Template Name: Sales Page - Cloned
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 " />
    <link media="all" rel="stylesheet" href="<?php bloginfo( 'template_url' ); ?>/library/css/sales-templ.css"/>
    <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo( 'template_url' ); ?>/favicon.ico">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
    var _vwo_code=(function(){
    var account_id=61633,
    settings_tolerance=2000,
    library_tolerance=2500,
    use_existing_jquery=false,
    // DO NOT EDIT BELOW THIS LINE
    f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WLRNLV"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-WLRNLV');</script>
<!-- End Google Tag Manager -->
<div class="header-box">
    <div class="holder">
        <div class="nav-box">
            <ul class="nav">
                <li><a href="#what_you_learn">What you Learn</a></li>
                <li><a href="#trade_setups">Trade Setups</a></li>
                <li><a href="#testimonials">Testimonials</a></li>
            </ul>
            <a href="<?php echo home_url( '/' ); ?>" class="logo"> Second Skies</a>
            <ul class="nav">
                <li><a href="#sample_lesson">Sample Lessons</a></li>
                <li><a href="#about_chris" >About Chris</a></li>
                <li><a href="#buy_course">Buy Course</a></li>
            </ul>
        </div>
        <div class="mobile-nav">
            <div class="menu-btn" id="menu-btn">
                <div></div>
                <span></span>
                <span></span>
                <span></span>
            </div>

            <div class="responsive-menu">
                <ul>
                    <li><a href="#what_you_learn">What You'll Learn</a></li>
                    <li><a href="#what_you_get">What You Will Get</a></li>
                    <li><a href="#about_chris" >About Chris</a></li>
                    <li><a href="#traders">What Traders Are Saying</a></li>
                    <li><a href="#sample_lesson">Sample Lesson</a></li>
                    <li><a href="#enroll">Enroll</a></li>
                </ul>
            </div>
        </div>
        <h1>The 2ndSkies <b>Advanced Price Action</b> Course</h1>
        <strong>Take Your Forex Trading to the Next Level</strong>
        <div class="box-holder">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-1.png" alt="">
            </div>
            <ul class="list">
                <li>Sharpen Your Trading Edge</li>
                <li>Improve Your Day-to-Day Performance</li>
                <li>Build a Successful Trading Mindset</li>
                <li>Increase Your Confidence, Discipline &amp; Consistency</li>
            </ul>
        </div>
    </div>
</div>
<div class="text-box">
    <div class="holder">
        <blockquote>
            <p>"<b>Elite performers</b> put more into training than the actual event.<br>
                <b>After this course</b>, you will never see price action the same.<br>
                <b>Become the next elite trader.</b>"
            </p>
            <cite>
                <span>Chris Capre</span>
                Founder of 2nd Skies
            </cite>
        </blockquote>
        <div class="img-holder">
            <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-2.png" alt="">
        </div>
    </div>
</div>
<div class="holder post-holder">
    <h2>Register today and get immediate access to:</h2>
    <div class="add-holder-post">
        <div class="post">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-3.png" alt="">
            </div>
            <div class="info" id="what_you_learn">
                <h3>40+ Hours of Price Action Course Videos</h3>
                <p>The videos are packed with examples, strategies, and advanced price action techniques. Each one is clear and specific. Watch and see exactly how Chris Capre, founder of 2nd Skies, does his own trading.</p>
            </div>
        </div>
        <div class="post">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-4.png" alt="">
            </div>
            <div class="info" id="trade_setups">
                <h3>Daily Trade Setups Commentary</h3>
                <p>Every day, you'll get access to Chris Capre's analysis on the market. Check out his daily trade setups and compare them to what you're doing. Find out what key levels, entry locations and trend direction Chris sees as the most dominant in the market.</p>
            </div>
        </div>
        <div class="post">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-5.png" alt="">
            </div>
            <div class="info">
                <h3>Live Trade Setups Forum</h3>
                <p>Chris Capre, senior traders and forum members post trade setups on the forum - so you always have lots of eyes on the market, across a range of major indices and commodities. Look for specific trade setups on specific instruments.</p>
            </div>
        </div>
        <div class="post last-post">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-6.png" alt="">
            </div>
            <div class="info">
                <h3>Build A Successful Trading Mindset</h3>
                <p>Chris has been studying Neuroscience for the past two decades. He has been meditating every day for the last 14 years. Because of this, the course includes special modules on developing a successful trading mindset - because even a 1% difference in your mindset could drastically improve your performance. </p>
            </div>
        </div>
    </div>
</div>
<div class="holder" id="testimonials">
    <div class="title-box">
        <h2>Your Membership Includes:</h2>
    </div>
    <div class="col-box">
        <div class="box">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-1.jpg" alt="">
            </div>
            <strong>Private Monthly Member Webinars</strong>
        </div>
        <div class="box">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-2.jpg" alt="">
            </div>
            <strong>One-on-One Follow-Up Session With Chris Capre</strong>
        </div>
    </div>
</div>
<div class="holder blockquote-box">
    <div class="col">
        <blockquote>
            <p>"I've been trading for about 2 years, with very mixed and unprofitable results. I could never stick with a strategy or get a good consistent read on price action. Couldn't pick good entries/targets, or identify the best direction to trade.</p>
            <p>After I watched a few of your videos (quite a few more to go), and my first week of trading, I doubled my small account, which pays for your course and then some. I'm able to finally piece together the key components I've been missing."</p>
            <cite>Rod R. (USA)</cite>
        </blockquote>
        <blockquote>
            <p>"It is absolutely amazing to see how close the market behaved today when compared with your Daily Commentary from yesterday.</p>
            <p>Your ability to read the price action is inspirational!"</p>
            <cite>Zoran V. (UK)</cite>
        </blockquote>
    </div>
    <div class="col">
        <blockquote>
            <p>"I'm quite surprised and impressed on how you've responded to my emails and your follow up emails. It's very rare to find this on other online courses or even offline courses.</p>
            <p>It's really a good personal touch you provide your students and I'm sure all your students feel as welcome and appreciative as I do.</p>
            <p>Thanks again Chris."</p>
            <cite>Fran (Singapore)</cite>
        </blockquote>
        <blockquote>
            <p>"From the course content to the forum, it's all been a great help. I've only been a member since January, but what I've learned thus far has helped my trading tremendously. I wish more people knew about the knowledge you're sharing with everyone!"</p>
            <cite>Brad P. (Australia)</cite>
        </blockquote>
        <blockquote>
            <p>"Since joining your course in February trading actually makes sense to me now.  Reading the price action the way you teach has changed how I see the charts."</p>
            <cite>Justin C. (Canada)</cite>
        </blockquote>
    </div>
</div>
<div class="visual-box ">
    <div class="holder">
        <div class="img-holder" id="sample_lesson">
            <iframe width="426" height="284" src="//www.youtube.com/embed/HV8kQh_R4HU?autohide=1&amp;rel=0&amp;showinfo=0&amp;wmode=transparent;hd=1" frameborder="0" allowfullscreen></iframe>
        </div>
        <h2>Preview three sample lessons from the course.</h2>
    </div>
</div>
<div class="holder article-box" id="about_chris">
    <div class="img-holder">
        <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-3.jpg" alt="">
    </div>
    <div class="article">
        <h2>Why I Am Passionate About This Course</h2>
        <p>Since 2007, I have taught thousands of students how to trade Forex using my high probability Price Action Strategies. I teach the exact same strategies I use every day trading my own money in the global markets.</p>
        <p>The 2nd Skies Price Action Trading Course is about helping you to become a consistently profitable trader. It gives you tools to improve your trading skills and build a successful trading mindset.</p>
        <p>I'm passionate about trading - and teaching. That's why in addition to constantly making new videos, providing daily trade setups, and being active on the forum, I answer 100+ emails &amp; questions a day from my students. Sign up today for lifetime access to my Advanced Price Action Trading course.</p>
    </div>
</div>
<div class="info-box" id="buy_course">
    <div class="holder">
        <h2>Order the Advanced Price Action Course</h2>
        <strong>Get Lifetime Access - No Ongoing Fees, Ever!</strong>
        <div class="box-holder">
            <div class="img-holder">
                <img src="<?php bloginfo( 'template_url' ); ?>/library/images/img-1.png" alt="">
            </div>
            <div class="text">
                <p>Receive access to all 40+ hours of videos, daily commentary, forum access, learning tools, monthly webinars, and your one-on-one session with me - AND all future updates.</p>
                <p>Join by June 30th and get lifetime access to all of it for a single one-time payment of <span> <span class="mark">$499</span> only $299.</span></p>
                <strong>this offer ends on:</strong>
                <span class="date">June 30th, 2015</span>
            </div>
        </div>
        <div class="btn-add-wrapper">
            <a href="https://courses.2ndskiesforex.com/courses/startPayment?id=1" class="btn-add">Add To Cart</a>
        </div>
        <ul class="list-card">
            <li><a href="https://courses.2ndskiesforex.com/courses/startPayment?id=1" class="visa">visa</a></li>
            <li><a href="https://courses.2ndskiesforex.com/courses/startPayment?id=1" class="master-card">master-card</a></li>
            <li><a href="https://courses.2ndskiesforex.com/courses/startPayment?id=1" class="discover">discover</a></li>
            <li><a href="https://courses.2ndskiesforex.com/courses/startPayment?id=1" class="american-express">american-express</a></li>
        </ul>
        <a href="#" class="secure">secure</a>
    </div>
</div>
<div class="footer">
    <div class="holder">
        <span class="copyright">Copyright  	&copy; 2007 - <?php echo date("Y"); ?> 2ndSkies Forex. All rights reserved.</span>
    </div>
</div>

<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/sales.js"></script>
<!--[if IE]><script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/library/js/ie.js"></script><![endif]-->

<script type="text/javascript">
    jQuery(function($){
        $( '.menu-btn' ).click(function(){
            $('.responsive-menu').toggleClass('expand')
        })
    })
</script>

<script type="text/javascript">
    adroll_adv_id = "PHTC33ZBENG7JL6Y47TGXA";
    adroll_pix_id = "7RZ45EMH7ZBZ3LZE2H5COL";
    (function () {
        var oldonload = window.onload;
        window.onload = function(){
            __adroll_loaded=true;
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
            if(oldonload){oldonload()}};
    }());
</script>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
    twttr.conversion.trackPid('l5in3');</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l5in3&p_id=Twitter" />
    <img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l5in3&p_id=Twitter" /></noscript>
</body>
<?php wp_footer(); ?>
</html>
