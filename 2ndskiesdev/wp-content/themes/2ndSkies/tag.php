<?php get_header(); ?>
<div id="main" class="clearfix">
<div id="content">
	<!-- breadcrumbs container -->
	<div class="breadcrumbs-container clearfix">
		<div class="breadcrumbs">
			<ul>
				<li><a href="<?php echo home_url(); ?>">Home</a></li>
				<li>
					<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
					<?php if(get_post_type() == 'videos') { ?>
					Archive for videos <?php } ?>
					<?php /* If this is a category archive */ if (is_category()) { ?>
					Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category
					<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
					Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;
					<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
					Archive for <?php the_time('F jS, Y'); ?>
					<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
					Archive for <?php the_time('F, Y'); ?>
					<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
					Archive for <?php the_time('Y'); ?></h1>
					<?php /* If this is an author archive */ } elseif (is_author()) { ?>
					Author Archive
					<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
					Blog Archives
					<?php } ?>
				</li>
			</ul>
		</div>
	</div>
	
	<div class="posts">
			<!-- main post -->
            


<?php
// get current page we are on. If not set we can assume we are on page 1.
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
// are we on page one?
if(1 == $paged) { ?>

   
	<?php
		$queried_object = get_queried_object()->slug;
	?> 
            
	<?php $my_query = new WP_Query("tag=".$queried_object."&posts_per_page=1");
			while ($my_query->have_posts()) : $my_query->the_post();
			$do_not_duplicate = $post->ID; ?>
			<div class="main-post">
				<h1><a href="<?php echo get_permalink( $post->ID );?>"><?php echo $post->post_title; ?></a></h1>
				<!-- meta info -->
				<div class="meta">
					<ul>
						<li><strong class="date"><?php echo get_post_time('F jS, Y',false,$post->ID); ?></strong></li>
						<li>in <?php the_category(', ','', $post->ID); ?></li>
						<li>| <a href="<?php echo get_permalink( $post->ID ).'#comments';?>"><?php echo $post->comment_count; ?> comments</a></li>
					</ul>
				</div>
                
				<!-- video holder -->
                <?php if($mainPostVideo[0]) { ?>
				<div class="post-video-holder">
					<?php echo $mainPostVideo[0]; ?>
				</div>
                <?php } else { ?>
                    <a href="<?php echo get_permalink( $mainPost->ID );?>"><?php echo get_the_post_thumbnail( $mainPost->ID, array(600,400) ); ?></a>
                <?php } ?>
				<!-- content of the post -->
				<div class="content">
					<?php 
					preg_match('~^(?>(?><[^>]*>\s*)*[^<]){0,600}(?=\s)~s', $post->post_content, $m);
					$content = preg_replace('/\<img([^>]+)([^>]*)\>/i','',$m[0]); 
					//echo $content;
					//echo excerpt(200);
					the_advanced_excerpt('length=120&length_type=words&no_custom=1&ellipsis=%26hellip;&exclude_tags=img,a');
				 ?>... <a href="<?php echo get_permalink( $mainPost->ID );?>" class="more">Keep reading</a>
				</div>
			</div>
	<?php endwhile; ?>
    

<?php }
?>



<?php
// get current page we are on. If not set we can assume we are on page 1.
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
// are we on page one?
if(1 == $paged) {  $numposts = 7 ;} else { $numposts = 6 ;} ?>


			<!-- other posts -->
			<div class="post-list">
            
			 <?php  //while ( have_posts() ) : the_post(); if( $post->ID == $do_not_duplicate ) continue; 
			 $queried_object = get_queried_object()->slug;
			// echo $queried_object
			 ?>
			<?php 
			$loop_main = new WP_Query("tag=".$queried_object."&posts_per_page=".$numposts."&paged=" . $paged);
			while ($loop_main->have_posts()) : $loop_main->the_post(); if( $post->ID == $do_not_duplicate ) continue; ?>
             
             
             
				<div class="list-item">
					<div class="photo">
						<a href="<?php echo get_permalink( $post->ID );?>">
							<?php //echo get_the_post_thumbnail( $post->ID, array(285,285) ); ?>
                        	<?php the_post_thumbnail('archive-thumb'); ?>
                        </a>
					</div>
					<!-- meta info -->
					<div class="meta">
						<ul>
							<li><strong class="date"><?php echo $post->post_date; ?></strong></li>
							<li>| <a href="<?php echo get_permalink( $post->ID ).'#comments';?>"><?php echo $post->comment_count; ?> comments</a></li>
						</ul>
					</div>
					<h2><a href="<?php echo get_permalink( $post->ID );?>"><?php echo $post->post_title; ?></a></h2>
				</div>
			<?php endwhile; ?>
			</div>
			<?php wp_pagenavi();?>
		</div>
	</div>
	<?php get_sidebar('recent-popular');  ?>
</div>

<?php get_footer(); ?>
