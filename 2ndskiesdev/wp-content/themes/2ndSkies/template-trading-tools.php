<?php
/*
 * Template Name: Trading Tools Page
 */

?>
<?php get_header(); ?>

<div id="content" class="trading-tools-template video-template">

    <div class="breadcrumbs-container clearfix">
        <div class="breadcrumbs">
            <ul>
                <li><a href="<?php echo home_url(); ?>">Home</a></li>
                <li><?php the_title(); ?></li>
            </ul>
        </div><!-- .breadcrumbs -->
    </div><!-- .breadcrumbs-container -->
    
    <div class="promo-text clearfix">
        <h1><?php the_title(); ?></h1>
    	
        <div class="grid-container grid-parent nopad">
            <div class="grid-55 tablet-grid-55">
				<?php if (have_posts()) : ?>
    
                    <?php while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                    <?php endwhile; ?>
    
                <?php else : ?>
                <?php endif; ?>
            </div>
            <div class="grid-45 tablet-grid-45">
                <?php
                    // check if the post has a Post Thumbnail assigned to it.
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail('full');
                    }
                ?>
            </div>
        </div>
        
        <?php get_template_part('inc', 'share-print'); ?>
        
    </div><!-- .promo-text -->
    
    <div class="trading-tools-block">
	
    	<div class="grid-container grid-parent nopad">
        
            
            <div class="grid-50 tablet-grid-50 grid-">
            <a href="/risk-of-ruin-calculator">
            	<img class="alignleft" src="<?php echo THEME_IMAGES ?>/tool-img-02.jpg" width="155" height="156" alt="" />
            	<h4>Risk Of Ruin Calculator</h4>
            	<p>Go beyond inefficient 2-dimensional risk models. Know mathematically whether your risk parameters will make you money or not.</p>
            </a>
            </div>
            
            <div class="grid-50 tablet-grid-50 grid-">
            <a href="/position-size-calculator">
            	<img class="alignleft" src="<?php echo THEME_IMAGES ?>/tool-img-04.jpg" width="155" height="156" alt="" />
            	<h4>Position Size Calculator</h4>
            	<p>Not sure how big a position to trade? Try our easy to use position size calculator, and know exactly how many lots to trade.</p>
            </a>
            </div>
            
            <div class="grid-50 tablet-grid-33 grid-">
            <a href="/customized-learning">
            	<img class="alignleft" src="<?php echo THEME_IMAGES ?>/tool-img-03.jpg" width="155" height="156" alt="" />
            	<h4>Customized Learning</h4>
            	<p>Not sure which forex strategies or videos to learn? Take our free Customized Learning test, and get study material and videos tailored specifically to your skill level and experience.</p>
            </a>
            </div>

            <div class="grid-50 tablet-grid-50 grid-">
                <a href="/new-york-close-forex-charts">
                    <img class="alignleft" src="<?php echo THEME_IMAGES ?>/tool-img-04.png" width="156" height="155" alt="" />
                    <h4>New York Close Charts</h4>
                    <p>Get Access to 5 Day New York Close Charts and Our Preferred Trading Platform</p>
                </a>
            </div>
        
        </div>

    </div><!-- .trading-tools-block -->
    
</div><!-- #content .trading-tools-template -->

        
<?php get_footer(); ?>