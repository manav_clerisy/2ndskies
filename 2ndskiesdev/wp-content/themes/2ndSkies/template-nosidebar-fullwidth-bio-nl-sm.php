<?php
/*
Template Name: No SB FW Bio NL SM
*/
?>
<?php get_header(); ?>

<div id="main" class="clearfix">

    <div id="content" class="nosb-fullwidth">

        <div class="breadcrumbs-container clearfix">
            <div class="breadcrumbs">
                <ul>
                    <li><a href="<?php echo home_url(); ?>">Home</a></li>
                    <li><?php the_title(); ?></li>
                </ul>
            </div><!-- .breadcrumbs -->
        </div><!-- .breadcrumbs-container -->

        <?php if (have_posts()) : ?>

        <?php while (have_posts()) : the_post(); ?>

        <div class="default-template">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div><!-- .default-template -->
        <?php get_template_part('inc', 'share-print'); ?>
        <?php get_template_part('author', 'box'); ?>

        <?php endwhile; ?>

        <?php else : ?>

        <div class="default-template">
            <h1>Not Found</h1>
            <p>Sorry, but you are looking for something that isn't here.</p>
        </div><!-- .default-template -->

        <?php endif; ?>

    </div><!-- #content -->
    
</div><!-- #main -->

<?php get_footer(); ?>