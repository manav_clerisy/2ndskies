<?php
/*
Template Name: Risk of ruin
*/
?>
<?php get_header(); ?>

<div id="main" class="clearfix"> 
  <!-- main content -->
  <div id="content"> 
    <!-- breadcrumbs container -->
    <div class="breadcrumbs-container clearfix">
      <div class="breadcrumbs">
        <ul>
          <li><a href="<?php echo home_url(); ?>">Home</a></li>
          <li>
            <?php the_title(); ?>
          </li>
        </ul>
      </div>
    </div>
    
    <div class="default-template">
    
      <h1><?php the_title(); ?></h1>
      
      <form class="form-tbl calc-input-values risk-of-ruin" id="calculator">
      
        <div class="row">
          <div class="row-holder">
            <label for="inp-pr">Payoff Ratio</label>
              <input name="points" type="text" class="num-field" id="inp-pr" value="" maxlength="3" data-min="0.01" data-max="100" />
              <span class="error-text"></span>
            </div>
        </div>
        
        <div class="row">
        	<div class="row-holder">
            	<label for="inp-ac">Accuracy %</label>
            	<input type="text" id="inp-ac" name="points" class="num-field" maxlength="3" data-min="0" data-max="100" value="" />
            	<span class="error-text"></span>
        	</div>
        </div>
        
        <div class="row">
        	<div class="row-holder">
            	<label for="inp-eq">% Equity</label>
            	<input type="text" id="inp-eq" name="points" class="num-field" maxlength="3" data-min="0.01" data-max="100" value="" />
            	<span class="error-text"></span>
        	</div>
        </div>
        
        <div class="row">
        	<div class="row-holder">
            	<label for="calc">Calculate</label>
        		<button id="calc" class="button">Calculate risk of ruin</button>
            </div>
        </div>
        
        <h3 class="risk-calc"><span><i class="dashicons dashicons-chart-bar dash-chart"></i> Result</span></h3>
        
        <div class="row">
        	<div class="row-holder risk-result-holder">
        		<div class="label-risk-result">Risk of ruin:</div>
                <div id="val-rr" class="risk-result"></div>
            </div>
        </div>
        
        
      </form>
</div>
<div class="zx-separator"></div>
<?php get_template_part('inc', 'share-print'); ?>

  </div>
  <script>
	jQuery(document).ready(function ($){
		//$(function(){
			function roundRisk(val) {
				val = val * 100;
				val = Math.min(val, 100);
				var num = val > 1 ? 100 : 10000;
				val = Math.round(val * num) / num;
				return val;
			}
			function risk1(pr, win, cap){
				var qp = (1-win)/win; // q/p
				return Math.pow(qp, cap);
			}
			function risk2(pr, win, cap){
				var qp = (1-win)/win; // q/p
				return Math.pow(Math.pow(0.25 + qp, 0.5) - 0.5, cap);
			}
			function riskN(pr, win, cap) {
				var a_o = 1-win; //initialization of newton method for numerical computation
				var a_k = a_o; //a_k is the kth iteration for a in the newton computation
				var error = 10; // initializing error to be 10 (note: Any number greater than our threshold should work)
				
				var abs = Math.abs;
				var pow = Math.pow;
				
				var finished = false;
				
				while (!finished) {
					a_k = a_k + ((1-win) + win*pow(a_k,(pr+1))- a_k)/(1 - win*(pr+1)*pow(a_k,pr));
					error = abs(a_k - ((1 - win) + win*pow(a_k,(pr+1))));
	
					finished = error < 0.0001;
				}
				
				return pow(a_k, cap);
			}
			function calcRisk(pr, ac, eq) {
				pr = parseFloat(pr);
				var cap = 100 / parseFloat(eq); // Capital units
				var win = parseFloat(ac) / 100; // Probability to win
				
				var risk;
				if (pr === 1) {
					risk = risk1(pr, win, cap);
				} else if (pr === 2) {
					risk = risk2(pr, win, cap);
				} else
					risk = riskN(pr, win, cap);
				
				return risk;
			}
			
			$('#calculator').submit(function (e) {
				e.preventDefault();
				
				// Validation
				var valid = true;
				$('#inp-pr, #inp-ac, #inp-eq').each(function(){
					
					var $inp = $(this);
					var val = this .value;
					var error = '';
					
					if (val !== val.split(',').join('.'))
						this .value = val = val.split(',').join('.')
					
					if (!isFinite(val) || isNaN(parseFloat(val))) {
						$(this).parents('.row').addClass('error');
						error = 'Please enter value';
					} else if (val < $inp.data('min')) {
						$(this).parents('.row').addClass('error');
						error = 'Cannot be smaller than ' + $inp.data('min');
						
					} else if (val > $inp.data('max')) {
						$(this).parents('.row').addClass('error');
						error = 'Cannot be greater than ' + $inp.data('max');
					}
					else{
						$(this).parents('.row').removeClass('error');
					}
					
					$inp .siblings('.error-text').text(error);
					
					if (error)
						valid = false;
				});
				
				if (!valid)
					return false;
				
				var pr = $('#inp-pr').val();
				var ac = $('#inp-ac').val();
				var eq = $('#inp-eq').val();
				
				var $risk = $('#val-rr');
				risk = calcRisk(pr, ac, eq);
				risk = roundRisk(risk);
				$risk .text(risk + '%');
				
				return false;
			});
		//});
	});
	</script>
    
    
<script>
	jQuery(document).ready(function($) {
		$("input.num-field").numeric();
	});
</script>
    
    
  <?php get_sidebar('pages'); ?>
  <!-- sidebar --> 
  
</div>
<!-- #main -->

<?php get_footer(); ?>
