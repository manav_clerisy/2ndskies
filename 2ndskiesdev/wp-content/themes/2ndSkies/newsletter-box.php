<?php
/**
 * The template part for displaying Author Box.
 *
 * @package WordPress
 * @subpackage 2ndSkies
 */
 ?>
    
    <div class="signup-wrap">
    	<div class="signup-top">
        	<p>Want to Learn Price Action Strategies for Trading Forex?</p>
        </div><!--signup-top-->
    	<div class="signup-content">

                <p>Sign Up for our Monthly Newsletter and Get our <span>FREE E-Book</span></p>
                	<div class="moonray-form-p2c26856f1 ussr"><div class="moonray-form moonray-form-label-pos-stacked">
                    <form class="moonray-form-clearfix" action="https://forms.ontraport.com/v2.4/form_processor.php?" method="post" accept-charset="UTF-8">
                         <div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-text"><label for="mr-field-element-153568200068" class="moonray-form-label"></label><input name="firstname" type="text" class="moonray-form-input" id="mr-field-element-153568200068" required value="" placeholder=" Your Name"/></div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-email"><label for="mr-field-element-558496658457" class="moonray-form-label"></label><input name="email" type="email" class="moonray-form-input" id="mr-field-element-558496658457" required value="" placeholder="Email Address"/></div>
						<div class="moonray-form-element-wrapper moonray-form-element-wrapper-contracted moonray-form-element-wrapper-alignment-left moonray-form-input-type-submit"><input type="submit" name="submit-button" value="Sign Up" class="moonray-form-input btn btn-red btn-red-grad" id="mr-field-element-520094126695" src/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="afft_" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="aff_" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="sess_" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="ref_" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="own_" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="oprid" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="contact_id" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_source" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_medium" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_term" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_content" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="utm_campaign" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="referral_page" type="hidden" value=""/></div>
						<div class="moonray-form-element-wrapper moonray-form-input-type-hidden"><input name="uid" type="hidden" value="p2c26856f1"/></div>
      
                        
                    </form>
					</div>
					</div>
      
            	

        </div><!--signup-content-->
        <img class="ibook-small" src="<?php echo THEME_IMAGES ?>/book-ipad-small.png" alt="Book-iPad" />
    </div><!--signup-wrap-->