<?php
/*
 * Log pin results
 */
 
 function wpgeoip_log()
 {
     
     global $wpdb;
     
     $logs = $wpdb->get_results("SELECT * FROM wpgeoip_log ORDER BY logID DESC LIMIT 0, 100");
     
     ?>
     <div class="wrap">
         <h2>WPGeoIP Log (Latest 100 entries)</h2>
     
     <?php
     
     if($logs) {
         
         print '<table class="wp-list-table widefat posts">';
         
         print '<thead>
                  <tr>
                    <th>Action</th>
                    <th>Result</th>
                  </tr>
                </thead>
                <tbody>';
         
         foreach($logs as $log) {
             print '<tr>
                    <td>'.$log->post.'</td>
                    <td>'.$log->message.'</td>
                    </tr>';
         }
         
         print '</tbody>
                </table>';
         
     }else{
         print 'Nothing logged yet.';
     }
     
     ?>
     </div>
     <?php
     
 }
